CREATE TABLE `#__photogallery_photo_albums`(
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	parent_id INT(11) NOT NULL,
	name VARCHAR(255) NOT NULL,
	alias VARCHAR(255) NULL,
	description TEXT NULL,
	keywords TEXT NULL,
	created DATETIME NOT NULL,
	state TINYINT(1) NOT NULL DEFAULT 1,
	position INT(11) NOT NULL,
	asset_id INT(3) NOT NULL DEFAULT 0,
	index `ix_parent_id`(parent_id),
	CONSTRAINT `fk_module_id_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `#__modules`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `#__photogallery_photos`(
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
	parent_id INT(11) NOT NULL,
	name VARCHAR(255) NOT NULL,
	alias VARCHAR(255) NULL,
	path VARCHAR(255) NOT NULL,
	description TEXT NULL,
	keywords TEXT NULL,
	created DATETIME NOT NULL,
	state TINYINT(1) NOT NULL DEFAULT 1,
	position INT(11) NOT NULL,
	asset_id INT(3) NOT NULL DEFAULT 0,
	index `ix_parent_id`(parent_id),
	CONSTRAINT `fk_album_id_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `#__photogallery_photo_albums`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

