<?php

defined('_JEXEC') or die('Restricted Access');

class MastersControllerMasters extends JControllerAdmin {
	
	public function getModel($name = 'Master',$prefix = 'MastersModel',$config = array()) {
		return parent::getModel($name,$prefix,$config);
	}
	
}