<?php
defined('_JEXEC') or die;


class MastersControllerTool_photo extends JControllerForm {
	public function getLastId(){
		$config = JFactory::getConfig();
		$database = $config->get('db');
		$prefix = $config->get('dbprefix');
		$db = JFactory::getDbo();
		$query = "SELECT MAX(LAST_INSERT_ID(id)) AS ID FROM #__masters_tools_photos";
		
		$db->setQuery($query);
		return $db->loadObject()->ID;
	}
	public function add(){
		parent::add();
		$id =  $this->getLastId()+1;
		$parent_id =  $this->input->get('parent_id');
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=tools&parent_id='.$parent_id.'&id='.$id.'&view=tool_photo&layout=edit', false));
	}
	
	public function apply_n_stay(){
		parent::save();
		$id = $this->getLastId();
		$parent_id =  $this->input->get('parent_id');
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=tools&parent_id='.$parent_id.'&id='.$id.'&view=tool_photo&layout=edit', false));
		
	}
	public function save_n_close(){
		parent::save();
		$parent_id =  $this->input->get('parent_id');
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=tools&parent_id='.$parent_id.'&view=tools_photos', false));
	}
	public function cancel(){
		parent::cancel();
		$parent_id =  $this->input->get('parent_id');
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=tools&parent_id='.$parent_id.'&view=tools_photos', false));
	}
	/*public function edit(){
		parent::edit();
		$id =  $this->input->get('id');
		$parent_id =  $this->input->get('parent_id');
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=tools&parent_id='.$parent_id.'&id='.$id.'&view=tool_photo&layout=edit', false));
	}*/
	public function pre($arr){
		echo '<pre>';
		print_r($arr);
		echo '</pre>';
	}
}
