<?php
defined("_JEXEC") or die();

if(!JFactory::getUser()->authorise('core.manage','com_masters')) {
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

$controller = JControllerLegacy::getInstance('Masters');//MastersController
$controller->registerTask('unconfirm','confirm');

JLoader::register('MastersHelper',__DIR__.'/helpers/masters.php');


$input = jFactory::getApplication()->input;
$controller->execute($input->getCmd('task','display'));
$controller->redirect();
?>

