<?php

defined('_JEXEC') or die('Restricted access');


class MastersViewTool_photo extends JViewLegacy {
	
	public $form = null;
	public $input;
	public $isNew;
	public $parent_id;
	protected $item = null; 
	public function display($tpl = null) 
	{
		$this->input = JFactory::getApplication()->input;
		$this->form	= $this->get('Form');//getForm
		$this->item = $this->get('Item');
		$this->form->setFieldAttribute('path', 'directory', 'com_masters/tools/'.$this->input->get('parent_id'));
		//$this->form->setFieldAttribute('parent_id', 'value', $this->input->get('parent_id'));
		$this->addToolBar();
 		$this->setDocument();
		parent::display($tpl);
	}
	protected function addToolBar() 
	{
	
		JFactory::getApplication()->input->set('hidemainmenu', true);
		
		$this->isNew = ($this->item->parent_id == 0);
		
		if ($this->isNew)
		{
			$title = JText::_('COM_MASTERS_MANAGER_CATEGORY_NEW');
			$this->parent_id = $this->input->get('parent_id');
		}
		else
		{
			$title = JText::_('COM_MASTERS_MANAGER_CATEGORY_EDIT');
			$this->parent_id = $this->item->parent_id;
		}
		
		JToolBarHelper::title($title,'tool_phototitle');
		JToolBarHelper::apply('tool_photo.apply_n_stay');
		JToolBarHelper::save('tool_photo.save_n_close');
		JToolBarHelper::cancel('tool_photo.cancel',$this->isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE');
		
	}
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		/*$document->setTitle(JText::_('COM_DOSKA_MANEGER_TYPE_NEW_PAGE_TITLE'));*/
		$document->addStyleSheet(JUri::root(true).'/media/com_masters/css/style.css');
	}
	
	
	
}