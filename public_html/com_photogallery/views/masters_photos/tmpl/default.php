<?php

defined("_JEXEC") or die();

?>

<form action="<?php echo JRoute::_('index.php?option=com_masters&category=masters&parent_id='.$this->input->get('parent_id').'&view=masters_photos');?>" method="post" name="adminForm" id="adminForm">

<?php if(!empty($this->sidebar)) :?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar;?>
	</div>
<?php endif;?>

<div id="j-main-container" class="span10">
 
 <table class="table table-striped table-hover">
 	
 	<thead>
 			<th width="1%">
 				<?php echo JText::_('COM_MASTERS_NUM');?>
 			</th>
 			
 			<th width="2%">
 				<?php echo JHtml::_('grid.checkall');?>
 			</th>
 			<th width="90%">
 				<?php echo JText::_('COM_MASTERS_MASTER_PHOTO_NAME');?>
 			</th>
 			<th width="5%">
 				<?php echo JText::_('JSTATUS');?>
 			</th>
 			<th width="2%">
 				<?php echo JText::_('COM_MASTERS_MASTER_PHOTO_ID');?>
 			</th>
 	</thead>
 	
 	<tbody>
 		<?php if(!empty($this->items)) :?>
 			<?php $i = 1;?>

 			<?php foreach($this->items as $key=>$item) :?>
 				<tr>
 					<td>
		 				<?php echo $i;?>
		 			</td>
		 			
		 			<td>
		 				<?php echo JHtml::_('grid.id', $key ,$item->id)?>
		 			</td>
		 			<td>
		 			<?php $link = JRoute::_('index.php?option=com_masters&task=master_photo.edit&id='.$item->id.'&parent_id='.$item->parent_id);?>
		 				<?php echo JHtml::_('link',$link,$item->name);?>
		 			</td>
		 			<td>
		 				<?php echo JHtml::_('jgrid.published',$item->state,$key,'masters_photos.');?>
		 			</td>
		 			<td>
		 				<?php echo $item->id;?>
		 			</td>
 				</tr>
 				<?php $i++;?>
 			<?php endforeach;?>
 		
 		<?php endif;?>
 	</tbody>
 	
 </table>
 
	
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	
	
	<?php echo JHtml::_('form.token');?>
	</div>
</form>
