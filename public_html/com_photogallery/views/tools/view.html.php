<?php

defined("_JEXEC") or die();

class MastersViewTools extends JViewLegacy {
	
	protected $items;
	
	public function display($tpl = null) {
		
		$this->sidebar = MastersHelper::addSubMenu('tools');
		
		$this->addToolBar();
		$this->setDocument();
		$this->items = $this->get('Items');///getItems()
		parent::display($tpl);
	}
	
	
	protected function addToolBar() {
	
		JToolbarHelper::title(JText::_("COM_MASTERS_MANAGER_TOOLS"),'tooltitle');
		
		JToolbarHelper::addNew('tool.add',JText::_('COM_MASTERS_MANAGER_TOOLS_ADD'));
		JToolbarHelper::deleteList(JText::_('COM_MASTERS_MANAGER_TOOLS_DELETE_MSG'),'tools.delete');
		JToolbarHelper::divider();
		
		JToolbarHelper::publish('tools.publish','JTOOLBAR_PUBLISH',TRUE);
		JToolbarHelper::unpublish('tools.unpublish','JTOOLBAR_UNPUBLISH',TRUE);
		
		//JToolbarHelper::cancel();
		
		
		/*JToolbarHelper::custom('type.create','mastersbutton','mastersbutton_hover',JText::_('COM_MASTERS_MANEGER_TYPES_CUSTOM'),FALSE);*/
		
		
		
		JToolbarHelper::preferences('com_masters');
		
		//echo JUri::root(true)."<br />";
		//echo JUri::base(true)."<br />";
		//echo JUri::current()."<br />";
		//print_r(JUri::getInstance()->getVar('task','default'));
		
		
		
	}
	protected function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
	protected function setDocument() {
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root(TRUE)."/media/com_masters/css/style.css");
		//print_r($document);
	}
	
}
?>