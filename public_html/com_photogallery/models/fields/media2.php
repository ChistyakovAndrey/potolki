<?php

defined('_JEXEC') or die('Restricted access');
JFormHelper::loadFieldClass('list');

class JFormFieldMedia2 extends JFormFieldMedia {
 
	
	protected $type = 'Media2';
 
	protected function getOptions() {
		$parent = parent::getOptions();
		$opt = $this->getAttribute('option');
		$options = array();
		
		if(!empty($parent)) {
			foreach($parent as $option) {
				if(isset($option['directory'])){
					$option['directory'] = '/images/1/';
				}
				array_push($options, $option);
			}
		}
		return $options;
	}
}