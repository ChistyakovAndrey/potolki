<?php
defined('_JEXEC') or die('Restricted access');


class MastersModelTools extends JModelList {
	
	protected function getListQuery() {
		
		$query = parent::getListQuery();
		
		$query->select('id, name, state, alias, icon_href');
		$query->from('#__masters_tools');
		
		return $query;
	}
}
?>