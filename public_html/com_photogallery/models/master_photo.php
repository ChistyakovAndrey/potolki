<?php
defined("_JEXEC") or die();

class MastersModelMaster_photo extends JModelAdmin {
	
	
	public function getForm($data = array(),$loadData = true) {
		
		$form = $this->loadForm(
					$this->option.'master_photo',
					'master_photo',
					array('control'=>'jform','load_data'=>$loadData)
				);
		
		if(empty($form)) {
			return FALSE;
		}
		
		return $form;
	}
	
	public function getTable($type = 'Master_photo', $prefix = 'MastersTable',$config = array()) {
		return JTable::getInstance($type,$prefix,$config);
	}
	
	protected function loadFormData(){
		$data = JFactory::getApplication()->getUserState('com_masters.edit.master_photo.data',array());
		
		if(empty($data)) {
			
			$data = $this->getItem();
		}
		
		return $data;
	}
	
	public function save($data) {
		
		if(!trim($data['name'])) {
			$this->setError(JText::_('COM_MASTERS_WARNING_PROVIDE_VALID_NAME'));
			return FALSE;
		}
		
		if(trim($data['alias']) == '') {
			$data['alias'] = $data['name'];
			$data['alias'] = JApplicationHelper::stringURLSafe($data['alias']);
		}
		
		
		if(parent::save($data)) {
			return TRUE;
		}
		return FALSE;
		
	}
	public function pre($arr){
		echo '<pre>';
		print_r($arr);
		echo '</pre>';
	}
}
?>