<?php
/**
 * @version    3.6.x
 * @package    Simple Image Gallery Pro
 * @author     JoomlaWorks - https://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2018 JoomlaWorks Ltd. All rights reserved.
 * @license    https://www.joomlaworks.net/license
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class plgButtonJw_SigPro extends JPlugin
{
    public function __construct(&$subject, $config)
    {
        parent::__construct($subject, $config);
        $this->loadLanguage('', JPATH_ADMINISTRATOR);
    }

    public function onDisplay($name)
    {
        $document = JFactory::getDocument();
        if (version_compare(JVERSION, '3.0', 'lt')) {
            $document->addScript('https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js');
        } else {
            JHtml::_('jquery.framework');
        }
        
        // Fancybox
        $document->addScript('https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js');
        $document->addStyleSheet('https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css');
        
        $document->addScript(JURI::root(true).'/administrator/components/com_sigpro/js/editor.js?v=3.6.0');
        $document->addStyleSheet(JURI::root(true).'/administrator/components/com_sigpro/css/editor.css?v=3.6.0');

        $button = new JObject();
        $link = 'index.php?option=com_sigpro&amp;tmpl=component&amp;type=site&amp;editorName='.$name;
        $j3xLink = 'index.php?option=com_sigpro&tmpl=component&type=site&editorName='.$name;
        $application = JFactory::getApplication();
        if ($application->isSite()) {
            $link .= '&amp;template=system';
            $j3xLink .= '&template=system';
        }
        $button->set('link', $link);
        $button->set('text', JText::_('PLG_EDITORS-XTD_JW_SIGPRO_IMAGE_GALLERIES'));
        $button->set('name', 'sigProEditorButton');
        $button->set('onclick', 'SigProModal(this); return false;');
        
        if (version_compare(JVERSION, '3.0', 'ge')) {
            $button->class = 'btn';
        }
        
        if (version_compare(JVERSION, '3.5', 'ge')) {
            $config = JFactory::getConfig();
            $user = JFactory::getUser();
            $editor = $user->getParam('editor', $config->get('editor'));
            if ($editor == 'tinymce') {
                $button->modal = false;
                $button->link = '#';
            }
            $button->set('onclick', 'SigProModal(this, \''.$j3xLink.'\'); return false;');
        }
        
        // Show when usable
        if (version_compare(JVERSION, '3.0', 'ge')) {
            if ($user->authorise('core.create', 'com_sigpro') === false) {
                return;
            }
        }
                
        return $button;
    }
}
