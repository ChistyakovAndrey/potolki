<?php
defined('_JEXEC') or die;
$class = $params->get('solial-links-style');
?>

<style>
	.social-links{
    margin:5px 0;
    cursor:pointer;
    padding-left:15px;}

	.social-links li{
		position:relative;
		display:inline-block;
		width:30px;
		height:29px;
		background-size:cover;
	}

	.social-links li:last-child{
    margin:0;
	}

	.social-links li:hover{
		background-position:-30px;
	}

	.social-links li a{
		position:absolute;
		left:0;top:0;right:0;bottom:0;
		text-decoration:none;
		color:#fff;
		}
</style>

<div class="row groups row-flex <?php echo $class; ?>">
  <ul class="social-links">
    <?php foreach($param['social-links'] as $value):?>
      <li class="social-link" style="background: url(<?php echo $value['social-links-icon'];?>) no-repeat center;">
        <a href="<?php echo $value['social-links-url'];?>" title="<?php echo $value['social-links-title'];?>" target="_blank"></a>
      </li>
    <?php endforeach;?>
  </ul>
</div>