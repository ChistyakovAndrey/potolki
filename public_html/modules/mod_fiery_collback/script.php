<?php
class mod_fiery_collbackInstallerScript
{
	/**
	 * Constructor
	 *
	 * @param   JAdapterInstance  $adapter  The object responsible for running this script
	 */
	
	
	/**
	 * Called before any type of action
	 *
	 * @param   string  $route  Which action is happening (install|uninstall|discover_install|update)
	 * @param   JAdapterInstance  $adapter  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	//public function preflight($route, JAdapterInstance $adapter);
	
	/**
	 * Called after any type of action
	 *
	 * @param   string  $route  Which action is happening (install|uninstall|discover_install|update)
	 * @param   JAdapterInstance  $adapter  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	//public function postflight($route, JAdapterInstance $adapter);
	
	/**
	 * Called on installation
	 *
	 * @param   JAdapterInstance  $adapter  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	 public function postflight($type, $parent)
    {
        
    }
	public function install($parent){
		if(!file_exists("/libraries/mail.php")){
			rename(JPATH_SITE."/modules/mod_fiery_collback/mail.php", JPATH_SITE."/libraries/mail.php");
		}
		if(!file_exists("/libraries/SendMailSmtpClass.php")){
			rename(JPATH_SITE."/modules/mod_fiery_collback/SendMailSmtpClass.php", JPATH_SITE."/libraries/SendMailSmtpClass.php");
		}
		
		
	}
	
	/**
	 * Called on update
	 *
	 * @param   JAdapterInstance  $adapter  The object responsible for running this script
	 *
	 * @return  boolean  True on success
	 */
	//public function update(JAdapterInstance $adapter);
	
	/**
	 * Called on uninstallation
	 *
	 * @param   JAdapterInstance  $adapter  The object responsible for running this script
	 */
	public function uninstall($parent){
		//$parent->getParent()->setRedirectURL('index.php?option=mod_fiery_collback');
		if(!is_dir(JPATH_SITE."/modules/mod_fiery_order")){
			if(file_exists(JPATH_SITE."/libraries/mail.php")){
				unlink(JPATH_SITE."/libraries/mail.php");
			}
			if(file_exists(JPATH_SITE."/libraries/SendMailSmtpClass.php")){
				unlink(JPATH_SITE."/libraries/SendMailSmtpClass.php");
			}
		}
	}
}

?>