<?php
class ModFieryCollbackHelper{
	private $paramsMail;
	private $phone;
	private $param;
	public function __construct($input){
		$this->param = json_decode($input->getString('parameters'));
		jimport('mail');
		if($input->getString('phone-form') !== null){
			if(!empty($input->getString('phone'))){
				$mailPlg = JPluginHelper::getPlugin( 'mail', 'mail' );
				$this->paramsMail = json_decode($mailPlg->params);
				
				$result_captcha = json_decode($this->check_captcha($input));
				ob_start();
				require_once "tmpl/mail_tmpl.php";
				$mail_body = ob_get_contents();
				ob_end_clean();
				if($this->param->captcha == true){
					if($result_captcha->success > 0){
						MailHelper::send_mailer(
							$mailPlg->params,
							$this->param->smtp_mail_header,
							$mail_body
							);
					}else{
						printf('reload_page');
					}
				}else{
					MailHelper::send_mailer(
							$mailPlg->params,
							$this->param->smtp_mail_header,
							$mail_body
						);
				}
			}else{
				printf("Поле ввода не может быть пустым!");
			}
		}
		
	}
	public static function fooAjax(){
		$module = JModuleHelper::getModule('mod_fiery_collback');
		$input  = JFactory::getApplication()->input;
		$helper = new ModFieryCollbackHelper($input);
	}
	private function check_captcha($input){
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$params = array(
		    'secret' => $this->paramsMail->secret_key,
		    'response' => $input->getString('g-recaptcha-response')
		);
		$result = file_get_contents($url, false, stream_context_create(array(
		    'http' => array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded',
		        'content' => http_build_query($params)
		    )
		)));

		return $result;
	}

	public function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
}

?>