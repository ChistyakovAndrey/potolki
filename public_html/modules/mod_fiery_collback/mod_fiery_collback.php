<?php
defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_fiery_collback');
$param = json_decode($module->params);

$template = 'fieryflash_template';
JLoader::register('Fieryflash', JPATH_THEMES.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'functions.php');
$city = Fieryflash::get_city_name(true);

$mailPlg = JPluginHelper::getPlugin( 'mail', 'mail' );
$paramsMail = json_decode($mailPlg->params);
$doc = JFactory::getDocument();
if(!empty($paramsMail->site_key)){
	$doc->addScript('https://www.google.com/recaptcha/api.js?hl=ru');
}
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/jquery.maskedinput.min.js');
$doc->addStyleSheet(JURI::base().'modules/'.$module->module . '/assets/fiery_collback.css');
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/fiery_collback.js');
require JModuleHelper::getLayoutPath('mod_fiery_collback', $params->get('layout', 'default'));

?>
