<?php defined('_JEXEC') or die; ?>
<style>
	.collback{
		background:<?php echo $params->get('background-color'); ?>;
	}
</style>
<div class="collback">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="collback-text">
					<?php echo $params->get('callback-text'); ?>
					<span><?php echo $params->get('callback-number'); ?></span>
				</div>
			</div>
			<div class="col-md-6">
				<div id="overlay"></div>
				<div class="collback-form">
						<form id="mail_form" action="" method="POST">
						<input type="hidden" name="phone-form"/>
						<input type="hidden" name="parameters" value='<?php echo $module->params;?>'/>
							<label for="phone-number"></label>
							<input type="text" name="phone" class="phone-number" id="phone-number" placeholder="Ваш телефон">
							<input type="button" class="execute" value="Перезвоните мне"/>
							<div id='result-collback-form' class='text-center'></div>
							<?php if($param->captcha == true && !empty($paramsMail->site_key)):?>
							<div class = "my_captcha">
								<div class="g-recaptcha" data-sitekey="<?php printf($paramsMail->site_key);?>"></div>
								<input type="button" class="approveSend" value="Отправить"/>
							</div>
							<?php endif;?>
						</form>
					<span>оставьте нам свой номер телефона<br/>и мы перезвоним через 1 минуту</span>
				</div>
			</div>
		</div>
	</div>
</div>