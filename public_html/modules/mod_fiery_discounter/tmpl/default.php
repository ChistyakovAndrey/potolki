<div class="slider slick-slider-responsive-1 slick-arrows-white">
	<?php foreach($param['discounter_info'] as $value):?>
		<div class="slide-1">
			<div class="slide-title">
				<span><?php echo $value['discounter-text-title'];?></span><?php echo $value['discounter-text-additional-title'];?>
			</div>
				<div class="info-blocks">
					<div class="info-left">
						
						<span class="information-label">
              <?php echo $value['discounter-general-label'];?>
						</span>
						<?php echo $value['discounter-general-info'];?>
						
					</div>
					
					<?php if(!empty($value['discounter-price'])) : ?>
            <div class="info-right action">
              <div class="action-date">
                до <?php echo $value['discounter-end-date'];?>
              </div>
              <div class="action-price">
                <span><?php echo $value['discounter-price'];?></span>руб/м2
              </div>
            </div>
					<?php endif;?>
				</div>
			</div>
	<?php endforeach;?>
</div>