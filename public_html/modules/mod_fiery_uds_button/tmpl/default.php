<?php defined('_JEXEC') or die; ?>

<?php if($params->get('load_button') == 1):?>
<div class="salealert">
	<a href="<?php echo $params->get('link');?>" target="_blank"></a>
	<span class="text-semibold"><?php echo $params->get('text');?></span>
</div>
<?php endif; ?>