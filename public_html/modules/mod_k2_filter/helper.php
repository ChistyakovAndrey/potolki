<?php
defined('_JEXEC') or die('Restricted access');
class ModK2FilterHelper {
	private $param;
	private $input;
	public function __construct($input, $params){
		$params = (array)$params;
		$filter = self::getList($params,"ajax");
		
		$ids_to_del = $this->getIDSFromFilter($filter, $input->getString("input_square"));
		$result_filter = $this->filter_items_from_filter($filter,$ids_to_del);
		
		$maxmin_price = $this->get_maxmin_prices_filter($result_filter);
		$result_filter = $this->final_sort_filter($filter, $maxmin_price);
		$prices = array();
		foreach($maxmin_price as $cat_prices){
			foreach($cat_prices as $value){
				if($value != ""){
					array_push($prices, $value);
				}
				
			}
			
		}
	
		require_once "tmpl/result_filter_ceilings.php";
		
	}
	
	protected static function get_extra_fields_by_name($name){
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from($db->quoteName('#__k2_extra_fields'));
		$query->where($db->quoteName('name').' = "'.$name.'"');
		$db->setQuery($query);
		return $db->loadAssocList();
	}
	public static function get_extra_field_value($arr,$name){
		$ids = self::get_extra_fields_by_name($name);
		$result = "";
		foreach($ids as $id){
			foreach($arr as $value){
			if($value->id == $id['id']){
				$result = $value->value;
			break;
				}
			}
		}
		return $result;
	}
	
	
	public function final_sort_filter($filter, $maxmin_price){
		
		foreach($filter as $key => $cat){
			$tmp_arr = array();
				foreach($maxmin_price[$key] as $a => $price){
						array_push($tmp_arr, $cat->materials[$a]);
						unset($cat->materials[$a]);		
				}
				foreach($tmp_arr as $value){
					array_push($cat->materials, $value);
				}	
		}
		return $filter;
	}
	public function get_maxmin_prices_filter($result_filter){
		$ids_price = $this->getIDExtField("price");
		$result = array();
		foreach($result_filter as $key => $cat){
			$result[$key] = array();
			foreach($cat->materials as $k=>$material){
				
				foreach(json_decode($material['extra_fields']) as $extra_field){
					foreach($ids_price as $id_price){
						if($id_price == $extra_field->id ){
							if($extra_field->value != ""){
								$ext_field_tmp = (int)str_replace(" ","",$extra_field->value);
								$result[$key][$k] = "";
								$result[$key][$k] .= $ext_field_tmp;
							}
						}
					}
				}
			}
			asort($result[$key]);
		}	
		return $result;
	}
	
	public function filter_items_from_filter($filter,$ids_to_del){
		foreach($filter as $cat){
			foreach($cat->materials as $key => $value){
				foreach($ids_to_del as $id_to_del){
					if($value['id'] == $id_to_del){
						unset($cat->materials[$key]);
					}
				}
			}
		}
		return $filter;
	}
	public function getIDSFromFilter($filter,$input_square){
		$result_to_del = array();
		$result = array();
		$ids_area = $this->getIDExtField("area");
		foreach($filter as $key => $cat){
			foreach($cat->materials as $k=>$material){
				foreach(json_decode($material['extra_fields']) as $extra_field){
					foreach($ids_area as $id_area){
						if($id_area == $extra_field->id ){
							$ext_field_tmp = explode(",",$extra_field->value);
							$area = (float)($ext_field_tmp[0].'.'.$ext_field_tmp[1]);
							if(ceil($area) == (int)$input_square){
								array_push($result, $k);
							}else{
								array_push($result_to_del, $material['id']);
							}
						}
					}
				}
			}
		}	
		return $result_to_del;
	}
	public function getIDExtField($alias){
		$result = array();
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__k2_extra_fields'));
		$db->setQuery($query);
		foreach($db->loadObjectList() as $extField){
			foreach(json_decode($extField->value) as $field)
			if($field->alias == $alias){
				array_push($result, $extField->id); 
			}
		}
		return $result;
	}
	public function filter_items(){
		
	}
	public function prea($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
	public function get_params($type){
		if($type == 'module'){
			echo json_encode($this->param);
		}
	}
	public static function getList($params,$flag="non-ajax") {
		$result = array();
		$ids = '';
		if($flag == "non-ajax"){
			if ($params->get('filter') && is_array($params->get('filter')) && count($params->get('filter')) > 0){
				if (!in_array(0, $params->get('filter'))) {
					$ids = $params->get('filter');
				}
			}
			if ($params->get('get-childs')) {
				$ids  = self::getIDs($ids);
				
			}
			if(!$params->get('show-parents')){
				foreach($params->get('filter') as $value){
					unset($ids[array_search($value,$ids)]);
				}
			}
		}
		if($flag == "ajax"){
			if ($params['filter'] && is_array($params['filter']) && count($params['filter']) > 0){
				if (!in_array(0, $params['filter'])) {
					$ids = $params['filter'];
				}
			}
			if ((int)$params['get-childs']) {
				$ids  = self::getIDs($ids);	
			}
			if(!$params['show-parents']){
				foreach($params['filter'] as $value){
					unset($ids[array_search($value,$ids)]);
				}
			}
		}
		return self::getFilter($ids,$params,$flag);
	}
	public static function fooAjax(){
		$module = JModuleHelper::getModule('mod_k2_filter');
		$params = json_decode($module->params);
		$input  = JFactory::getApplication()->input;
		$helper = new ModK2FilterHelper($input, $params);
	}
	protected static function getFilter($ids,$params,$flag="non-ajax"){
		
		$result = array();
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select(array('id','name', 'alias', 'parent', 'description', 'image'));
		$query->from($db->quoteName('#__k2_categories'));
		if ($ids) {
			$query->where($db->quoteName('id').' IN ('.implode(',', $ids).')');
		}
		$query->where($db->quoteName('published').' = '.$db->quote(1));
		$query->where($db->quoteName('language').' IN ('.$db->quote(JFactory::getLanguage()->getTag()).', '.$db->quote('*').')');
		$query->where($db->quoteName('access').' IN '.'('.implode(',',JFactory::getUser()->getAuthorisedViewLevels()).')');
		$query->order('ordering'.' ASC');
		$db->setQuery($query);
		$result = self::prepareFilter($db->loadObjectList('id'), $params, $flag);
		return $result;
	}
	protected static function prepareFilter ($rows, $params, $flag="non-ajax"){
		$result = array();
		require_once (JPATH_SITE.'/components/com_k2/helpers/route.php');
		$unsets = array();
		foreach ($rows as $id => $row) {
			if($flag == "non-ajax"){
				$materials = self::getMaterials($row->id,$params->get("limit_output"));
			}
			if($flag == "ajax"){
				$materials = self::getMaterials($row->id,$params['limit_output']);
			}
			
			$row->title = $row->name;
			$row->image = 'media/k2/filter/'.$row->image;
			if (JFile::exists($row->image)) {
				$row->image = '/'.$row->image;
			}
			else {
				$row->image = false;
			}
			$row->link = JRoute::_(K2HelperRoute::getCategoryRoute($row->id.':'.urlencode($row->alias)));
			$row->childs = self::getChilds($rows,$id);
			$row->child = count($row->childs);
			$row->materials = $materials;
			
			$unsets = array_merge($unsets, array_keys($row->childs));
			$rows[$id] = $row;
			
		}
		foreach ($unsets as $key) {
			unset($rows[$key]);
		}
		$result = $rows;
		//self::pre($result);		
		return $result;
		
	}
	protected static function getMaterials($catid,$limit){
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__k2_items'));
		$query->where($db->quoteName('catid').' = '.$catid);
		$query->where($db->quoteName('published').' = 1');
		$query->setLimit($limit);
		$db->setQuery($query);
		return $db->loadAssocList();
	}
	public static function getMaxMin($filter,$alias){
		$result = array();
		foreach($filter as $cat){
			foreach($cat->materials as $material){
				
				//print_r(json_decode($material['extra_fields']));
				foreach(json_decode($material['extra_fields']) as $extraField){
					$field = self::getExtraField($extraField->id);
					if(json_decode($field->value)[0]->alias == $alias){
						//echo $extraField->value;
						if($alias=='area'){
							$float_num = explode(',',$extraField->value);
							$extraField->value = $float_num[0].'.'.$float_num[1];
							array_push($result,(float)$extraField->value);
						}else{
							if($extraField->value !== ""){
								array_push($result,(int)str_replace(' ', '', $extraField->value));
							}
							
						}
					}
					//echo $field->name."<br/>";
					//echo $field->id."<br/>";
				}
			}
		}
		return $result;
	}
	public static function getExtraField($id){
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__k2_extra_fields'));
		$query->where($db->quoteName('id').' = '.$id);
		$db->setQuery($query);
		return $db->loadObject();
	}
	protected static function getChilds ($rows, $parent){
		$result = array();
		foreach ($rows as $id => $row) {
			if ($row->parent == $parent) {
				$result[$id] = $row;
			}
		}
		return $result;	
	}
	protected static function getIDs ($filter){
		
		$db = JFactory::getDBO();
		if (!is_array($filter)){
			$filter = array($filter);
		}
		JArrayHelper::toInteger($filter);
		$filter = array_unique($filter);
		$array = $filter;
		while (count($array)) {
			$query = $db->getQuery(true);
			$query->select('id');
			$query->from($db->quoteName('#__k2_categories'));
			$query->where($db->quoteName('parent').' IN ('.implode(',', $array).')');
			$query->where($db->quoteName('id').' NOT IN ('.implode(',', $array).')');
			$db->setQuery($query);
			$array = $db->loadColumn();
			$filter = array_merge($filter, $array);
		}
		JArrayHelper::toInteger($filter);
		$filter = array_unique($filter);
		return $filter;
	}
	protected static function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}




}
