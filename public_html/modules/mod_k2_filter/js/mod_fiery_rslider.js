(function($){
	$(document).ready(function(){
setInterval(function() {
if($('.calculation-item-body').parent().parent('.slick-active').length < 1){
$('.result-title').parent('div').hide();
}else{
$('.result-title').parent('div').show();
}
if($('.range-slider-price').get(0).firstChild == null){
$('.range-slider-price').text("0 руб.");
}
}, 100);
		
	result_calc($("input[name=std_square]").val());
	if($('.calculation-item-body ').parent().parent().length < 1){
		
		//$('.result-title').parent('div').hide();
	}
	
	
	$(".range-slider-line").each(function(){
			var insert_val=$(this).closest(".range-slider").find(".range-slider-input-val");
			
			var curr_slide=$(this).slider({
				min:parseInt($(this).attr("data-min")),
				max:parseInt($(this).attr("data-max")),
				step:parseFloat($(this).attr("data-step")),
				value:parseInt($(this).attr("data-val")),
				stop: function(event, ui) {
					insert_val.val(curr_slide.slider("value"));
					result_calc(curr_slide.slider("value"));
					var data=$(".range-slider-input-val").val();
       				$(".get-arrow").text(data);
					sort_clides();
					//calc(); можно подключить функцию обработки/расчета если надо
				},
				slide: function(event, ui){
					setTimeout(function(){
						insert_val.val(curr_slide.slider("value"));
						result_calc(curr_slide.slider("value"));
						//calc(); можно подключить функцию обработки/расчета если надо
					},30);
				}
				
			});
			
			insert_val.on("change",function(){
				var this_val=$(this).val();
				var tmp_1=curr_slide.slider("value");
				var tmp_2=this_val;
			
				if(tmp_1!=tmp_2){
					curr_slide.slider("value",tmp_2);
					//calc(); можно подключить функцию обработки/расчета если надо
				}
			});
			
			insert_val.val($(this).attr("data-val")).trigger("change");
			
		});			

		function sort_clides(){
				$.ajax({
					type: 'POST',
					url: "?option=com_ajax&module=k2_filter&format=debug&method=foo",
					data: $("#range_sort_slider").serialize(),
					beforeSend: function(){
						$("#overlay").css('display', 'block');
					},
					success: function(data){
						//$("#overlay").css('display', 'none');
						//alert(data);
						$('.result_filter_ceilings').html(data);
			        }
				});
			}	

	 	setInterval(function(){
		 	var width = parseInt($('.range-slider-line').children('span').css("left"));
			$('.range-slider-line-child').css('max-width',width+'px');
				
	 	},5);
		
		function result_calc(value){
			var min_price = $("input[name=min_price]").val();
			var max_price = $("input[name=max_price]").val();
			var std_square = $("input[name=std_square]").val();
			//console.log("Значение:"+value);
			$("input[name=input_square]").val(value);
			if(value=="" || value == null){
				value = std_square;
			}
			//$('.range-slider-price').html(value * min_price+" - "+value * max_price+" руб.");
		}
		
		
	});
	})(jQuery);