<?php
defined('_JEXEC') or die('Restricted access');
require_once __DIR__ . '/helper.php';

$filter = ModK2FilterHelper::getList($params);
if(class_exists('McsData')) $cityHref = McsData::get('city');
$doc = JFactory::getDocument();
//$doc->addScript(JURI::base().'modules/'.$module->module . '/js/ui-slider/jquery-1.12.4.js');
$doc->addScript(JURI::base().'modules/'.$module->module . '/js/ui-slider/jquery-ui.js');
$doc->addScript(JURI::base().'modules/'.$module->module . '/js/mod_fiery_rslider.js');
$doc->addScript(JURI::base().'modules/'.$module->module . '/js/ui-slider/jquery.ui.touch-punch.min.js');

$doc->addStyleSheet(JURI::base().'modules/'.$module->module . '/css/custom.css');
require JModuleHelper::getLayoutPath('mod_k2_filter', $params->get('layout', 'default'));

?>