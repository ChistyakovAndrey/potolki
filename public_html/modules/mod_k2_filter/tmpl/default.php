<?php
defined('_JEXEC') or die('Restricted access');
$maxminSquare = ModK2FilterHelper::getMaxMin($filter,'area');
$maxminPrice = ModK2FilterHelper::getMaxMin($filter,'price');

$minPrice = $params->get("min-price");
$maxPrice = $params->get("max-price");

?>
<div class="result_ajax"></div>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<form action="" id="range_sort_slider" class="range-slider-form">
				<input type="hidden" name="min_price" value="<?php printf($minPrice);?>">
				<input type="hidden" name="max_price" value="<?php printf($maxPrice);?>">
				<input type="hidden" name="std_square" value="<?php printf(min($maxminSquare));?>">

				<input type="hidden" name="input_square">
				<input type="hidden" name="range_slider">

				<div class="range-slider">
					<div class="row">
						<div class="col-sm-<?php echo number_format(12/$params->get('colls')); ?>">
							<div class="range-slider-row-1">
								<div class="range-slider-txt">Потолок, м2</div>
								<div class="range-slider-input"><input type="text" class="range-slider-input-val calc_1_val_type_1" value="1"/></div>
							</div>
							<div class="range-slider-row-2">
								<div class="range-slider-line-parent rslider">
									<div class="range-slider-line2 rslider"></div>
									<div class="range-slider-line3 rslider"></div>
								</div>
								<div class="range-slider-line" 
									data-min="<?php printf(min($maxminSquare));?>" 
									data-val="1" 
									data-step="1" 
									data-max="<?php printf(max($maxminSquare));?>">
									<div class="range-slider-line-child"></div>
								</div>
								<div class="calс_tab_slider_grad">
									<ul>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</div>
								<div class="calс_tab_slider_grad2">
									<div class="calс_tab_slider_num calс_tab_slider_num_1"><?php printf(min($maxminSquare));?></div>
									<div class="calс_tab_slider_num calс_tab_slider_num_30"><?php printf((min($maxminSquare)+max($maxminSquare))/2);?></div>
									<div class="calс_tab_slider_num calс_tab_slider_num_60"><?php printf(max($maxminSquare));?></div>
								</div>
							</div>
						</div>

						<div class="col-sm-<?php echo number_format(12/$params->get('colls')); ?>">
							<div class="range-slider-price">0 руб</div>
								<?php if(
									(!empty($params->get('show_link'))) &&
									(!empty($params->get('link')))
								) :?>

								<div class="range-details">
									<a href="<?php printf($cityHref);?>/<?php echo $params->get('link');?>" class="button">Подробнее</a>
								</div>
								<?php endif; ?>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="calc-result-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="result-title text-center">Для ~<span class="get-arrow"></span> кв.м расчеты клиентов были такими:</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="result_filter_ceilings"></div>
			</div>
		</div>
	</div>
</div>

