<?php
if(class_exists('McsData')) $cityHref = McsData::get('city');
$total = 0;
foreach($result_filter as $category){
	if(count($category->materials) > 0){
		$total++;
	}
}
?>
<?php if($total > 0):?>
<div class="maxmin_price_range">
	<?php
		if(min($prices) !== max($prices)){
			printf(min($prices)."-".max($prices));
		}else{
			printf(min($prices));
		}
	 ?> руб.
</div>

<div class="tabs-wrapper">
	<ul class="nav nav-tabs" id="filter-tabs">
		<?php foreach ($result_filter as $key => $category): ?>
			<?php if(count($category->materials) > 0):?>
				<li class="k2-filter-navigate">
					<a data-toggle="tab" href="#materials<?php echo $category->id; ?>"><?php echo $category->title; ?></a>
				</li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>
	<div class="tab-content">
		<?php foreach($result_filter as $key => $value): ?>
			<div id="materials<?php echo $value->id;?>" class="tab-pane fade in k2-filter-materials">
				<div class="row-flex filter-result-slider filter slick-arrows-white">
					<?php foreach($value->materials as $k => $v): ?>
					<?php $extra_fields = json_decode($v['extra_fields']);?>
						<div class="slide-object">
							<div class="calculation-item-<?php echo $v['id'];?>">
								<div class="calculation-item-body">
									<div class="slide-object-title"><?php echo $v['title'];?></div>
									<hr/>

								<div class="catItemExtraField">
									<div class="slideItemExtraFieldTitle">Включено:</div>
									<div>Полотно - <?php echo ModK2FilterHelper::get_extra_field_value($extra_fields,'площадь');?> м2</div>
									<div>профиль ПВХ <?php echo ModK2FilterHelper::get_extra_field_value($extra_fields,'профиль');?> м2</div>
									<div>обработка угла <?php echo ModK2FilterHelper::get_extra_field_value($extra_fields,'обработка угла');?></div>
									<div>обход трубы <?php echo ModK2FilterHelper::get_extra_field_value($extra_fields,'обход трубы');?> м2</div>
									<div>изготовл.отверстия  <?php echo ModK2FilterHelper::get_extra_field_value($extra_fields,'изготовл. отверстия');?></div>
									<div>стойка светильника <?php echo ModK2FilterHelper::get_extra_field_value($extra_fields,'стойка светильника');?></div>
								</div>
									
									<?php if(ModK2FilterHelper::get_extra_field_value($extra_fields,'Бесплатный сервис') == 2):?>
									<hr/>
									<span class="free-service">Бесплатный сервис</span>
									<?php endif;?>
									<!-- <div class="item-description"><?php //echo $v['introtext'];?></div> -->
									<hr/>
									<div class="price"><?php echo json_decode($v['extra_fields'])[0]->value;?></div>
								</div>
							</div>
						</div>
					<?php endforeach;?>
				</div>
			</div>
		<?php endforeach;?>
	</div>
</div>
	<?php else:?>
			
	<div class="filter-info">Для Вашей площади подсчет еще не производился, но Вы можете <a href="<?php printf($cityHref);?>/natyazhnye-potolki/vyzvat-zamershchika" title="Вызвать замерщика">Вызвать замерщика</a></div>

	<?php endif;?>

<script>
	(function($){
		$('.range-slider-price').html($('.maxmin_price_range').text());
		$('.maxmin_price_range').text("");

		$('.k2-filter-navigate').first().addClass('active');
		
		//$('.k2-filter-materials').first().addClass('active');
		$($('.k2-filter-navigate').first().children('a').attr('href')).addClass('active');
			
			$('.k2-filter-navigate').click(function(){
				var id = jQuery(this).children('a').attr('href');
				$('.k2-filter-navigate').each(function(elem){
					$(this).removeClass('active');
				});
				$(this).addClass('active');
				$('.k2-filter-materials').each(function(elem){
					$(this).removeClass('active');
				});
				///alert(id);

				$(id).addClass('active');
			});
	})(jQuery);

	$('.filter-result-slider').slick({
		dots: true,
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1024,
			      settings: {
			        slidesToShow: 3,
			        slidesToScroll: 3,
			        infinite: true,
			        dots: true
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			  ]
			});
</script>