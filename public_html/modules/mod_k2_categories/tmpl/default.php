<?php
defined('_JEXEC') or die('Restricted access');

$template = 'fieryflash_template';
JLoader::register('Fieryflash', JPATH_THEMES.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'functions.php');
$city = Fieryflash::get_city_name(true);
?>
<div class="image-section-1">
<div class="mod_k2_cetegories">
	<div class="container">
		<div class="row">
			<div class="col-md-12 calculation-section-title">
				<h3>Расчёт стоимости натяжных потолков в <?php echo $city;?></h3>
			</div>
		</div>
		<div class="row">
				<div class="tabs-wrapper">
					<ul class="nav nav-tabs ">
					<?php foreach ($categories as $key => $category): ?>
					  <li class="k2-categories-filter-navigate">
					  	<a data-toggle="tab" href="#StaticMaterials<?php echo $category->id; ?>"><?php echo $category->title; ?></a>
					  </li>
					<?php endforeach; ?>
					</ul>
					<div class="tab-content">
					
							<?php foreach($categories as $key => $value): ?>
								<div id="StaticMaterials<?php echo $value->id;?>" class="tab-pane fade in k2-categories-filter-materials">
								<div class="tab-content category-tab-content row-flex mobile-slider slick-arrows-white">
								<?php foreach($value->materials as $k => $v): ?>
								<?php $extra_fields = json_decode($v['extra_fields']);?>
									<div class="col-md-4">
										<div class="calculation-item-<?php echo $v['id'];?>">
										<?php
										$dir = array();
										if($v['gallery'] !== "" && $v['gallery'] !== "NULL" && $v['gallery'] !== null  && !empty($v['gallery'])){
											$id_gallery = explode('{/gallery}',explode("{gallery}",$v['gallery'])[1])[0];
											$dir = array();
											$labels = JPATH_BASE.'/media/k2/galleries/'.$id_gallery.'/ru-RU.labels.txt';
											$lines = file($labels);
											foreach($lines as $line){
												$tmp = explode('|',$line);
												$tmp_arr = array('file'=>$tmp[0],'title'=>$tmp[1],'description'=>$tmp[2]); 
												array_push($dir,$tmp_arr);
											}
										}
										?>
											<div class="item-discount"><?php echo json_decode($v['extra_fields'])[1]->value;?></div>

											<div class="item-image">
											<?php if(count($dir) < 1):?>
												<img src="<?php echo $v['image'];?>" alt="" id="ch0" class="">
											<?php else:?>
												<?php $counter = 0;?>
												<?php foreach($dir as $image):?>
													<?php $counter++;?>
														<img src="<?php echo '/media/k2/galleries/'.$id_gallery.'/'.$image['file']?>" alt="" id="ch<?php echo $counter;?>" class="">
													
													<?php endforeach;?>
												<?php endif;?>
											</div>
											<div class="calculation-item-body">
												<div class="item-title"><?php echo $v['title'];?> <?php echo json_decode($v['extra_fields'])[2]->value;?> м2</div>
												<!-- <div class="item-description"><?php //echo $v['introtext'];?></div> -->
												<?php
												if(ModK2CategoriesHelper::get_extra_field_value($extra_fields,'Бесплатный сервис') == 2):
												?>
												<div style="position: relative;"><span class="free-service">Бесплатный сервис</span></div>
												<?php else:?>
												<div style="position: relative;"><span></span></div>
												<?php endif;?>
												<div class="item-price"><?php echo ModK2CategoriesHelper::get_extra_field_value($extra_fields,'Цена:');?></div>
												
												<!--<a href="<//?php echo JRoute::_(K2HelperRoute::getItemRoute($v['id'].'-'.$v['alias']));?>" class="readmore light-pink-button-inversion">Подробнее</a>-->
												<a href="<?php printf($cityHref);?>/natyazhnye-potolki/tekhnologii-potolkov" class="readmore light-pink-button-inversion">Подробнее</a>
											</div>
										</div>
									</div>
									<?php endforeach;?>
									</div>
						</div>
								<?php endforeach;?>
							
					</div>
				</div>
		</div>
	</div>
</div>
</div>
<script>
	(function($){
		$(document).ready(function () {
			$('.k2-categories-filter-navigate').first().addClass('active');
			$('.k2-categories-filter-materials').first().addClass('active');
			
			$('.k2-categories-filter-navigate').click(function(){
				var id = jQuery(this).children('a').attr('href');
				$('.k2-categories-filter-navigate').each(function(elem){
					$(this).removeClass('active');
				});
				$(this).addClass('active');
				$('.k2-categories-filter-materials').each(function(elem){
					$(this).removeClass('active');
				});
				///alert(id);
				
				$(id).addClass('active');
			});
			
		});
	})(jQuery);
</script>

<script>
	function windowSize(){
		if ($(window).width() <= '980'){
			$('.mobile-slider').slick({
			  dots: true,
			  infinite: false,
			  speed: 300,
			  slidesToShow: 1,
			  slidesToScroll: 1
			});
		}
	}
	//$(window).load(windowSize); // при загрузке
	//$(window).resize(windowSize); // при изменении размеров
	// или "два-в-одном", вместо двух последних строк:
	$(window).on('load resize',windowSize);
	
	
</script>