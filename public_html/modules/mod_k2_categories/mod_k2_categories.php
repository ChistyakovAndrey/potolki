<?php
defined('_JEXEC') or die('Restricted access');
require_once __DIR__ . '/helper.php';
$categories = ModK2CategoriesHelper::getList($params);
if(class_exists('McsData')) $cityHref = McsData::get('city');
require JModuleHelper::getLayoutPath('mod_k2_categories', $params->get('layout', 'default'));
?>
