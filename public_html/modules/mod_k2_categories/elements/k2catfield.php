<?php
defined('_JEXEC') or die('Restricted access');
 
jimport('joomla.form.formfield');
 
class JFormFieldK2CatField extends JFormField {
	protected $type = 'K2CatField';
	protected function getInput() {
		$db = JFactory::getDBO();
		$query = 'SELECT m.* FROM #__k2_categories m WHERE trash = 0 ORDER BY parent, ordering';
		$db->setQuery($query);
		$mitems = $db->loadObjectList();
		$children = array();
		if ($mitems) {
			foreach ($mitems as $v) {
				$v->title = $v->name;
				$v->parent_id = $v->parent;
				$pt = $v->parent;
				$list = @$children[$pt] ? $children[$pt] : array();
				array_push($list, $v);
				$children[$pt] = $list;
			}
		}
		$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0);
		$mitems = array();
		$mitems[] = JHTML::_('select.option', '0', JText::_('JALL'));
		foreach ($list as $item) {
			$item->treename = JString::str_ireplace('&#160;', ' -', $item->treename);
			$mitems[] = JHTML::_('select.option', $item->id, $item->treename);
		}
		$attributes = 'class="inputbox"';
		$attribute = K2_JVERSION == '25' ? $this->element->getAttribute('multiple') : $this->element->attributes()->multiple;
			if ($attribute) {
				$attributes .= ' multiple="multiple" size="10"';
			}

		$fieldName = $this->name;
		return JHTML::_('select.genericlist', $mitems, $fieldName, $attributes, 'value', 'text', $this->value);
	}
}