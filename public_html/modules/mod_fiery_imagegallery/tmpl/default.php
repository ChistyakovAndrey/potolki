<?php 
$dir = array();
if($item->gallery !== "" && $item->gallery !== "NULL" && $item->gallery !== null  && !empty($item->gallery)){
$id_gallery = explode('{/gallery}',explode("{gallery}",$item->gallery)[1])[0];
$dir = array();
$labels = JPATH_BASE.'/media/k2/galleries/'.$id_gallery.'/ru-RU.labels.txt';
if(file_exists($labels)){ 
$lines = file($labels); 
foreach($lines as $line){ 
$tmp = explode('|',$line); 
$tmp_arr = array('file'=>$tmp[0],'title'=>$tmp[1],'description'=>$tmp[2]); 
array_push($dir,$tmp_arr); 
} 
}
}
?>
<style>
.ceilings-photo-section img{
  margin-bottom:30px;
}

</style>

<div class="ceilings-photo-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 ceilings-photo-section-title">
				<h3>Фото натяжных потолков</h3>
			</div>
		</div>
		<div class="row mobile-slider slick-arrows-white">
			<?php $counter = 0;?>
			<?php foreach($dir as $image):?>
			<?php $counter++;?>
			<?php if($counter <= $param->num_to_home):?>
			<div class="col-sm-6 col-md-4">
				<img src="<?php echo '/media/k2/galleries/'.$id_gallery.'/'.$image['file']?>" width="370" height="300" alt="" id="ch<?php echo $counter;?>" class="">
			</div>
			<?php endif;?>
			<?php endforeach;?>
		</div>
		<div class="row">
			<div class="col-xs-12 all-photos">
				<?php $url = JRoute::_('index.php?option=com_k2&view=item&id='.$item->id);?>
				<a href="<?php echo $url?>" class="button light-pink-button">Все фото</a>
			</div>
		</div>
	</div>
</div>