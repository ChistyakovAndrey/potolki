<?php
defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_fiery_imagegallery');
$param = json_decode($module->params);
$item = ModFieryImagegalleryHelper::getMaterial($param->material_id);
//$category = ModFieryImagegalleryHelper::getCategory($item->catid);
$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::base().'modules/'.$module->module . '/assets/fiery_imagegallery.css');
//$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/fiery_imagegallery.js');
require JModuleHelper::getLayoutPath('mod_fiery_imagegallery', $params->get('layout', 'default'));
?>