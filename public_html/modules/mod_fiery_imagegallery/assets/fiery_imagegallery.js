(function($){
	$(document).ready(function () {
		code_constructor();
		$("#phone-number").mask("8(999) 999-9999");
		$(".my_captcha").hide();
		
		
		
	});
	function execute_func(captcha){
		$('.execute').click(function(){	
			remove_classes();
			if(captcha > 0){
				$(".my_captcha").show();
				$('.approveSend').click(function(){
					ajaxExec();
				});	
			}else{
				ajaxExec();
			}
			
		});
	}
	function remove_classes(){
		if($('#result-form').hasClass('error-form')){
			$('#result-form').removeClass('error-form');
		}
		if($('#result-form').hasClass('success-form')){
			$('#result-form').removeClass('success-form');
		}
	}
	function code_constructor(){
		jQuery.ajax({
			type: 'POST',
			url: "?option=com_ajax&module=fiery_collback&format=debug&method=foo",
			data: 'get_params=mail_plugin',
			success: function(data){
				execute_func($.parseJSON(data).captcha);
				//alert($.parseJSON(data).captcha);
	        }
		});
	}
	function ajaxExec(){
		jQuery.ajax({
			type: 'POST',
			url: "?option=com_ajax&module=fiery_collback&format=debug&method=foo",
			data: $("#mail_form").serialize(),
			beforeSend: function(){
				$("#overlay").css('display', 'block');
			},
			success: function(data){
				$("#overlay").css('display', 'none');
				//alert(data);
				if(data.toString() == 'success' ){
					$(".my_captcha").hide();
					$('#result-form').addClass('success-form');
					$('#result-form').html("Ваш запрос успешно отправлен");
				}else{
					if(data.toString() == 'reload_page'){
						location.reload(); 
					}else{
						$(".my_captcha").hide();
						$('#result-form').addClass('error-form');	
						$('#result-form').html("Ошибка: "+data);
					}
					
				}
	            
	        }
		});
	}
})(jQuery);








