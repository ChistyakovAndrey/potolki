<?php
class ModFieryImagegalleryHelper{
	private $paramsMail;
	private $phone;
	private $param;
	public function __construct($input,$param){
		$this->param = json_decode($param);
		
	}
	public static function getMaterial($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__k2_items')
			->where("id = $id");
			
		$db->setQuery($query);
		return $db->loadObject();
	}
	public static function getCategory($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__k2_categories')
			->where("id = $id");
			
		$db->setQuery($query);
		return $db->loadObject();
	}
	public function get_params($type){
		if($type == 'module'){
			echo json_encode($this->param);
		}
	}
	public static function fooAjax(){
		$module = JModuleHelper::getModule('mod_fiery_collback');
		$params = $module->params;
		$input  = JFactory::getApplication()->input;
		$helper = new ModFieryCollbackHelper($input, $params);
	}
	private function check_captcha($input){
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$params = array(
		    'secret' => $this->paramsMail->secret_key,
		    'response' => $input->getString('g-recaptcha-response')
		);
		$result = file_get_contents($url, false, stream_context_create(array(
		    'http' => array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded',
		        'content' => http_build_query($params)
		    )
		)));

		return $result;
	}
	public function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
}

?>