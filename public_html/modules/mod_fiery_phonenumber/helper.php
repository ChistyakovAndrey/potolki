<?php
class ModFieryPhoneNumberHelper
{
   public static function getParams($template)
    {
        $db = JFactory::getDbo();
        
        $query = $db->getQuery(true);
        
        $query->select($db->quoteName('params'))
                ->from($db->quoteName('#__template_styles'))
                ->where($db->quoteName('template').' = '. $db->quote($template));
        
        $db->setQuery($query);
        return json_decode($db->LoadResult());
    } 
}
?>