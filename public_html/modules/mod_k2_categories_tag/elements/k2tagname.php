<?php

defined('_JEXEC') or die('Restricted access');
JFormHelper::loadFieldClass('list');

class JFormFieldK2TagName extends JFormFieldList {
 
	
	protected $type = 'K2TagName';
 
	protected function getOptions() {
		
		$parent = parent::getOptions();
		
		$opt = $this->getAttribute('option');
		
		$options = array();
		if(!empty($parent)) {
			foreach($parent as $option) {
				array_push($options, $option);
			}
		}
		
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id AS value, name AS text')
			->from('#__k2_tags')
			->where("published = 1");
			
		$db->setQuery($query);
		
		try
		{
			$row = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->unqueueMessage($e->getMessage,'error');
		}
		
		if ($row)
		{
			for($i = 0;$i<count($row);$i++)
			{
				array_push($options,$row[$i]);
			}
		}
		
		return $options;
	}
}