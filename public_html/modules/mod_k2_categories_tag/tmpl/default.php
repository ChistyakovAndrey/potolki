<?php
defined('_JEXEC') or die('Restricted access');
$template = 'fieryflash_template';
JLoader::register('Fieryflash', JPATH_THEMES.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'functions.php');
$city = Fieryflash::get_city_name(true);
?>


<div class="gray-section">
<div class="mod_k2_cetegories mod_k2_cetegories_tags">
	<div class="container">
		<div class="row">
			<div class="col-md-12 calculation-section-title">
				<h3>Натяжные потолки цены за 1 м2 с установкой в <?php echo $city;?></h3>
			</div>
		</div>
		<div class="row">
				<div class="tabs-wrapper">
					<ul class="nav nav-tabs" id="static-tabs">
					<?php foreach ($categories as $key => $category): ?>
					  <li class="k2-categories-tag-navigate">
					  	<a data-toggle="tab" href="#staticTabsTags<?php echo $category->id; ?>"><?php echo $category->title; ?></a>
					  </li>
					<?php endforeach; ?>
					</ul>
					<div class="tab-content">
					
							<?php foreach($categories as $key => $value): ?>
								<div id="staticTabsTags<?php echo $value->id;?>" class="tab-pane fade in k2-categories-tag-materials">
								<div class="row-flex mobile-slider slick-arrows-white">
								<?php foreach($value->materials as $k => $v): ?>
								<?php $extra_fields = json_decode($v['extra_fields']);?>
									<div class="col-md-4">
										<div class="calculation-item-<?php echo $v['id'];?>">

										<?php
										$dir = array();
										if($v['gallery'] !== "" && $v['gallery'] !== "NULL" && $v['gallery'] !== null  && !empty($v['gallery'])){
											$id_gallery = explode('{/gallery}',explode("{gallery}",$v['gallery'])[1])[0];
											$dir = array();
											$labels = JPATH_BASE.'/media/k2/galleries/'.$id_gallery.'/ru-RU.labels.txt';
											$lines = file($labels);
											foreach($lines as $line){
												$tmp = explode('|',$line);
												$tmp_arr = array('file'=>$tmp[0],'title'=>$tmp[1],'description'=>$tmp[2]); 
												array_push($dir,$tmp_arr);
											}
										}
										?>
											<div class="item-discount"><?php echo json_decode($v['extra_fields'])[1]->value;?> <?php echo ModK2CategoriesTagHelper::get_extra_field_value($extra_fields,'Лейбл');?></div>

											<div class="item-image">
											<?php if(count($dir) < 1):?>
												<img src="<?php echo $v['image'];?>" alt="" id="ch0" class="">
											<?php else:?>
												<?php $counter = 0;?>
												<?php foreach($dir as $image):?>
													<?php $counter++;?>
														<img src="<?php echo '/media/k2/galleries/'.$id_gallery.'/'.$image['file']?>" alt="" id="ch<?php echo $counter;?>" class="">
													
													<?php endforeach;?>
												<?php endif;?>
											</div>
											<div class="calculation-item-body">
												<div class="item-title"><?php echo $v['title'];?> <?php echo json_decode($v['extra_fields'])[2]->value;?> м2</div>
												<div class="item-description"><?php echo $v['introtext'];?></div>

												<div class="item-price">
													<?php if(ModK2CategoriesTagHelper::get_extra_field_value($extra_fields,'Цена за 1 м2') > 0){
															echo ModK2CategoriesTagHelper::get_extra_field_value($extra_fields,'Цена за 1 м2');
														}else{
															echo "0";
															}?>
													руб.</div>
												
												<!--<a href="<//?php echo JRoute::_(K2HelperRoute::getItemRoute($v['id'].'-'.$v['alias']));?>" class="readmore light-pink-button-inversion">Калькулятор</a>-->
												<a href="<?php printf($cityHref);?>/natyazhnye-potolki/kalkulyator-potolkov" class="readmore light-pink-button-inversion">Калькулятор</a>
											</div>
										</div>
									</div>
									<?php endforeach;?>
									</div>
						</div>
								<?php endforeach;?>
							
					</div>
				</div>
		</div>
	</div>
</div>
</div>
<script>
	(function($){
		$(document).ready(function () {
			$('.k2-categories-tag-navigate').first().addClass('active');
			$('.k2-categories-tag-materials').first().addClass('active');
			
			$('.k2-categories-tag-navigate').click(function(){
				var id = jQuery(this).children('a').attr('href');
				$('.k2-categories-tag-navigate').each(function(elem){
					$(this).removeClass('active');
				});
				$(this).addClass('active');
				$('.k2-categories-tag-materials').each(function(elem){
					$(this).removeClass('active');
				});
				//alert($(id).length);
				
				$(id).addClass('active');
			});
			
		});
	})(jQuery);
</script>

<script>
	function windowSize(){
		if ($(window).width() <= '980'){
			$('.mobile-slider').slick({
			  dots: true,
			  infinite: false,
			  speed: 300,
			  slidesToShow: 1,
			  slidesToScroll: 1
			});
		}
	}
	//$(window).load(windowSize); // при загрузке
	//$(window).resize(windowSize); // при изменении размеров
	// или "два-в-одном", вместо двух последних строк:
	$(window).on('load resize',windowSize);
</script>