<?php
defined('_JEXEC') or die('Restricted access');
class ModK2CategoriesTagHelper {
	public static function getList($params) {
		

		$result = array();
		$ids = $params->get('tags');
		//self::prea($params);
		return self::getTags($ids,$params);
	}
	protected static function prea($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
	protected static function getTags($ids,$params){
		
		$result = array();
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select(array('id','name'));
		$query->from($db->quoteName('#__k2_tags'));
		if ($ids) {
			$query->where($db->quoteName('id').' IN ('.implode(',', $ids).')');
		}
		$query->where($db->quoteName('published').' = '.$db->quote(1));
		$db->setQuery($query);
		//self::prea($db->loadObjectList('id'));
		$result = self::prepareTags($db->loadObjectList('id'), $params);
		return $result;
	}
	protected static function prepareTags ($rows, $params){
		$result = array();
		require_once (JPATH_SITE.'/components/com_k2/helpers/route.php');
		$unsets = array();
		foreach ($rows as $id => $row) {
			$materials = self::getMaterials($row->id,$params->get("limit_output"));
			$row->{"title"} = "";
			$row->{"image"} = "";
			$row->{"link"} = "";
			$row->{"alias"} = "";
			$row->title = $row->name;
			$row->image = 'media/k2/categories/'.$row->image;
			if (JFile::exists($row->image)) {
				$row->image = '/'.$row->image;
			}
			else {
				$row->image = false;
			}
			$row->link = JRoute::_(K2HelperRoute::getCategoryRoute($row->id.':'.urlencode($row->alias)));
			$row->childs = self::getChilds($rows,$id);
			$row->child = count($row->childs);
			$row->materials = $materials;
			
			$unsets = array_merge($unsets, array_keys($row->childs));
			$rows[$id] = $row;
			
		}
		foreach ($unsets as $key) {
			unset($rows[$key]);
		}
		$result = $rows;	
		return $result;
		
	}
	protected static function getMaterials($id,$limit){
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select(array('ki.*'));
		$query->from($db->quoteName('#__k2_items','ki'));
		$query->join('LEFT', $db->quoteName('#__k2_categories', 'kc') . ' ON (' . $db->quoteName('kc.id') . ' = ' . $db->quoteName('ki.catid') . ')');
		$query->join('LEFT', $db->quoteName('#__k2_tags_xref', 'ktx') . ' ON (' . $db->quoteName('ktx.itemID') . ' = ' . $db->quoteName('ki.id') . ')');
		$query->join('LEFT', $db->quoteName('#__k2_tags', 'kt') . ' ON (' . $db->quoteName('kt.id') . ' = ' . $db->quoteName('ktx.tagID') . ')');
		$query->where($db->quoteName('kt.id').' = '.$id);
		$query->setLimit($limit);
		$db->setQuery($query);
		return $db->loadAssocList();
	}
	protected static function getChilds ($rows, $parent){
		$result = array();
		foreach ($rows as $id => $row) {
			$row->{"parent"} = "";
			if ($row->parent == $parent) {
				$result[$id] = $row;
			}
		}
		return $result;	
	}
	protected static function get_extra_fields_by_name($name){
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from($db->quoteName('#__k2_extra_fields'));
		$query->where($db->quoteName('name').' = "'.$name.'"');
		$db->setQuery($query);
		return $db->loadAssocList();
	}
	public static function get_extra_field_value($arr,$name){
		$ids = self::get_extra_fields_by_name($name);
		$result = "";
		foreach($ids as $id){
			foreach($arr as $value){
			if($value->id == $id['id']){
				$result = $value->value;
			break;
				}
			}
		}
		return $result;
	}
	protected static function getIDs ($categories){
		
		$db = JFactory::getDBO();
		if (!is_array($categories)){
			$categories = array($categories);
		}
		JArrayHelper::toInteger($categories);
		$categories = array_unique($categories);
		$array = $categories;
		while (count($array)) {
			$query = $db->getQuery(true);
			$query->select('id');
			$query->from($db->quoteName('#__k2_categories'));
			$query->where($db->quoteName('parent').' IN ('.implode(',', $array).')');
			$query->where($db->quoteName('id').' NOT IN ('.implode(',', $array).')');
			$db->setQuery($query);
			$array = $db->loadColumn();
			$categories = array_merge($categories, $array);
		}
		JArrayHelper::toInteger($categories);
		$categories = array_unique($categories);
		return $categories;
	}
	protected static function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
}
