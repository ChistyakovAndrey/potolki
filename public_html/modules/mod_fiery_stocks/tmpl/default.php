<?php
defined('_JEXEC') or die;
$class = $params->get('stocks-style');
?>

<div class="row groups row-flex <?php echo $class; ?>">
	<?php foreach($param['services-counter-info'] as $value):?>
		<div class="col-xs-12 col-md-6">
			<div class="group-item">
				<span class="icon-calculations" style="background: url(<?php echo $value['stocks-info-slide-icon'];?>) no-repeat top center"></span>
				<div class="group-item-title"><?php echo $value['stocks-info-slide-title'];?></div>
				<div><?php echo $value['stocks-info-slide-description'];?></div>
				<a href="<?php echo $value['stocks-info-slide-link'];?>" title="<?php echo $value['stocks-info-slide-title'];?>"></a>
			</div>
		</div>
	<?php endforeach;?>
</div>