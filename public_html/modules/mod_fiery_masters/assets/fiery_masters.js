(function($){
	$(document).ready(function () {
		var tool_id;
		var master_id;
		$('.select-equip .selector').first().addClass('active');
		$('.select-masters .selector').first().addClass('active');
		
		equip_option = $('.select-equip .selector').filter(function( index ){
		    return $(this).hasClass('active');
		  }).attr('data-equip');
		master_id = $('.select-masters .selector').filter(function( index ){
		    return $(this).hasClass('active');
		  }).attr('data-master');

		get_photo_master(equip_option,master_id);
		

		$('.select-equip .selector').click(function(){
			$(this).siblings().each(function(elem){
				$(this).removeClass('active');
			});
			$(this).addClass('active');
			if(equip_option !== $(this).attr('data-equip')){
				
				equip_option = $(this).attr('data-equip');
				
				get_photo_master(equip_option,master_id);
			}
			
		});

		$('.select-masters .selector').click(function(){
			
			$(this).siblings().each(function(elem){
				$(this).removeClass('active');
			});
			$(this).addClass('active');
			//alert($(this).attr('data-master'));
			if(master_id !== $(this).attr('data-master')){
				master_id = $(this).attr('data-master');	
				//get_photo_master(tool_id,master_id);	
			}
			get_photo_master(equip_option,master_id);
		});
		//$('.master-images').css({"width":"640px","height":"480px","overflow":"hidden"});
					
		function get_photo_master(equip_option,master_id){
			
					$('#jsv-holder').empty();	

			//alert("Шляпа");
			//alert(equip_option+'---'+master_id);
			console.log(master_id);
			console.log(equip_option);
			$.ajax({
				type: 'POST',
				url: "?option=com_ajax&module=fiery_masters&format=debug&method=foo",
				data: 'get_photo_master=get_photo_master&master_id='+master_id+'&equip_option='+equip_option,
				success: function(response){
					//alert(response);
					//console.log(response);

					var result = $.parseJSON(response);
					/*console.log(result.img_path);
					console.log(result.name);
					console.log(result.page_url);
					console.log(result.extra_data);*/

					$('.master_name').text(result.name);
					$('.master_page_url').text(result.page_url);
					$('.master_extra_data').text(result.extra_data);
					 $("<img id='jsv-image' alt='example' src='"+result.img_path+"'>").appendTo('#jsv-holder');
					 const viewer = new JavascriptViewer({
			            mainHolderId: 'jsv-holder',
			            mainImageId: 'jsv-image',
			            totalFrames: result.count_photos,
			         });
			        
			         viewer.start();
					
					
		        }
			});
		
		};
		
		
	
	
	});
})(jQuery);