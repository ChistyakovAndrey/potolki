<?php
error_reporting(0);
defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';
jimport('joomla.application.module.helper');
$param = json_decode($module->params);
$module = JModuleHelper::getModule('mod_fiery_masters');

$mailPlg = JPluginHelper::getPlugin( 'mail', 'mail' );
$paramsMail = json_decode($mailPlg->params);
$doc = JFactory::getDocument();
if(class_exists('McsData')) $cityHref = McsData::get('city');
$doc->addScript('https://www.google.com/recaptcha/api.js?hl=ru');
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/jquery.maskedinput.min.js');
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/JavascriptViewer.min.js');
$doc->addStyleSheet(JURI::base().'modules/'.$module->module . '/assets/fiery_masters.css');
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/fiery_masters.js');

/*УСЛОВИЕ ПОДГРУЗКИ СКРИПТОВ ДЛЯ ФОТОГАЛЕРЕИ В ЗАВИСИМОСТИ ОТ НАСТРОЙКИ МОДУЛЯ*/
if($param->photogallery){
	$doc->addStyleSheet(JURI::base().'modules/'.$module->module . '/assets/Blueimp/css/blueimp-gallery.min.css');
	$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/jquery-1.11.0.min.js');
	$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/Blueimp/js/blueimp-gallery.min.js');
}

require JModuleHelper::getLayoutPath('mod_fiery_masters', $params->get('layout', 'default'));

?>