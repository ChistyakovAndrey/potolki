<?php
error_reporting(0);
class ModFieryMastersHelper{
	public $masters = array();
	public $tools = array();
	
	public function __construct($input=""){
		
		$photogallery = json_decode(JModuleHelper::getModule('mod_fiery_masters')->params)->photogallery;
	
		
		$this->masters = $this->getData('masters');
		//$this->tools = $this->getData('tools');
		//echo '<pre>'.print_r($this->masters).'</pre>';
		
		
		if($input !== "" && $input->get('get_photo_master')=='get_photo_master'){
			$this->get_master_photos($input->get('master_id'),$input->get('equip_option'));
		}
	}
	
	public function get_master_photo_url($master_id,$equip_option){
		//echo $master_id;
		//$img_path =  $this->get_master_and_tool_info($master_id,$equip_option)->path;
		//$master = $this->get_master_info($master_id)->id;
		$master_photos = $this->get_master_photos($master_id,$equip_option)->id;
		//$tool =  $this->get_tool_info($tool_id)->id;
		//setcookie("MASTER", $img_path->parent_id, time() + 3600,"/","",0,0);
		

		//echo $img_path;
		

		//echo json_encode(['path'=>$db->loadObject()->path]);
	}
	public function get_master_photos($master_id,$equip_option){
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('mmp.path AS path', 'mm.name AS name', 'mm.page_url AS page_url', 'mm.extra_data AS extra_data'));
		$query->from($db->quoteName('#__masters_masters_photos', 'mmp'));
		$query->join('LEFT', $db->quoteName('#__masters_masters', 'mm') . ' ON ' . $db->quoteName('mm.id') . ' = ' . $db->quoteName('mmp.parent_id'));
		
		$query->where('mmp.parent_id='.$master_id);
		$query->where('mmp.equip="'.$equip_option.'"');
		$query->setLimit(1);
		$db->setQuery($query);
		//echo $query;
		$master_photos = $db->loadAssoc();
		//$result = array();
		$count_photos = count(scandir(dirname($master_photos['path'])))-3;
		$result = array(
			'master_id'=>$master_id, 
			'img_path'=>$master_photos['path'],
			'name'=>$master_photos['name'],
			'page_url'=>$master_photos['page_url'],
			'extra_data'=>$master_photos['extra_data'],
			'count_photos' => $count_photos
		);
		//echo count($master_photos);
		//for($i = 0;$i < count($master_photos);$i++) {
		//	array_push($result['img_path'],$master_photos[$i]['path']);
			//echo $this->pre($master_photos[$master_id['path'][$i]]);
		//}
		
		echo json_encode($result);
	}
	public function get_master_and_tool_info($master_id,$equip_option){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__masters_masters_photos');
		$query->where('parent_id='.$master_id);
		$query->where('equip='.$equip_option);
		$query->where('state=1');
		//echo $query;
		$query->setLimit(1);
		$db->setQuery($query);
		return $db->loadObject();
	}
	public function get_master_info($master_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__masters_masters');
		$query->where('id='.$master_id);
		$db->setQuery($query);
		return $db->loadObject();
	}
	
	public function get_tool_info($tool_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__masters_tools');
		$query->where('id='.$tool_id);
		$db->setQuery($query);
		return $db->loadObject();
	}
	public static function fooAjax(){
		$module = JModuleHelper::getModule('mod_fiery_masters');
		/*//$params = $module->params;*/
		$input  = JFactory::getApplication()->input;
		$helper = new ModFieryMastersHelper($input);
	}
	public function getData($table){
		$result = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__masters_'.$table);
		$query->where('state=1');
		$db->setQuery($query);
		$row = $db->loadAssocList();
		for($i = 0;$i < count($row);$i++) {
			array_push($result,$row[$i]);
			$result[$i]['photos'] = array();
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__masters_'.$table.'_photos');
			$query->where('parent_id='.$row[$i]['id']);
			$db->setQuery($query);
			$row1 = $db->loadAssocList();
			for($a = 0;$a < count($row1);$a++) {
			array_push($result[$i]['photos'],$row1[$a]);
			}
		}
		return $result;
	}


	/* БЛОК ДЛЯ ПОКАЗА ФОТОГАЛЕРЕИ*/
	public function sort_photogallery_albums($module_id){
		$albums = $this->get_photogallery();
		//echo $module_id;
		$result = array();
		foreach($albums as $value){
			//$this->pre(explode(',', $value['parent_ids']));
			foreach(explode(',', $value['parent_ids']) as $key => $id){
				if($id == $module_id){
					$photos = $this->get_photos($value['id']);
					$value['photos'] = array();
					$value['photos'] = $photos;
					array_push($result, $value);
				}
			}
		}
		require_once 'tmpl/photogallery.php';
		
	}
	public function get_photos($parent_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__photogallery_photos');
		$query->where('parent_id='.$parent_id);
		$query->where('state=1');
		$query->order('position ASC');
		$db->setQuery($query);
		return $db->loadAssocList();
	}
	public function get_photogallery(){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__photogallery_photo_albums');
		$query->where('state=1');
		$query->order('position ASC');
		$db->setQuery($query);
		return $db->loadAssocList();
	}
	/* КОНЕЦ БЛОК ДЛЯ ПОКАЗА ФОТОГАЛЕРЕИ*/



	public function pre($arr){
	echo "<pre>";
		print_r($arr);
	echo "</pre>";
	
	}
}
$obj = new ModFieryMastersHelper();

/*УСЛОВИЕ В ЗАВИСИМОСТИ ОТ НАСТРОЙКИ МОДУЛЯ (см. в параметрах модуля)*/
if(json_decode(JModuleHelper::getModule('mod_fiery_masters')->params)->photogallery){
	$obj->sort_photogallery_albums($module->id);
}

?>