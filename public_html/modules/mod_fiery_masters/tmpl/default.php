<?php defined('_JEXEC') or die; ?>

<div class="master-section hidden-xs">
	<div class="container">
		<div class="row">
			<div class="col-md-12 master-section-title">
				<h3>Соберите специалиста, который приедет к вам1</h3>
				Здесь небольшой поясняющий текст
			</div>
		</div>
		<div class="row">
			<div class="master_name"></div>
			<div class="master_page_url"></div>
			<div class="master_extra_data"></div>
			<form action="">
				<div class="form-select">
					
					<div class="equip">
							<div class="text">Выберите вид:</div>
							<div class="select-equip">
								<div class="selector" data-equip="true" >
									<span class="select-equip">Equip</span>
								</div>
								<div class="selector" data-equip="false">
									<span class="select-equip">Not equip</span>
								</div>
							</div>
							
						</div>
					<!-- кнопки выбора мастера начало -->
					<div style="position: relative;">
						<div class="masters">
							<div class="text">Выберите специалиста:</div>
							<div class="select-masters">
							<?php foreach($obj->masters as $key=>$value):?>
								<div class="selector" data-master='<?php echo $value['id'];?>' style="background: url(<?php echo $value['icon_href'];?>) no-repeat center 30px #000;">
									<span class="select-master"><?php echo $value['name'];?></span>
								</div>
							<?php endforeach;?>
							</div>
						</div>
						<a class="execute-master light-pink-button" href="<?php echo $cityHref."/natyazhnye-potolki/vyzvat-zamershchika"?>">Вызвать замерщика</a>
						<!--<input type="button" class="execute-master light-pink-button" value="Вызвать замерщика">-->
					</div>
					<!-- кнопки выбора мастера конец -->
				</div>
				
			</form>

		</div>
	</div>
	
	<div class="master-images" id="jsv-holder">
	</div>
</div>
<style>
	.master-images img{
		max-width: 300px ;
	}
</style>