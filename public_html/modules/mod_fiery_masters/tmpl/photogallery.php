<?php if(count($result) > (int)1):?>
<?php foreach($result as $album):?>
	<div class="tab">
		<div class="tab_label"><h3><?php printf($album['name'])?></h3></div>
		<div class="tab_desc"><?php printf($album['description'])?></div>
	<?php foreach($album['photos'] as $photo):?>
		<div class="photogallery_preview_photos">
      <a class="photogallery_preview_photo" href="/<?php printf($photo['path']);?>" title="<?php printf($photo['name']);?>">
        <img src="/<?php printf($photo['path']);?>" width="120px" />
      </a>
		</div>
	<?php endforeach;?>
	</div>
<?php endforeach;?>
<?php else: ?>
	<?php foreach($result[0]['photos'] as $photo):?>
		<div class="photogallery_preview_photos">
      <a class="photogallery_preview_photo" href="/<?php printf($photo['path']);?>" title="<?php printf($photo['name']);?>">
        <img src="/<?php printf($photo['path']);?>" width="120px" />
      </a>
		</div>
	<?php endforeach;?>
<?php endif;?>
<div id="blueimp-gallery" class="blueimp-gallery">
  <div class="slides"></div>
  <h3 class="title"></h3>
  <a class="prev">‹</a>
  <a class="next">›</a>
  <a class="close">×</a>
  <a class="play-pause"></a>
  <ol class="indicator"></ol>
</div>
<script>
	$(document).ready(function(){
	 $('.photogallery_preview_photos').click(function(event) {
	    event = event || window.event
	    var link = $('.photogallery_preview_photos').index(this),
	    options = {index: link, event: event, youTubeClickToPlay: false},
	    links = $('.photogallery_preview_photos .photogallery_preview_photo').toArray()
	    blueimp.Gallery(links, options)
  	})
  });
</script>