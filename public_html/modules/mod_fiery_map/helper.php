<?php
class ModFieryMapHelper{
	private $paramsMail;
	private $phone;
	private $param;
	private $errors = array();
	public function __construct($input){
		$this->param = json_decode($input->getString('parameters'));
		jimport('mail');
		
		if($input->getString($this->param->form_id) !== null){
				$mailPlg = JPluginHelper::getPlugin( 'mail', 'mail' );
				$this->paramsMail = json_decode($mailPlg->params);
				$form_structure = self::fields_format($this->param);
				ob_start();
				require_once "tmpl/mail_tmpl.php";
				$mail_body = ob_get_contents();
				ob_end_clean();
				$result_captcha = json_decode($this->check_captcha($input));
				
				if($this->param->captcha == true){
					if($result_captcha->success > 0){
						MailHelper::send_mailer(
							$mailPlg->params,
							$this->param->smtp_mail_header,
							$mail_body
							);
					}else{
						printf('reload_page');
					}
				}else{
					MailHelper::send_mailer(
							$mailPlg->params,
							$this->param->smtp_mail_header,
							$mail_body
						);
				}
		}
		
	}
	public function get_params($type){
		if($type == 'module'){
			echo json_encode($this->param);
		}
	}

	public static function fields_format($param){
		if(class_exists('McsData')) $city_id = McsData::get('cityId');
		$result_array = array();
		$parameters = (array)$param;
		$cities = (array)$parameters['form-cities'];
		$counter = 0;
		foreach($cities as $key=>$city){
			if($city_id == $city->cities){
				array_push($result_array, ['city'=>$city->cities]);
				$count_point = 0;
				$result_array[$counter]['sales_points'] = array();
				foreach($city->sales_points as $k=>$points){
					$result_array[$counter]['sales_points'] = (array)$result_array[$counter]['sales_points'];
					array_push($result_array[$counter]['sales_points'], ['latitude'=>$points->latitude,
																		 'longitude'=>$points->longitude,
																		 'text_hover'=>$points->text_hover,
																		 'worktimes'=>$points->worktimes,
																		 'telephone'=>$points->telephone,
																		 'text_click'=>$points->text_click]);
					$count_point++;
				}
			}else{
				continue;
			}
			$counter++;
			}
		
		
		if($counter < 1){
			return false;
		}else{
			return $result_array;
		}
		
	}
	private function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
}

?>