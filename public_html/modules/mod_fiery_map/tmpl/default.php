<?php defined('_JEXEC') or die; ?>

<?php if($param->moduletitle > 0):?>
<div class="fiery_custom_map_title">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h3 >Офисы продаж в <?php echo $city;?></h3>
			</div>
		</div>
	</div>
</div>
<?php endif;?>

<?php if($city_map_properties==true):?>
<div class="fiery_custom_map_wrap">
	<div id="fiery_custom_map" class="fiery_custom_map"></div>

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="map_example_div">
					<?php $total_points = 1;?>

					<?php foreach(json_decode($city_map_properties)[0]->sales_points as $point_of_sale):?>
						<div class="store">
							<div class="h3"><?php echo $point_of_sale->text_click;?></div>
							<div class="worktimes"><span><?php echo $point_of_sale->worktimes;?></span></div>
							<div class="phone"><span><?php echo $point_of_sale->telephone;?></span></div>
							<?php $total_points++;?>
						</div>
					<?php endforeach;?>

				</div>
			</div>
		</div>
	</div>

</div>
<?php else:?>
<div id="fiery_custom_map_empty" class="fiery_custom_map_empty">
	<div class="maps">
		<div class="maps-info">В Вашем городе нет представителей компании<br/>
		<a href="/o-kompanii/stat-predstavitelem" class="button-new-branch">Стать представителем</a></div>
	</div>
</div>
<?php endif;?>
<script>
	(function($){
	google.maps.event.addDomListener(window, "load", initMap);
	function initMap() {
	var elem;
	var click_address;

	var city_map_properties = '<?php echo $city_map_properties;?>';
	if (city_map_properties != false) {
		var sales_points = $.parseJSON(city_map_properties)[0].sales_points;	
		var coordinates = new google.maps.LatLng(sales_points[0].latitude,sales_points[0].longitude);
		var mapOptions = {
			center: coordinates, 
			zoom: 15,
			disableDefaultUI: true
		};
		var map = new google.maps.Map(document.getElementById('fiery_custom_map'), mapOptions);
		$.getJSON("/modules/mod_fiery_map/assets/map_style_colored.json", function(data) {
			map.setOptions({styles: data});
		});
		var defaultMarkerImage = "";
		var bigMarkerImage = "";
		var markersArray = [];
		defaultMarkerImage = '/modules/mod_fiery_map/images/marker-logo.png';
		bigMarkerImage = '/modules/mod_fiery_map/images/marker-logo-big.png';
		var click_address = "";
		markers(sales_points,defaultMarkerImage,bigMarkerImage,click_address,map,markersArray,"create");	
		
		$('.store').click(function(){
			$('.store').each(function(elem){
				$(this).removeClass('active');
			});
			$(this).addClass('active');
			click_address = $(this).children('.h3').text();
			markers(sales_points,defaultMarkerImage,bigMarkerImage,click_address,map,markersArray,"delete");		
			markers(sales_points,defaultMarkerImage,bigMarkerImage,click_address,map,markersArray,"create");		
		});
		
		function markers(sales_points,defaultMarkerImage,bigMarkerImage,click_address,map,markersArray,flag){
			var markerImage;
			var marker;
			if(flag=="delete"){
				for(var i=0; i<markersArray.length; i++){
			        markersArray[i].setMap(null);
			    }
			}else if(flag=="create"){
				for(var i =0; i < sales_points.length;i++){
					if(click_address != ""){
						if(click_address == sales_points[i].text_hover){
							markerImage = new google.maps.MarkerImage(bigMarkerImage);
						}else{
							markerImage = new google.maps.MarkerImage(defaultMarkerImage);
						}
					}else{
						markerImage = new google.maps.MarkerImage(defaultMarkerImage);
					}
					marker = new google.maps.Marker({
						icon: markerImage,
						position: new google.maps.LatLng(sales_points[i].latitude,sales_points[i].longitude), 
						map: map,
						title:sales_points[i].text_hover
					});
					markersArray.push(marker);
					listenMarker(marker,sales_points[i]);
				}
			}
		}
		function listenMarker (marker,point){
			
			var content = document.createElement('div');
			content.innerHTML = "<div class='maps-object-label-text'>"+point.text_click+"<div class='phone'><span>"+point.telephone+"</span></div></div>";
			var infowindow = new google.maps.InfoWindow({
					content: content
			});
			google.maps.event.addListener(marker, 'click', function() {
				//infowindow.open(map, marker);
				//marker.setIcon(markerImageHover);
			});
			google.maps.event.addListener(marker, 'mouseover', function() {
				infowindow.open(map, marker);
			});
	
			google.maps.event.addListener(marker, 'mouseout', function() {
					infowindow.close(map, marker);
				});
			}
		}
	}
})(jQuery);
	</script>