<?php

defined('_JEXEC') or die('Restricted access');
JFormHelper::loadFieldClass('list');

class JFormFieldCity extends JFormFieldList {
 
	
	protected $type = 'city';
 
	protected function getOptions() {
		
		$parent = parent::getOptions();
		
		$opt = $this->getAttribute('option');
		
		$options = array();
		if(!empty($parent)) {
			foreach($parent as $option) {
				array_push($options, $option);
			}
		}
		
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->select(array('mc.id AS value', 'mcn.name AS text'))
			->from($db->quoteName('#__mycityselector_cities', 'mc'))
			->join('LEFT', $db->quoteName('#__mycityselector_city_names', 'mcn') . ' ON (' . $db->quoteName('mc.id') . ' = ' . $db->quoteName('mcn.city_id') . ')')
			->where("mc.country_id = 1 AND mc.published=1");
			
		$db->setQuery($query);
		try
		{
			$row = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->unqueueMessage($e->getMessage,'error');
		}
		
		if ($row)
		{
			for($i = 0;$i<count($row);$i++)
			{
				array_push($options,$row[$i]);
			}
		}
		
		return $options;
	}
}