<?php
defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';
jimport('joomla.application.module.helper');
$module_name = 'fiery_map';
$module = JModuleHelper::getModule('mod_'.$module_name);
$param = json_decode($module->params);

$template = 'fieryflash_template';
JLoader::register('Fieryflash', JPATH_THEMES.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'functions.php');
$city = Fieryflash::get_city_name(true);

if(class_exists('McsData')) $city_id = McsData::get('cityId');
if(ModFieryMapHelper::fields_format($param) == true){
	$city_map_properties = json_encode(ModFieryMapHelper::fields_format($param),JSON_UNESCAPED_UNICODE);
}else{
	$city_map_properties = false;
}


$doc = JFactory::getDocument();


$doc->addStyleSheet(JURI::base().'modules/'.$module->module . '/assets/'.$module_name.'.css');
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/'.$module_name.'.js');

$doc->addScript('https://maps.googleapis.com/maps/api/js?key='.$param->g_api_key);

require JModuleHelper::getLayoutPath('mod_'.$module_name, $params->get('layout', 'default'));

?>