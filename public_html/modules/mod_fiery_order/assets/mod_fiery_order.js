(function($){

	$(document).ready(function () {
		var parameters = $('.order_measure_form input[name="parameters"]').val();
		var min_time = $.parseJSON(parameters).min_time;
		var max_time = $.parseJSON(parameters).max_time;
		date_order = check_date_param(parameters);

		//alert();

		//alert($.datepicker.formatDate('dd.mm.yy', new Date()));

		//var current_date = 

		//var date_order = ;

		

		$('.measure-address').suggestions({

			token:"47b02b3ad94a7bf0e5f7fffc03563b75dd0193a1",

			type: "ADDRESS",

			onSelect: function(suggestion) {

            	console.log(suggestion);

        	}

		});

		$('.measure-date').datepicker({

				dateFormat: "dd.mm.yy",

				minDate: date_order,

				monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],

				dayNamesMin: ['вс','пн','вт','ср','чт','пт','сб'],

			});

		$('.measure-time').timepicker({

			timeFormat: 'HH:mm',

		    interval: 30,

		    minTime: min_time,

		    maxTime: max_time+':00',

		    defaultTime: min_time,

		    startTime: min_time+':00',

		    dynamic: false,

		    dropdown: true,

		    scrollbar: true

		});

		

		

		

		execute_func(parameters);

		$(".my_captcha").hide();

		

		

		

	});

	function execute_func(parameters){

		var captcha = $.parseJSON(parameters).captcha;

		$('.execute-measure').click(function(){

			var current_form = $(this).parents('.order_measure_form');

			remove_classes(current_form);

			

			if(captcha > 0){

				$(current_form).children('.my_captcha').show();

				$(current_form).children('.approveSend').click(function(){

					ajaxExec(current_form);

				});	

			}else{

				ajaxExec(current_form);

			}

			

		});

	}

	function check_date_param(parameters){

		var date_param_tmp = $.parseJSON(parameters).date_order.split('.');

		var date_param = new Date(date_param_tmp[2]+'-'+date_param_tmp[1]+'-'+date_param_tmp[0]);

		var current_date = new Date();

		var returned_date;

		if (current_date == date_param) {

			returned_date = current_dat;

		} else if(current_date > date_param){

			returned_date = current_date;

		}else{

			returned_date = date_param;

		}

		//alert(returned_date.getDate()+1);
		return returned_date.getDate()+1+'.'+(returned_date.getMonth()+1)+'.'+returned_date.getFullYear();

	}

	function remove_classes(current_form){

		var result_form = current_form.children('.result-form');

		if($(result_form).hasClass('error-form')){

			$(result_form).removeClass('error-form');

		}

		if($(result_form).hasClass('success-form')){

			$(result_form).removeClass('success-form');

		}

	}

	function ajaxExec(current_form){

		var result_form = current_form.children('.result-form');

		jQuery.ajax({

			type: 'POST',

			url: "?option=com_ajax&module=fiery_order&format=debug&method=foo",

			data: $(current_form).serialize(),

			beforeSend: function(){

				$(".overlay").css('display', 'block');

			},

			success: function(data){

				$(".overlay").css('display', 'none');

				if(data.toString() == 'success' ){

					$(current_form).children(".my_captcha").hide();

					$(result_form).addClass('success-form');

					$(result_form).html("Ваш запрос успешно отправлен");

				}else{

					if(data.toString() == 'reload_page'){

						location.reload(); 

					}else{

						$(current_form).children(".my_captcha").hide();

						$(result_form).addClass('error-form');	

						$(result_form).html("Ошибка: "+data);

					}

					

				}

	            

	        }

		});

	}

})(jQuery);

















