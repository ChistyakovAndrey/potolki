<?php
class ModFieryOrderHelper{
	private $paramsMail;
	private $phone;
	private $param;
	private $errors = array();
	public function __construct($input){
		$this->param = json_decode($input->getString('parameters'));
		jimport('mail');
		if($input->getString('order-measure-form') !== null){
			// if(empty($input->getString('measure-address'))) array_push($this->errors,"Укажите адрес");
			// if(empty($input->getString('measure-date')))array_push($this->errors,"Выберите дату");
			// if(empty($input->getString('measure-time')))array_push($this->errors,"Выберите время");
			if(count($this->errors) == (int)"0"){
				$this->phone = $input->getString('phone');
				$mailPlg = JPluginHelper::getPlugin( 'mail', 'mail' );
				$this->paramsMail = json_decode($mailPlg->params);
				$result_captcha = json_decode($this->check_captcha($input));
				ob_start();
				require_once "tmpl/mail_tmpl.php";
				$mail_body = ob_get_contents();
				ob_end_clean();
				if($this->param->captcha == true){
					if($result_captcha->success > 0){
						MailHelper::send_mailer(
							$mailPlg->params,
							$this->param->smtp_mail_header,
							$mail_body
							);
					}else{
						printf('reload_page');
					}
				}else{
					
					MailHelper::send_mailer(
							$mailPlg->params,
							$this->param->smtp_mail_header,
							$mail_body
						);
				}
			}else{
				foreach($this->errors as $value){
					echo $value.'<br/>';
				}
			}
		}
		
	}
	public function get_params($type){
		if($type == 'module'){
			echo json_encode($this->param);
		}
	}
	public static function get_next_date_order($date_order_param){
		$months = array(
			"01" => "января",
			"02" => "февраля",
			"03" => "марта",
			"04" => "апреля",
			"05" => "мая",
			"06" => "июня",
			"07" => "июля",
			"08" => "августа",
			"09" => "сентября",
			"10" => "октября",
			"11" => "ноября",
			"12" => "декабря"
		);
		$date_order_current = explode('.',$date_order_param);
		$date_order_current_format = $date_order_current[2].'-'.$date_order_current[1].'-'.$date_order_current[0];
		$current_date = new DateTime();

		if(date($current_date->format('Y-m-d')) >= date($date_order_current_format)){
			$current_date->modify('+1 day');
			$date_order = $current_date->format('d.m.Y');
		}
		if(date($current_date->format('Y-m-d')) <= date($date_order_current_format)){
	
			$date_order = $date_order_param;
		}
		
		
		$date_order_date = explode(".", $date_order)[0];
		$date_order_month = explode(".", $date_order)[1];
		$date_order_year = explode(".", $date_order)[2];
		
		$date_order = $date_order_date." ".$months[$date_order_month]." ".$date_order_year;
		return $date_order;
	}
	public static function fooAjax(){
		$module = JModuleHelper::getModule('mod_fiery_order');
		$input  = JFactory::getApplication()->input;
		$helper = new ModFieryOrderHelper($input);
	}
	private function check_captcha($input){
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$params = array(
		    'secret' => $this->paramsMail->secret_key,
		    'response' => $input->getString('g-recaptcha-response')
		);
		$result = file_get_contents($url, false, stream_context_create(array(
		    'http' => array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded',
		        'content' => http_build_query($params)
		    )
		)));

		return $result;
	}
}
?>