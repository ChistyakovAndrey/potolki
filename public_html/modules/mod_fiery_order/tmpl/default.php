<?php defined('_JEXEC') or die; ?>
<div class="overlay"></div>

<div class="form-wrap">
	<div class="form-title"><span>Бесплатный замер</span>в <?php echo $city;?></div>
		<div class="collback-form">
				<form class="order_measure_form" action="" method="POST">
				<input type="hidden" name="order-measure-form"/>
				<input type="hidden" name="parameters" value='<?php echo $module->params;?>'/>
<!-- 					<label for="measure-address"></label>
					<input type="text" name="measure-address" class="measure-address"  placeholder="Ваш адрес"> -->
					<label for="measure-date"></label>
					<input type="text" name="measure-date" class="measure-date"  placeholder="Дата замера">
					<label for="measure-time"></label>
					<input type="text" name="measure-time" class="measure-time"  placeholder="Удобное время">
					<label for="measure-phone"></label>
					<input type="tel" name="measure-phone" class="measure-phone"  placeholder="Телефон">
					
					<input type="button" class="execute-measure" value="Вызвать замерщика"/>
					<div class='result-form' class='text-center'></div>
					<?php if($param->captcha == true && !empty($paramsMail->site_key)):?>
					<div class = "my_captcha">
						<div class="g-recaptcha" data-sitekey="<?php printf($paramsMail->site_key);?>"></div>
						<input type="button" class="approveSend" value="Отправить"/>
					</div>
					<?php endif;?>
				</form>
			<span></span>
		</div>			
	<div class="measurement-time">Ближайшее время замера<span><?php echo $date_order;?></span></div>
</div>