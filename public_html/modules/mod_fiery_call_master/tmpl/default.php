<?php defined('_JEXEC') or die; ?>
<div id="overlay"></div>

<?php
$columns = ModFieryCallMasterHelper::fields_format($param);
if(isset($param->master_info) && $param->master_info > 0){
	$master_info = ModFieryCallMasterHelper::get_master_tool_info();
}
?>
<?php if(isset($param->form_check_title) && $param->form_check_title > 0):?>
	<h3 class="text-center"><?php printf($param->form_title);?> 
		<?php if($param->form_check_city > 0):?>
			<?php echo $city;?>
		<?php endif; ?>
	</h3>
<?php endif;?>
<!--<h3 class="text-center"><?php printf($param->form_check_title);?> </h3>-->
<?php if(isset($param->master_info) && $param->master_info > 0):?>
<div class="master_info">
	Мастер: <?php echo $master_info->master_name;?></div></br>
	Инструмент: <?php echo $master_info->tool_name;?></div></br>
	Фото мастера: <img width="100px" src="/<?php echo $master_info->master_photo;?>"/></br>
</div>
<?php endif;?>
<div class="callback-form">
<div class="validate-result"></div>
	<form id="<?php printf($param->form_id);?>" action="" method="POST">
		<input type="hidden" name="<?php printf($param->form_id);?>"/>
		<input type="hidden" name="module_name" value ="<?php printf($module_name);?>"/>
		<input type="hidden" id="form_params" name="parameters" value='<?php echo $module->params;?>'/>
		
		<div class="row">
		<?php foreach($columns as $form_fields):?>
			<div class="col-sm-6 input-fields">
				<?php foreach($form_fields as $field):?>
				<label for="<?php printf($field['name']);?>" class="label_<?php printf($field['name']);?>"></label>
					<?php if($field['type'] == 'textarea'):?>
						<textarea 
							name="<?php printf($field['name']);?>" 
							class="<?php printf($field['name']);?>" 
							id="<?php printf($field['name']);?>" 
							required="<?php printf(($field['required'] == 'true') ? "required" : "");?>"
							placeholder="<?php printf($field['placeholder']);?>"></textarea>
					<?php else:?>
						<input 
							type="<?php printf(($field['type'] == 'phone' || $field['type'] == 'calendar' || $field['type'] == 'address') ? "text" : $field['type']);
							?>" 
							name="<?php printf($field['name']);?>" 
							class="
							<?php 
							switch($field['type']){
								case 'phone' : printf($field['name']." phone-mask"); break;
								case 'calendar' : printf($field['name']." calendar"); break;
								case 'time' : printf($field['name']." time"); break;
								case 'address' : printf($field['name']." address"); break;
								case 'email' : printf($field['name']." email"); break;
								default : printf($field['name']); break;
							}
							?>
							
							<?php foreach($field['classes'] as $class):?>
								<?php printf(" ".$class." ");?>
							<?php endforeach;?>
							"
							id="<?php printf($field['name']);?>" 
							required="<?php printf(($field['required'] == 'true') ? "required" : "");?>" 
							placeholder="<?php printf($field['placeholder']);?>"/>
					<?php endif;?>
				<?php endforeach;?>
			
			</div>
			<?php endforeach;?>
		</div>

		<div class="row">
		<?php if($param->private_info > 0):?>
			<div class="col-sm-6">
				<div class="personal-info-wrapper">
					<input type="checkbox" name="personal-info" class="personal-info" id="personal-info"/>
					<label for="personal-info">Я согласен на обработку персональных данных и получение уведомлений.</label>
				</div>
			</div>
		<?php endif;?>
			<div class="col-sm-6">
				<div class="execute" value="<?php printf($param->button_text);?>"><?php printf($param->button_text);?></div>
			</div>
		</div>
		<div class='result' class='text-center'></div>
		
		<?php if($param->captcha == true):?>
			<div class = "my_captcha">
				<div class="g-recaptcha" data-sitekey="<?php printf($paramsMail->site_key);?>"></div>
				<button type="button" class="approveSend">Отправить</button>
			</div>
		<?php endif;?>
		
	</form>
	<?php if($param->order_next_time > 0):?>
		<div class="measurement-time">Ближайшее время замера<span><?php echo $date_order;?></span></div>
	<?php endif;?>
	
</div>