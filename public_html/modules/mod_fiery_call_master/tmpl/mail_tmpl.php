<h2><?php printf($this->param->smtp_mail_header);?></h2>

<?php
$parameters = (array)$this->param;

?>
<b>Город:</b> <?php if(class_exists('McsData')) echo McsData::get('cityName');?>
<hr/>
<?php foreach($form_structure as $columns):?>
<?php foreach($columns as $field):?>
	<b><?php printf(($field['label'] != "") ? $field['label'] : $field['placeholder'] );?>:</b>
	<?php printf($input->getString($field['name']));?>
	<hr/>
<?php endforeach;?>

<?php endforeach;?>
<?php if(isset($this->param->master_info) && $this->param->master_info > 0):?>
<div class="master_info">
	<b>Мастер: </b><?php echo $master_tool->master_name;?></div><hr/>
	<b>Инструмент: </b><?php echo $master_tool->tool_name;?></div><hr/>
</div>
<?php endif;?>