(function($){
	$(document).ready(function () {
		$('.validate-result').hide();
		
		var parameters = $.parseJSON($('#form_params').val());
		$('#'+parameters.form_id+' .address').suggestions({
			token:"47b02b3ad94a7bf0e5f7fffc03563b75dd0193a1",
			type: "ADDRESS",
			onSelect: function(suggestion) {
            	console.log(suggestion);
        	}
		});
		$('#'+parameters.form_id+' .time').timepicker({
			timeFormat: 'HH:mm',
		    interval: 30,
		    minTime: '9',
		    maxTime: '20:00',
		    defaultTime: '9',
		    startTime: '10:00',
		    dynamic: false,
		    dropdown: true,
		    scrollbar: true
		});
		$('#'+parameters.form_id+" .calendar" ).datepicker({
			firstDay: 1,
			dateFormat: "dd.mm.yy",
			//minDate: new Date($('#hiddendelivdate').val()),
			minDate: $('#hiddendelivdate').val(),
			monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
			dayNamesMin: ['вс','пн','вт','ср','чт','пт','сб'],
		});
		$('#'+parameters.form_id+" .email" ).focusout(function(){
			var validate = validateEmail($(this).val());
			if(validate == false){
				$(this).removeClass('success');
				$(this).addClass('error');
			}else{
				$(this).removeClass('error');
				$(this).addClass('success');
			}
		});
		
		
	/*if(parameters.private_info > 0){
		$('#'+parameters.form_id+" .execute").hide();
		 $("#personal-info").click(function(){
		 	if($('#'+parameters.form_id+' .personal-info').is(':checked')) { 
	         $('#'+parameters.form_id+" .execute").show();
	         $('#'+parameters.form_id+' .personal-info').css('border', '1px solid red');
	        
	     	}else{	
		 		$('#'+parameters.form_id+" .execute").hide();
		 	}
		 });		
	}*/
		 
		
		
		execute_func(parameters);
		
		$('#'+parameters.form_id+' .phone-mask').mask("8(999) 999-9999");
		$('#'+parameters.form_id+' .my_captcha').hide();
		
		
		
	});
	function validate_form(parameters){
		$('.validate-result').html("");
		var total = 0;
		var required = $('#'+parameters.form_id+' [required="required"]').each(function(elem){
			$(this).removeClass('success');
			
			if($(this).val() == ""){
				$(this).addClass('error');
				if($('.label_'+$(this).attr('name')).val() == ""){
					$('.validate-result').append("Заполните поле: "+$(this).attr("placeholder")+"<br/>");
					//alert($(this).attr("placeholder"));	
				}else{
					$('.validate-result').append($('.label_'+$(this).attr('name')).val());
				}
					total++;
			}
			if($(this).hasClass('email')){
				var validate = validateEmail($(this).val());
				if(validate == false){
					$(this).addClass('error');
					if($('.label_'+$(this).attr('name')).val() == ""){
						$('.validate-result').append("Неверный формат: "+$(this).attr("placeholder")+"<br/>");
						//alert($(this).attr("placeholder"));	
					}else{
						$('.validate-result').append($('.label_'+$(this).attr('name')).val());
					}
						total++;
				}
				
			}
		});
		if(parameters.private_info > 0){
			if($('#'+parameters.form_id+' .personal-info').is(':checked') == false){
				$('.validate-result').append("Необходимо, дать согласие на обработку персональных данных<br/>");
				total++;
			}
		}
		if(total > 0){
			$('.validate-result').show();
			$('.validate-result').addClass('error');
			return false;
		}else{
			$('.validate-result').hide();
			return true;
		}
	}
	function execute_func(parameters){
		$('#'+parameters.form_id+" .execute").click(function(){	
		if(validate_form(parameters)==true){
			remove_classes(parameters);
			if(parameters.captcha > 0){
				$('#'+parameters.form_id+' .my_captcha').show();
				$('#'+parameters.form_id+' .approveSend').click(function(){
					ajaxExec(parameters);
				});	
			}else{
				ajaxExec(parameters);
			}
		}
			
			
		});
	}
	function remove_classes(parameters){
		if($('#'+parameters.form_id+" .result").hasClass('error-form')){
			$('#'+parameters.form_id+" .result").removeClass('error-form');
		}
		if($('#'+parameters.form_id+" .result").hasClass('success-form')){
			$('#'+parameters.form_id+" .result").removeClass('success-form');
		}
	}
	function validateEmail($email) {
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})+$/;
		return emailReg.test( $email );
	}
	function ajaxExec(parameters){
		var module_name = $('#'+parameters.form_id+" input[name='module_name']").val();
		$.ajax({
			type: 'POST',
			url: '?option=com_ajax&module='+module_name+'&format=debug&method=foo',
			data: $('#'+parameters.form_id).serialize(),
			beforeSend: function(){
				$("#overlay").css('display', 'block');
			},
			success: function(data){
				$("#overlay").css('display', 'none');
				if(data.toString() == 'success' ){
					$('#'+parameters.form_id+' .my_captcha').hide();
					$('#'+parameters.form_id+' .result').addClass('success-form');
					$('#'+parameters.form_id+' .result').html('Ваш запрос успешно отправлен');
					$('#'+parameters.form_id)[0].reset();
				}else{
					if(data.toString() == 'reload_page'){
						location.reload(); 
					}else{
						$('#'+parameters.form_id+' .my_captcha').hide();
						$('#'+parameters.form_id+" .result").addClass('error-form');	
						$('#'+parameters.form_id+" .result").html("Ошибка: "+data);
					}
					
				}
	            
	        }
		});
	}
})(jQuery);








