<?php
class ModFieryCallMasterHelper{
	private $paramsMail;
	private $phone;
	private $param;
	private $errors = array();
	public function __construct($input){
		$this->param = json_decode($input->getString('parameters'));
		//$this->pre($this->param);
		jimport('mail');
		
		if($input->getString($this->param->form_id) !== null){
				//$this->phone = $input->getString('phone');
				$mailPlg = JPluginHelper::getPlugin( 'mail', 'mail' );
				$this->paramsMail = json_decode($mailPlg->params);
				$form_structure = self::fields_format($this->param);
				$master_tool = self::get_master_tool_info();

				ob_start();
				require_once "tmpl/mail_tmpl.php";
				$mail_body = ob_get_contents();
				ob_end_clean();
				$result_captcha = json_decode($this->check_captcha($input));
				
				if($this->param->captcha == true){
					if($result_captcha->success > 0){
						MailHelper::send_mailer(
							$mailPlg->params,
							$this->param->smtp_mail_header,
							$mail_body
							);
					}else{
						printf('reload_page');
					}
				}else{
					MailHelper::send_mailer(
							$mailPlg->params,
							$this->param->smtp_mail_header,
							$mail_body
						);
				}
		}
		
	}
	public function get_params($type){
		if($type == 'module'){
			echo json_encode($this->param);
		}
	}
	public static function get_master_tool_info(){
		$master_id = !empty($_COOKIE['MASTER'])?$_COOKIE['MASTER']:false;
		$tool_id = $_COOKIE['TOOL']?$_COOKIE['TOOL']:false;
		$master_info;
		$tool_info;
				if($master_id !== false && $tool_id !== false){
		//echo $master_id.'--->'.$tool_id;
			// self::query_master_tool_info($master_id,$tool_id)->master_photo;
			return self::query_master_tool_info($master_id,$tool_id);
		}
	}
	public static function query_master_tool_info($master_id,$tool_id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('m.name as master_name','t.name as tool_name','mp.path as master_photo'));
		$query->from($db->quoteName('#__masters_masters', 'm'));
		$query->join('LEFT',$db->quoteName('#__masters_masters_photos','mp') . 'ON (' . $db->quoteName('mp.parent_id') . ' = ' . $db->quoteName('m.id') . ')');
		$query->join('LEFT',$db->quoteName('#__masters_tools','t') . 'ON ('  . $db->quoteName('t.id') . ' = ' . $db->quoteName('mp.tool_id') . ')');
		$query->where($db->quoteName('m.id').' = '.$master_id . ' AND ' . $db->quoteName('t.id') .' = ' . $tool_id);
		//echo $query;
		/*$query = "SELECT 
				 	m.name as master_name,
					t.name as tool_name,
					mp.path as master_photo
				FROM #__masters_masters m
				LEFT JOIN #__masters_masters_photos mp ON mp.parent_id = m.id 
				LEFT JOIN #__masters_tools t  ON t.id = mp.tool_id 
				WHERE m.id=$master_id AND t.id=$tool_id";*/
		$db->setQuery($query);
		return $db->loadObject();
	}
	public static function fooAjax(){
		$input  = JFactory::getApplication()->input;
		$module_name = $input->getString('module_name');
		$module = JModuleHelper::getModule('mod_'.$module_name);
		$helper = new ModFieryCallMasterHelper($input);
	}
	private function check_captcha($input){
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$params = array(
		    'secret' => $this->paramsMail->secret_key,
		    'response' => $input->getString('g-recaptcha-response')
		);
		$result = file_get_contents($url, false, stream_context_create(array(
		    'http' => array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded',
		        'content' => http_build_query($params)
		    )
		)));

		return $result;
	}

	public static function get_next_date_order($date_order_param){
		$months = array(
			"01" => "января",
			"02" => "февраля",
			"03" => "марта",
			"04" => "апреля",
			"05" => "мая",
			"06" => "июня",
			"07" => "июля",
			"08" => "августа",
			"09" => "сентября",
			"10" => "октября",
			"11" => "ноября",
			"12" => "декабря"
		);
		$date_order_current = explode('.',$date_order_param);
		$date_order_current_format = $date_order_current[2].'-'.$date_order_current[1].'-'.$date_order_current[0];
		$current_date = new DateTime();
		
		
		if(date($current_date->format('Y-m-d')) >= date($date_order_current_format)){
			$current_date->modify('+1 day');
			$date_order = $current_date->format('d.m.Y');
		}
		if(date($current_date->format('Y-m-d')) <= date($date_order_current_format)){
	
			$date_order = $date_order_param;
		}
		
		
		$date_order_date = explode(".", $date_order)[0];
		$date_order_month = explode(".", $date_order)[1];
		$date_order_year = explode(".", $date_order)[2];
		
		$date_order = $date_order_date." ".$months[$date_order_month]." ".$date_order_year;
		return $date_order;
	}
	public static function fields_format($param){
		$result_array = array();
		$parameters = (array)$param;
		$form_columns = (array)$parameters['form-columns'];
		$counter = 0;
		foreach($form_columns as $columns){
			array_push($result_array, array());
			foreach($columns as $fields){
				
				foreach($fields as $field){
					$field = (array)$field;
					$field['classes'] = array();
					$field['field_classes'] = array();
					$counter_field = 0;
					$field['field_classes'] = (array)$field['field_classes'];
					foreach($field['field_classes'] as $class){
						$class = (array)$class;
						$field['classes'][$counter_field] = $class['class'];
						$counter_field++;
					}

				unset($field['field_classes']);
					array_push($result_array[$counter], $field);
					
				}	
			}
		$counter++;
		}
		return $result_array;
	}
	private function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
}

?>