<?php
defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';
jimport('joomla.application.module.helper');
$module_name = 'fiery_call_master';
$module = JModuleHelper::getModule('mod_'.$module_name);
$param = json_decode($module->params);


$template = 'fieryflash_template';
JLoader::register('Fieryflash', JPATH_THEMES.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'functions.php');
$city = Fieryflash::get_city_name(true);

if($param->order_next_time > 0){
	$date_order = ModFieryCallMasterHelper::get_next_date_order($param->date_order);
}





$mailPlg = JPluginHelper::getPlugin( 'mail', 'mail' );
$paramsMail = json_decode($mailPlg->params);
$doc = JFactory::getDocument();
if(!empty($paramsMail->site_key)){
	$doc->addScript('https://www.google.com/recaptcha/api.js?hl=ru');
}
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/jquery.maskedinput.min.js');
$doc->addStyleSheet(JURI::base().'modules/'.$module->module . '/assets/suggestions.min.css');
$doc->addStyleSheet(JURI::base().'modules/'.$module->module . '/assets/'.$module_name.'.css');
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/'.$module_name.'.js');

$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/jquery-ui-1.12.1.custom/jquery-ui.min.js');
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/jquery.timepicker.min.js');
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/jquery.xdomainrequest.min.js');
$doc->addScript(JURI::base().'modules/'.$module->module . '/assets/jquery.suggestions.min.js');

require JModuleHelper::getLayoutPath('mod_'.$module_name, $params->get('layout', 'default'));

?>