<?php defined('_JEXEC') or die; ?>
<form action="" class="range-slider-form">
	<input type="hidden" name="min_price" value="<?php echo $params->get('min_price');?>">
	<input type="hidden" name="max_price" value="<?php echo $params->get('max_price');?>">
	<input type="hidden" name="std_square" value="<?php echo $params->get('std_square');?>">
	<div class="form-title text-center">
		<h3>Расчет стоимости натяжного потолка</h3>
	</div>
	<div class="range-slider">
		<div class="row">
			<div class="col-sm-<?php echo number_format(12/$params->get('colls')); ?>">
				<div class="range-slider-row-1">
					<div class="range-slider-txt">Потолок, м2</div>
					<div class="range-slider-input"><input type="text" class="range-slider-input-val calc_1_val_type_1" value="1"/></div>
				</div>
				<div class="range-slider-row-2">
					<div class="range-slider-line-parent rslider">
						<div class="range-slider-line2 rslider"></div>
						<div class="range-slider-line3 rslider"></div>
					</div>
					<div class="range-slider-line" 
						data-min="<?php printf($params->get('min_square'));?>" 
						data-val="<?php printf($params->get('std_square'));?>" 
						data-step="<?php printf($params->get('step'));?>" 
						data-max="<?php printf($params->get('max_square'));?>">
						<div class="range-slider-line-child"></div>
					</div>
					<div class="calс_tab_slider_grad">
						<ul>
							<li></li>
							<li></li>
							<li></li>
							<li></li>
						</ul>
					</div>
					<div class="calс_tab_slider_grad2">
						<div class="calс_tab_slider_num calс_tab_slider_num_1"><?php printf($params->get('min_square'));?></div>
						<div class="calс_tab_slider_num calс_tab_slider_num_30"><?php printf((int)$params->get('max_square')/2);?></div>
						<div class="calс_tab_slider_num calс_tab_slider_num_60"><?php printf($params->get('max_square'));?></div>
					</div>
				</div>
			</div>
			<div class="col-sm-<?php echo number_format(12/$params->get('colls')); ?>">
				<div class="range-slider-price"></div>
					<?php if(
						(!empty($params->get('show_link'))) &&
						(!empty($params->get('link')))
					) :?>
						<div class="range-details">
							<a href="<?php echo $cityHref.$params->get('link');?>" class="button">Подробнее</a>
						</div>
					<?php endif; ?>
			</div>
		</div>
	</div>
</form>