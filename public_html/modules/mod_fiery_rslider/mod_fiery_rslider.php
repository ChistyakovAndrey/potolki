<?php
defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_fiery_rslider');
if(class_exists('McsData')) $cityHref = McsData::get('city');
$doc = JFactory::getDocument();
//$doc->addScript(JURI::base().'modules/'.$module->module . '/js/ui-slider/jquery-1.12.4.js');
$doc->addScript(JURI::base().'modules/'.$module->module . '/js/ui-slider/jquery-ui.js');
$doc->addScript(JURI::base().'modules/'.$module->module . '/js/mod_fiery_rslider.js');
$doc->addScript(JURI::base().'modules/'.$module->module . '/js/ui-slider/jquery.ui.touch-punch.min.js');

$doc->addStyleSheet(JURI::base().'modules/'.$module->module . '/css/custom.css');
require JModuleHelper::getLayoutPath('mod_fiery_rslider', $params->get('layout', 'default'));

?>
