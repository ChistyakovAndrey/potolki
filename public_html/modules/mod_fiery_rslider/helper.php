<?php
class ModFieryRsliderHelper{
	private $param;
	public function __construct($input,$param){
		
		$this->param = json_decode($param);
		if($input->getString('get_params_range') !== null){
			
			$this->get_params();
		}
	}
	public function get_params(){
		echo json_encode($this->param);
	}
	public static function fooAjax(){
		$module1 = JModuleHelper::getModule('mod_fiery_rslider');
		$params = $module->params;
		
		$input  = JFactory::getApplication()->input;
		$helper = new ModFieryRsliderHelper($input,$params);
	}

	public function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
}
?>