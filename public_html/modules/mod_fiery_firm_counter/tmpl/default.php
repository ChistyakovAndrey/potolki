<?php
defined('_JEXEC') or die;
$class = $params->get('firm_counter-style');
?>

<div class="row">
<div class="col-md-12 text-center">
<h3 style="font-family: 'Conv_Gilroy-Bold', Sans-Serif;font-size: 3.6rem;letter-spacing: 0.05rem;"><?php echo $module->title; ?></h3>
</div>
</div>

<div class="row <?php echo $class; ?>">
	<?php foreach($param['firm-counter-info'] as $value):?>
		<div class="col-md-4">
			<div class="firm-counter-title">
				<span><?php echo $value['firm_counter-info-slide-title'];?></span><?php echo $value['firm_counter-info-slide-title-2'];?>
			</div>
			<div class="firm-counter-description"><?php echo $value['firm_counter-info-slide-description'];?></div>
		</div>
	<?php endforeach;?>
</div>