<?php
	defined('_JEXEC') or die;
?>

<div class="contact-info">
	<?php if($params->get('load_telephone') == 1):?>
		<div class="phone">Телефон:<br/><?php printf($params_template->telephone);?></div>
	<?php endif; ?>

	<?php if($params->get('load_worktime') == 1):?>
		<div class="worktime">Рабоче время:<br/><?php printf($params_template->worktimes);?></div>
	<?php endif;?>

	<?php if($params->get('load_email') == 1):?>
		<div class="worktime">E-mail:<br/><?php printf($params_template->email);?></div>
	<?php endif;?>

	<?php if($params->get('load_address') == 1):?>
		<div class="worktime">Адрес:<br/><?php printf($params_template->address);?></div>
	<?php endif;?>
</div>