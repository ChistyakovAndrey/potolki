<?php
defined("_JEXEC") or die();

class PhotogalleryModelAlbum_photo extends JModelAdmin {
	
	
	public function getForm($data = array(),$loadData = true) {
		
		$form = $this->loadForm(
					$this->option.'album_photo',
					'album_photo',
					array('control'=>'jform','load_data'=>$loadData)
				);
		
		if(empty($form)) {
			return FALSE;
		}
		
		return $form;
	}
	
	public function getTable($type = 'Album_photo', $prefix = 'PhotogalleryTable',$config = array()) {
		return JTable::getInstance($type,$prefix,$config);
	}
	
	protected function loadFormData(){
		$data = JFactory::getApplication()->getUserState('com_photogallery.edit.album_photo.data',array());
		
		if(empty($data)) {
			
			$data = $this->getItem();
		}
		
		return $data;
	}
	
	public function save($data) {
		
		if(!trim($data['name'])) {
			$this->setError(JText::_('COM_PHOTOGALLERY_WARNING_PROVIDE_VALID_NAME'));
			return FALSE;
		}
		
		if(trim($data['alias']) == '') {
			$data['alias'] = $data['name'];
			$data['alias'] = JApplicationHelper::stringURLSafe($data['alias']);
		}
		
		
		if(parent::save($data)) {
			return TRUE;
		}
		return FALSE;
		
	}
	public function pre($arr){
		echo '<pre>';
		print_r($arr);
		echo '</pre>';
	}
}
?>