<?php
defined('_JEXEC') or die('Restricted access');


class PhotogalleryModelAlbum_photos extends JModelList {
	
	protected function getListQuery() {
		
		$query = parent::getListQuery();
		
		$query->select('*');
		$query->from('#__photogallery_photos');
		$query->where('parent_id='.JFactory::getApplication()->input->get('parent_id'));
		$query->order('position ASC');
		return $query;
	}
}
?>