<?php
defined("_JEXEC") or die();

class PhotogalleryModelAlbum extends JModelAdmin {
	
	public function getForm($data = array(),$loadData = true) {
		
		
		$form = $this->loadForm(
					$this->option.'album',
					'album',
					array('control'=>'jform','load_data'=>$loadData)
				);
		
		
		if(empty($form)) {
			return FALSE;
		}
		
		return $form;
	}
	
	public function getTable($type = 'Album', $prefix = 'PhotogalleryTable',$config = array()) {
		return JTable::getInstance($type,$prefix,$config);
	}
	
	protected function loadFormData(){
		$data = JFactory::getApplication()->getUserState('com_photogallery.edit.album.data',array());
		
		if(empty($data)) {
			$data = $this->getItem();
		}
		
		return $data;
	}
	
	public function save($data) {
		
		if(!trim($data['name'])) {
			$this->setError(JText::_('COM_PHOTOGALLERY_WARNING_PROVIDE_VALID_NAME'));
			return FALSE;
		}
		
		if(trim($data['alias']) == '') {
			$data['alias'] = $data['name'];
			$data['alias'] = JApplicationHelper::stringURLSafe($data['alias']);
		}
		
		
		if(parent::save($data)) {
			return TRUE;
		}
		return FALSE;
		
	}
}
?>