<?php

defined('_JEXEC') or die('Restricted access');
JFormHelper::loadFieldClass('list');

class JFormFieldModselect extends JFormFieldList {
 
	
	protected $type = 'Modselect';
 
	protected function getOptions() {
		
		$parent = parent::getOptions();
		
		$opt = $this->getAttribute('option');
		
		$options = array();
		if(!empty($parent)) {
			foreach($parent as $option) {
				array_push($options, $option);
			}
		}
		
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('DISTINCT id AS value, title AS text')
			->from('#__modules')
			->where("published = 1");
		$db->setQuery($query);

		
		
		try
		{
			$row = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->unqueueMessage($e->getMessage,'error');
		}
		
		if ($row)
		{
			if(isset($this->form->getData()['parent_ids']) && $this->form->getData()['parent_ids'] != null){
			
			//$this->get_modules($this->form->getData()['parent_ids']);
		}
			for($i = 0;$i<count($row);$i++)
			{
				array_push($options,$row[$i]);

			}
		}
		
		return $options;
	}
	
}