<?php

defined('_JEXEC') or die('Restricted access');
JFormHelper::loadFieldClass('list');

class JFormFieldAlbumid extends JFormFieldList {
 
	
	protected $type = 'Albumid';
 
	protected function getOptions() {
		
		$parent = parent::getOptions();
		
		$opt = $this->getAttribute('option');
		
		$options = array();
		if(!empty($parent)) {
			foreach($parent as $option) {
				array_push($options, $option);
			}
		}
		
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('DISTINCT id AS value, name AS text')
			->from('#__photogallery_photo_albums')
			->where("state > 0");
		$db->setQuery($query);
		
		try
		{
			$row = $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->unqueueMessage($e->getMessage,'error');
		}
		
		if ($row)
		{
			for($i = 0;$i<count($row);$i++)
			{
				if($row[$i]->value==JFactory::getApplication()->input->get('parent_id')){
					array_unshift($options, $row[$i]);
					unset($row[$i]);
				}else{
					array_push($options,$row[$i]);
				}
			}
		}
		
		return $options;
	}
}