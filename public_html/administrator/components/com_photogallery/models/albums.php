<?php
defined('_JEXEC') or die('Restricted access');


class PhotogalleryModelAlbums extends JModelList {
	
	protected function getListQuery() {
		
		$query = parent::getListQuery();
		
		$query->select('*');
		$query->from('#__photogallery_photo_albums');
		$query->order('position ASC');
		return $query;
	}
}
?>