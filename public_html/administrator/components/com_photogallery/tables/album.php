<?php
defined("_JEXEC") or die();

class PhotogalleryTableAlbum extends JTable {
	
	public function __construct($db) {
		parent::__construct('#__photogallery_photo_albums','id',$db);
	}
	
	public function publish($pks = null, $state = 1, $userId = 0) {
		
		
		JArrayHelper::toInteger($pks);
		$state = (int)$state;
		if(empty($pks)) {
			throw new RuntimeException(JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
		}
		
		foreach($pks as $pk) {
			if(!$this->load($pk)) {
				throw new RuntimeException(JText::_('COM_PHOTOGALLERY_TABLE_ERROR_TYPE'));
			}
			$this->state = $state;
			
			if(!$this->store()) {
				throw new RuntimeException(JText::_('COM_PHOTOGALLERY_TABLE_ERROR_TYPE_STORE'));
			}
		}
		
		return true;
		
	}
	function bind( $array, $ignore = '' )
    {
        if (key_exists( 'parent_ids', $array ) && is_array( $array['parent_ids'] )) {
	        $array['parent_ids'] = implode( ',', $array['parent_ids'] );
        }

        return parent::bind( $array, $ignore );
    }
}