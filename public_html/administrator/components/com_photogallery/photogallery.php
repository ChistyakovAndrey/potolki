<?php
defined("_JEXEC") or die();

if(!JFactory::getUser()->authorise('core.manage','com_masters')) {
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

$controller = JControllerLegacy::getInstance('Photogallery');//MastersController
$controller->registerTask('unconfirm','confirm');

$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::base().'components/com_photogallery/assets/css/style.css');
$doc->addStyleSheet(JURI::base().'components/com_photogallery/assets/Blueimp/css/blueimp-gallery.min.css');
$doc->addScript(JURI::base().'components/com_photogallery/assets/jquery-1.11.0.min.js');
$doc->addScript(JURI::base().'components/com_photogallery/assets/Blueimp/js/blueimp-gallery.min.js');

$input = jFactory::getApplication()->input;
$controller->execute($input->getCmd('task','display'));
$controller->redirect();
?>

