<?php

defined('_JEXEC') or die('Restricted access');


class PhotogalleryViewAlbum_photo extends JViewLegacy {
	
	public $form = null;
	public $input;
	public $isNew;
	public $parent_id;
	protected $item = null; 
	public function display($tpl = null) 
	{
		$this->input = JFactory::getApplication()->input;
		$this->form	= $this->get('Form');//getForm
		$this->item = $this->get('Item');
		$this->form->setFieldAttribute('path', 'directory', 'com_photogallery/photos/'.$this->input->get('parent_id'));
		//$this->form->setFieldAttribute('parent_id', 'value', $this->input->get('parent_id'));
		$this->addToolBar();
 		$this->setDocument();
 		$this->check_folder_images();
		parent::display($tpl);
	}
	protected function check_folder_images(){
		$cat;
		$cat_id;
		
			$cat =  $this->input->get('category');
			$cat_id =  $this->input->get('parent_id');
		
		
		if(!is_dir(JPATH_SITE.'/images/com_photogallery')){
			mkdir(JPATH_SITE.'/images/com_photogallery');
		}
		if(!is_dir(JPATH_SITE.'/images/com_photogallery/'.$cat)){
			mkdir(JPATH_SITE.'/images/com_photogallery/'.$cat);
		}
		if(!is_dir(JPATH_SITE.'/images/com_photogallery/'.$cat.'/'.$cat_id)){
			mkdir(JPATH_SITE.'/images/com_photogallery/'.$cat.'/'.$cat_id);
		}
		
	}
	protected function addToolBar() 
	{
	
		JFactory::getApplication()->input->set('hidemainmenu', true);
		
		$this->isNew = ($this->item->parent_id == 0);
		
		if ($this->isNew)
		{
			$title = JText::_('COM_PHOTOGALLERY_MANAGER_ALBUM_PHOTO_NEW');
			$this->parent_id = $this->input->get('parent_id');
		}
		else
		{
			$title = JText::_('COM_PHOTOGALLERY_MANAGER_ALBUM_PHOTO_EDIT');
			$this->parent_id = $this->item->parent_id;
		}
		
		JToolBarHelper::title($title,'album_phototitle');
		JToolBarHelper::apply('album_photo.apply_n_stay');
		JToolBarHelper::save2new('album_photo.save_n_new');
		JToolBarHelper::save('album_photo.save_n_close');
		JToolBarHelper::cancel('album_photo.cancel',$this->isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE');
		
	}
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		/*$document->setTitle(JText::_('COM_DOSKA_MANEGER_TYPE_NEW_PAGE_TITLE'));*/
	}
	
	
	
}