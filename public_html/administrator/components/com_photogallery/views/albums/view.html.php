<?php

defined("_JEXEC") or die();

class PhotogalleryViewAlbums extends JViewLegacy {
	
	protected $items;
	
	public function display($tpl = null) {
		
		
		
		$this->addToolBar();
		$this->setDocument();
		$this->items = $this->get('Items');///getItems()
		if(isset($this->items->parent_ids) && $this->items->parent_ids != null){
			print_r($this->get_modules($this->items->parent_ids));
		}
		parent::display($tpl);
	}
	
	
	protected function addToolBar() {
	
		JToolbarHelper::title(JText::_("COM_PHOTOGALLERY_MANAGER_ALBUMS"),'mastertitle');
		
		JToolbarHelper::addNew('album.add',JText::_('COM_PHOTOGALLERY_MANAGER_ALBUMS_ADD'));
		JToolbarHelper::deleteList(JText::_('COM_PHOTOGALLERY_MANAGER_ALBUMS_DELETE_MSG'),'albums.delete');
		JToolbarHelper::divider();
		JToolbarHelper::publish('albums.publish','JTOOLBAR_PUBLISH',TRUE);
		JToolbarHelper::unpublish('albums.unpublish','JTOOLBAR_UNPUBLISH',TRUE);
		JToolbarHelper::preferences('com_photogallery');

	}
	protected function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
	protected function setDocument() {
		$document = JFactory::getDocument();
		
		//print_r($document);
	}
	protected function get_modules($ids){
		$where = implode(' OR id = ', explode(',', $ids));
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id,title');
		$query->from('#__modules');
		$query->where('id='.$where);
		$db->setQuery($query);
		return $db->loadAssoc();
	}
	
}
?>