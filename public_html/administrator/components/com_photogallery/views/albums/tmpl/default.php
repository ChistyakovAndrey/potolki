<?php

defined("_JEXEC") or die();

?>

<form action="<?php echo JRoute::_("index.php?option=com_photogallery&category=albums&view=albums");?>" method="post" name="adminForm" id="adminForm">

<?php if(!empty($this->sidebar)) :?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar;?>
	</div>
<?php endif;?>

<div id="j-main-container" class="span10">
 
 <table class="table table-striped table-hover">
 	
 	<thead>
 			<th width="1%">
 				<?php echo JText::_('COM_ALBUMS_NUM');?>
 			</th>
 			
 			<th width="2%">
 				<?php echo JHtml::_('grid.checkall');?>
 			</th>
 			<th width="30%">
 				<?php echo JText::_('COM_PHOTOGALLERY_ALBUM_PREVIEW');?>
 			</th>
 			<th width="48%">
 				<?php echo JText::_('COM_PHOTOGALLERY_ALBUM_NAME');?>
 			</th>
 			<th width="10%">
 				<?php echo JText::_('COM_PHOTOGALLERY_MEDIA_HREF');?>
 			</th>
 			<th width="5%">
 				<?php echo JText::_('JSTATUS');?>
 			</th>
 			<th width="2%">
 				<?php echo JText::_('COM_PHOTOGALLERY_ALBUM_POSITION');?>
 			</th>
 			<th width="2%">
 				<?php echo JText::_('COM_PHOTOGALLERY_ALBUM_ID');?>
 			</th>
 	</thead>
 	
 	<tbody>
 		<?php if(!empty($this->items)) :?>
 			<?php $i = 1;?>

 			<?php foreach($this->items as $key=>$item) :?>
 				<tr>
 					<td>
		 				<?php echo $i;?>
		 			</td>
		 			
		 			<td>
		 				<?php echo JHtml::_('grid.id', $key ,$item->id)?>
		 			</td>
		 			<td class="com_photogallery_preview_album_icons">
			 				<a class="com_photogallery_preview_album_icon" href="/<?php printf($item->icon_href);?>" title="<?php printf($item->name);?>">
			 					<img src="/<?php printf($item->icon_href);?>" width="120px" />
			 				</a>
			 			</td>
		 			<td>
		 			<?php $link = JRoute::_('index.php?option=com_photogallery&category=albums&id='.$item->id.'&task=album.edit');?>
		 				<?php echo JHtml::_('link',$link,$item->name);?>
		 			</td>
		 			<td>
		 			<?php $link = JRoute::_('index.php?option=com_photogallery&category=photos&parent_id='.$item->id.'&view=album_photos');?>
		 				<?php echo JHtml::_('link',$link,'Фото');?>
		 			</td>
		 			<td>
		 				<?php echo JHtml::_('jgrid.published',$item->state,$key,'albums.');?>
		 			</td>
		 			<td>
		 				<?php echo $item->position;?>
		 			</td>
		 			<td>
		 				<?php echo $item->id;?>
		 			</td>
		 			
 				</tr>
 				<?php $i++;?>
 			<?php endforeach;?>
 		
 		<?php endif;?>
 	</tbody>
 	
 </table>
 
	
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	
	
	<?php echo JHtml::_('form.token');?>
	</div>
</form>
<div id="blueimp-gallery" class="blueimp-gallery">
  <div class="slides"></div>
  <h3 class="title"></h3>
  <a class="prev">‹</a>
  <a class="next">›</a>
  <a class="close">×</a>
  <a class="play-pause"></a>
  <ol class="indicator"></ol>
</div>
<script>
	$(document).ready(function(){
	 $('.com_photogallery_preview_album_icons').click(function(event) {
	    event = event || window.event
	    var link = $('.com_photogallery_preview_album_icons').index(this),
	    options = {index: link, event: event, youTubeClickToPlay: false},
	    links = $('.com_photogallery_preview_album_icons .com_photogallery_preview_album_icon').toArray()
	    blueimp.Gallery(links, options)
  	})
  });
</script>