<?php

defined("_JEXEC") or die();

class PhotogalleryViewAlbum_photos extends JViewLegacy {
	
	protected $items;
	protected $input;
	
	public function display($tpl = null) {
		$this->input = JFactory::getApplication()->input;
		
		$this->addToolBar();
		$this->setDocument();
		
		$this->items = $this->get('Items');///getItems()
		
		parent::display($tpl);
	}
	
	
	protected function addToolBar() {
	
		JToolbarHelper::title(JText::_("COM_PHOTOGALLERY_MANAGER_ALBUM_PHOTOS"),'album_photostitle');
		
		JToolbarHelper::addNew('album_photo.add',JText::_('COM_PHOTOGALLERY_MANAGER_ADD_ALBUM_PHOTO'));
		JToolbarHelper::deleteList(JText::_('COM_PHOTOGALLERY_MANAGER_DELETE_ALBUM_PHOTOS'),'album_photos.delete');
		JToolbarHelper::divider();
		
		JToolbarHelper::publish('album_photos.publish','JTOOLBAR_PUBLISH',TRUE);
		JToolbarHelper::unpublish('album_photos.unpublish','JTOOLBAR_UNPUBLISH',TRUE);
		JToolbarHelper::preferences('com_photogallery');
	}
	protected function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
	protected function setDocument() {
		$document = JFactory::getDocument();
		//print_r($document);
	}
	
}
?>