<?php

defined("_JEXEC") or die();

?>

<form action="<?php echo JRoute::_('index.php?option=com_photogallery&category=photos&parent_id='.$this->input->get('parent_id').'&view=album_photos');?>" method="post" name="adminForm" id="adminForm">

<?php if(!empty($this->sidebar)) :?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar;?>
	</div>
<?php endif;?>

<div id="j-main-container" class="span10">
 
 <table class="table table-striped table-hover">
 	
 	<thead>
 			<th width="1%">
 				<?php echo JText::_('COM_PHOTOGALLERY_PHOTO_NUM');?>
 			</th>
 			
 			<th width="2%">
 				<?php echo JHtml::_('grid.checkall');?>
 			</th>
 			<th width="78%">
 				<?php echo JText::_('COM_PHOTOGALLERY_PHOTO_PREVIEW');?>
 			</th>
 			<th width="10%">
 				<?php echo JText::_('COM_PHOTOGALLERY_PHOTO_NAME');?>
 			</th>
 			<th width="5%">
 				<?php echo JText::_('JSTATUS');?>
 			</th>
 			<th width="2%">
 				<?php echo JText::_('COM_PHOTOGALLERY_PHOTO_POSITION');?>
 			</th>
 			<th width="2%">
 				<?php echo JText::_('COM_PHOTOGALLERY_PHOTO_ID');?>
 			</th>
 	</thead>
 	
 	<tbody>
 		<?php if(!empty($this->items)) :?>
 			<?php $i = 1;?>
	 			<?php foreach($this->items as $key=>$item) :?>
	 				<tr>
	 					<td>
			 				<?php echo $i;?>
			 			</td>
			 			
			 			<td>
			 				<?php echo JHtml::_('grid.id', $key ,$item->id)?>
			 			</td>
			 			<td class="com_photogallery_preview_photos">
			 				<a class="com_photogallery_preview_photo" href="/<?php printf($item->path);?>" title="<?php printf($item->name);?>">
			 					<img src="/<?php printf($item->path);?>" width="120px" />
			 				</a>
			 			</td>
			 			<td>
			 			<?php $link = JRoute::_('index.php?option=com_photogallery&task=album_photo.edit&id='.$item->id.'&parent_id='.$item->parent_id);?>
			 				<?php echo JHtml::_('link',$link,$item->name);?>
			 			</td>
			 			<td>
			 				<?php echo JHtml::_('jgrid.published',$item->state,$key,'album_photos.');?>
			 			</td>
			 			<td>
		 					<?php echo $item->position;?>
			 			</td>
			 			<td>
			 				<?php echo $item->id;?>
			 			</td>
	 				</tr>
	 				<?php $i++;?>
	 			<?php endforeach;?>
 		<?php endif;?>
 	</tbody>
 	
 </table>
 
	
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	
	
	<?php echo JHtml::_('form.token');?>
	</div>
</form>

<div id="blueimp-gallery" class="blueimp-gallery">
  <div class="slides"></div>
  <h3 class="title"></h3>
  <a class="prev">‹</a>
  <a class="next">›</a>
  <a class="close">×</a>
  <a class="play-pause"></a>
  <ol class="indicator"></ol>
</div>
<script>
	$(document).ready(function(){
	 $('.com_photogallery_preview_photos').click(function(event) {
	    event = event || window.event
	    var link = $('.com_photogallery_preview_photos').index(this),
	    options = {index: link, event: event, youTubeClickToPlay: false},
	    links = $('.com_photogallery_preview_photos .com_photogallery_preview_photo').toArray()
	    blueimp.Gallery(links, options)
  	})
  });
</script>