<?php
defined("_JEXEC") or die();

class PhotogalleryViewAlbum extends JViewLegacy {
	
	protected $form;
	protected $item;
	protected $input;
	
	
	public function display($tmpl = null) {
		
		$this->input = JFactory::getApplication()->input;
		$this->form = $this->get('Form');//getForm
		$this->item = $this->get('Item');//getItem
		$this->form->setFieldAttribute('icon_href', 'directory', 'com_photogallery/albums/'.$this->input->get('id'));

		if(isset($this->item->parent_ids) && $this->item->parent_ids != null){
			$ids = explode(',', $this->item->parent_ids);
			foreach($ids as $value){
				$this->form->setValue('parent_ids',$value);
			}
			$this->form->setValue('parent_ids',null,$ids);
			
			
		//$this->form->setFieldAttribute('parent_ids','default','1');
		
		// Назначаем атрибут полю price
		//$elem[0]['default'] = '1';
		}
		$this->addToolBar();
		$this->setDocument();
		$this->check_folder_images();
		parent::display($tmpl);
	}
	
	protected function check_folder_images(){
		$cat;
		$cat_id;
		
			$cat =  $this->input->get('category');
			$cat_id =  $this->input->get('id');
		
		
		if(!is_dir(JPATH_SITE.'/images/com_photogallery')){
			mkdir(JPATH_SITE.'/images/com_photogallery');
		}
		if(!is_dir(JPATH_SITE.'/images/com_photogallery/'.$cat)){
			mkdir(JPATH_SITE.'/images/com_photogallery/'.$cat);
		}
		if(!is_dir(JPATH_SITE.'/images/com_photogallery/'.$cat.'/'.$cat_id)){
			mkdir(JPATH_SITE.'/images/com_photogallery/'.$cat.'/'.$cat_id);
		}
		
	}
	protected function addToolBar () {
		
		$isnew = ($this->item->id == 0);
		
		if($isnew) {
			$title = JText::_('COM_PHOTOGALLERY_MANAGER_ALBUM_NEW_MANAGER_TITLE');
		}
		else {
			
			$title = JText::_('COM_PHOTOGALLERY_MANAGER_ALBUM_EDIT_MANAGER_TITLE');
		}
		
		JToolBarHelper::title($title);
		JToolBarHelper::apply('album.apply_n_stay');
		
		JToolBarHelper::save2new('album.save_n_new');
		JToolBarHelper::save('album.save_n_close');
		JToolBarHelper::cancel('album.cancel');
		
		
		
	}
	
	protected function setDocument () {
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_PHOTOGALLERY_MANAGER_ALBUM_NEW_PAGE_TITLE'));
	}
}
?>