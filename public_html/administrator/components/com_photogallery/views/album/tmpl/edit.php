<?php
defined("_JEXEC") or die();


JHtml::_('formbehavior.chosen','select');
JHtml::_('behavior.keepalive');

?>

<form action="<?php echo JRoute::_('index.php?option=com_photogallery&category=malbums&layout=edit&id='.(int)$this->item->id)?>" method="post" id="adminForm" name="adminForm" class="form-validate">

<div class="row-fluid">
	<div class="span9">
		<?php echo JLayoutHelper::render('edit.title_alias',$this,'administrator/components/com_photogallery');?>
	</div>
	
	<div class="span3">
		<?php echo JLayoutHelper::render('edit.global',$this,'administrator/components/com_photogallery');?>
	</div>
	<div class="span3">
		<?php //print_r($modules);?>
	</div>
</div>

<?php echo $this->form->getField('id')->renderField();?>


<?php echo $this->form->renderFieldset('basic');?>


<input  type="hidden" name="task" value="type.edit"/>

<?php echo JHtml::_('form.token');//JHtmlForm?>
</form>