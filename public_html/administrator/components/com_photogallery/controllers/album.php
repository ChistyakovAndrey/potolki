<?php
defined('_JEXEC') or die;


class PhotogalleryControllerAlbum extends JControllerForm {
	public function getLastId(){
		$config = JFactory::getConfig();
		$database = $config->get('db');
		$prefix = $config->get('dbprefix');
		$db = JFactory::getDbo();
		$query = "SELECT MAX(LAST_INSERT_ID(id)) AS ID FROM #__photogallery_photo_albums";
		
		$db->setQuery($query);
		return $db->loadObject()->ID;
	}
	public function add(){
		parent::add();
		$id =  $this->getLastId()+1;
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=albums&id='.$id.'&view=album&layout=edit', false));
	}
	public function apply_n_stay(){
		parent::save();
		$id =  $this->input->get->get('id');
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=albums&id='.$id.'&view=album&layout=edit', false));
	}
	public function save_n_new(){
		parent::save();
		#$id =  $this->input->get->get('id');
		$id =  $this->getLastId()+1;
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=albums&id='.$id.'&view=album&layout=edit', false));
	}
	public function edit(){
		parent::edit();
		$id =  $this->input->get('id');
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=albums&id='.$id.'&view=album&layout=edit', false));
	}
	public function save_n_close(){
		parent::save();
		$id =  $this->getLastId();
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=albums&view=albums', false));
	}
	public function cancel(){
		parent::cancel();
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=albums&view=albums', false));
	}
}
