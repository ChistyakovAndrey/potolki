<?php

defined('_JEXEC') or die('Restricted Access');

class PhotogalleryControllerAlbum_photos extends JControllerAdmin {
	
	public function getModel($name = 'Album_photo',$prefix = 'PhotogalleryModel',$config = array( 'ignore_request' => true )) {
		return parent::getModel($name,$prefix,$config);
	}
	public function delete(){
		
		$parent_id =  $this->input->get('parent_id');
		$cid = $this->input->get('cid', array(), 'array');
		$photos = $this->control_get_path_photos($parent_id,$cid);
		foreach($photos as $photo){
			if(unlink(JPATH_ROOT.'/'.$photo['path'])){
				parent::delete();
				$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=photos&parent_id='.$parent_id.'&view=album_photos', false));
			}
		}
	}
	public function publish(){
		$parent_id =  $this->input->get('parent_id');
		parent::publish();
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=photos&parent_id='.$parent_id.'&view=album_photos', false));
	}
	public function unpublish(){
		$parent_id =  $this->input->get('parent_id');
		parent::unpublish();
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=photos&parent_id='.$parent_id.'&view=album_photos', false));
	}
	public function control_get_path_photos($parent_id,$cid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id,parent_id,path,position');
		$query->from('#__photogallery_photos');
		$query->where('parent_id='.$parent_id);
		$query->where('id IN ('.implode(',',$cid).')');
		$db->setQuery($query);
		return $db->loadAssocList();
	
	}
}