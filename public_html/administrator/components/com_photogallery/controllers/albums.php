<?php

defined('_JEXEC') or die('Restricted Access');

class PhotogalleryControllerAlbums extends JControllerAdmin {
	
	public function getModel($name = 'Album',$prefix = 'PhotogalleryModel',$config = array()) {
		return parent::getModel($name,$prefix,$config);
	}
	public function delete(){
		$cid = $this->input->get('cid', array(), 'array');
		$photos = $this->control_get_path_photos($cid);
		foreach($photos as $photo){
			if(unlink(JPATH_ROOT.'/'.$photo['path'])){
				parent::delete();
			}
		}
	}
	public function control_get_path_photos($cid){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id,parent_id,path,position');
		$query->from('#__photogallery_photos');
		$query->where('parent_id IN ('.implode(',',$cid).')');
		$db->setQuery($query);
		return $db->loadAssocList();
	
	}
}