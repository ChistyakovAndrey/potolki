CREATE TABLE IF NOT EXISTS `#__masters_masters`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(255) NOT NULL,
`alias` varchar(255) NOT NULL,
`icon_href` varchar(255) NOT NULL,
`extra_data` TEXT NULL,
`state` tinyint(1) NOT NULL DEFAULT '1',
`asset_id` int(3) NOT NULL DEFAULT '0',
PRIMARY KEY (`id`)
)ENGINE=InnoDb DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__masters_masters_photos`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`parent_id` int(11) NOT NULL,
`tool_id` int(11) NOT NULL,
`name` varchar(255) NOT NULL,
`alias` varchar(255) NOT NULL,
`path` varchar(255) NOT NULL,
`state` tinyint(1) NOT NULL DEFAULT '1',
PRIMARY KEY (`id`),
KEY `id_master_parent` (`parent_id`),
CONSTRAINT `#__masters_master_photos_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `#__masters_masters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDb DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `#__masters_tools` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(255) NOT NULL,
`alias` varchar(255) NOT NULL,
`icon_href` varchar(255) NOT NULL,
`state` tinyint(1) NOT NULL DEFAULT '1',
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `#__masters_tools_photos`(
`id` int(11) NOT NULL AUTO_INCREMENT,
`parent_id` int(11) NOT NULL,
`name` varchar(255) NOT NULL,
`alias` varchar(255) NOT NULL,
`path` varchar(255) NOT NULL,
`state` tinyint(1) NOT NULL DEFAULT '1',
PRIMARY KEY (`id`),
KEY `id_tool` (`parent_id`),
CONSTRAINT `#__masters_tools_photos_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `#__masters_tools` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDb DEFAULT CHARSET=utf8;