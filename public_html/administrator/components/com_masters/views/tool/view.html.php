<?php
defined("_JEXEC") or die();

class MastersViewTool extends JViewLegacy {
	
	protected $form;
	protected $item;
	protected $input;
	
	
	public function display($tmpl = null) {
		
		$this->input = JFactory::getApplication()->input;
		$this->form = $this->get('Form');//getForm
		$this->item = $this->get('Item');//getItem
		$this->form->setFieldAttribute('icon_href', 'directory', 'com_masters/tools/'.$this->input->get('id'));
		$this->addToolBar();
		$this->setDocument();
		$this->check_folder_images();
		parent::display($tmpl);
	}
	
	protected function check_folder_images(){
		$cat;
		$cat_id;
		
			$cat =  $this->input->get('category');
			$cat_id =  $this->input->get('id');
		
		
		if(!is_dir(JPATH_SITE.'/images/com_masters')){
			mkdir(JPATH_SITE.'/images/com_masters');
		}
		if(!is_dir(JPATH_SITE.'/images/com_masters/'.$cat)){
			mkdir(JPATH_SITE.'/images/com_masters/'.$cat);
		}
		if(!is_dir(JPATH_SITE.'/images/com_masters/'.$cat.'/'.$cat_id)){
			mkdir(JPATH_SITE.'/images/com_masters/'.$cat.'/'.$cat_id);
		}
		
	}
	protected function addToolBar () {
		
		$isnew = ($this->item->id == 0);
		
		if($isnew) {
			$title = JText::_('COM_MASTERS_MANAGER_TOOL_NEW_TITLE');
		}
		else {
			
			$title = JText::_('COM_MASTERS_MANAGER_TOOL_EDIT_TITLE');
		}
		
		JToolBarHelper::title($title);
		JToolBarHelper::apply('tool.apply_n_stay');
		JToolBarHelper::save('tool.save_n_close');
		JToolBarHelper::cancel('tool.cancel');
		
		
		
	}
	
	protected function setDocument () {
		
		
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_MASTERS_MANAGER_TOOL_NEW_PAGE_TITLE'));
		$document->addStyleSheet(JUri::root(true).'/media/com_masters/css/style.css');
	}
}
?>