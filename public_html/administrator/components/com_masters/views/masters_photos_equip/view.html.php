<?php

defined("_JEXEC") or die();

class MastersViewMasters_photos_equip extends JViewLegacy {
	
	protected $items;
	protected $input;
	
	public function display($tpl = null) {
		$this->input = JFactory::getApplication()->input;
		//$this->sidebar = MastersHelper::addSubMenu('masters');
		$this->addToolBar();
		$this->setDocument();
		
		$this->items = $this->get('Items');///getItems()
		$this->pagination	= $this->get('Pagination');
		parent::display($tpl);
	}
	
	
	protected function addToolBar() {
	
		JToolbarHelper::title(JText::_("COM_MASTERS_MANAGER_MASTERS_PHOTOS_EQUIP"),'masters_photostitle');
		
		JToolbarHelper::addNew('master_photo_equip.add',JText::_('COM_MASTERS_MANAGER_ADD_MASTER_PHOTO'));
		JToolbarHelper::deleteList(JText::_('COM_MASTERS_MANAGER_DELETE_MASTERS_PHOTOS'),'masters_photos_equip.delete');
		JToolbarHelper::divider();
		
		JToolbarHelper::publish('masters_photos_equip.publish','JTOOLBAR_PUBLISH',TRUE);
		JToolbarHelper::unpublish('masters_photos_equip.unpublish','JTOOLBAR_UNPUBLISH',TRUE);
		JToolBarHelper::cancel('masters_photos_equip.cancel','JTOOLBAR_CLOSE');
		JToolbarHelper::preferences('com_masters');
	}
	protected function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
	protected function setDocument() {
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root(TRUE)."/media/com_masters/css/style.css");
		//print_r($document);
	}
	
}
?>