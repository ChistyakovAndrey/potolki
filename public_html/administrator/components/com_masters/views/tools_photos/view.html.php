<?php

defined("_JEXEC") or die();

class MastersViewTools_photos extends JViewLegacy {
	
	protected $items;
	protected $input;
	
	public function display($tpl = null) {
		$this->input = JFactory::getApplication()->input;
		$this->sidebar = MastersHelper::addSubMenu('tools');
		$this->addToolBar();
		$this->setDocument();
		
		$this->items = $this->get('Items');///getItems()
		
		parent::display($tpl);
	}
	
	
	protected function addToolBar() {
	
		JToolbarHelper::title(JText::_("COM_MASTERS_MANEGER_TYPES"),'toolstitle');
		
		JToolbarHelper::addNew('tool_photo.add',JText::_('COM_MASTERS_MANEGER_TYPES_ADD'));
		JToolbarHelper::deleteList(JText::_('COM_MASTERS_MANEGER_TYPES_DELETE_MSG'),'tools_photos.delete');
		JToolbarHelper::divider();
		
		JToolbarHelper::publish('tools_photos.publish','JTOOLBAR_PUBLISH',TRUE);
		JToolbarHelper::unpublish('tools_photos.unpublish','JTOOLBAR_UNPUBLISH',TRUE);
		JToolbarHelper::preferences('com_masters');
	}
	protected function pre($arr){
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
	protected function setDocument() {
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root(TRUE)."/media/com_masters/css/style.css");
		//print_r($document);
	}
	
}
?>