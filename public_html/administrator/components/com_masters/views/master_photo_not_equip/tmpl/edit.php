<?php
defined("_JEXEC") or die();


JHtml::_('formbehavior.chosen','select');
JHtml::_('behavior.keepalive');
?>

<form action="<?php echo JRoute::_(
	'index.php?option=com_masters
				&category=masters
				&parent_id='.$this->input->get('parent_id').'
				&id='.(int) $this->item->id.'
				&view=master_photo_not_equip
				&layout=edit');?>"
 method="post" id="adminForm" name="adminForm" class="form-validate">

<div class="row-fluid">
	<div class="span9">
		<?php echo JLayoutHelper::render('edit.title_alias',$this,'administrator/components/com_masters');?>
	</div>
	
	<div class="span3">
		<?php echo JLayoutHelper::render('edit.global',$this,'administrator/components/com_masters');?>
	</div>
</div>

<?php //echo $this->form->getField('id')->renderField();?>

<?php 

$this->form->bind(array('parent_id'=>$this->parent_id));

?>

<?php echo $this->form->renderFieldset('basic');?>


<input  type="hidden" name="task" value="type.edit"/>

<?php echo JHtml::_('form.token');//JHtmlForm?>
</form>