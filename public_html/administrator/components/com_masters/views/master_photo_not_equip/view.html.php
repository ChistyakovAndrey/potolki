<?php

defined('_JEXEC') or die('Restricted access');


class MastersViewMaster_photo_not_equip extends JViewLegacy {
	
	public $form = null;
	public $input;
	public $isNew;
	public $parent_id;
	protected $item = null; 
	public function display($tpl = null) 
	{
		$this->input = JFactory::getApplication()->input;
		$this->form	= $this->get('Form');//getForm
		$this->item = $this->get('Item');
		$this->form->setFieldAttribute('path', 'directory', 'com_masters/masters/'.$this->input->get('parent_id').'/not_equip');
		$this->form->setValue('equip', null, 'false');
		$this->form->setFieldAttribute('parent_id', 'value', $this->input->get('parent_id'));
		//$this->form->setFieldAttribute('parent_id', 'value', $this->input->get('parent_id'));
		$this->addToolBar();
 		$this->setDocument();
		parent::display($tpl);
	}
	protected function addToolBar() 
	{
	
		JFactory::getApplication()->input->set('hidemainmenu', true);
		
		$this->isNew = ($this->item->parent_id == 0);
		
		if ($this->isNew)
		{
			$title = JText::_('COM_MASTERS_MANAGER_MASTER_PHOTO_NEW');
			$this->parent_id = $this->input->get('parent_id');
		}
		else
		{
			$title = JText::_('COM_MASTERS_MANAGER_MASTER_PHOTO_EDIT');
			$this->parent_id = $this->item->parent_id;
		}
		
		JToolBarHelper::title($title,'master_phototitle');
		JToolBarHelper::apply('master_photo_not_equip.apply_n_stay');
		JToolBarHelper::save('master_photo_not_equip.save_n_close');
		JToolBarHelper::cancel('master_photo_not_equip.cancel',$this->isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE');
		
	}
	protected function setDocument() 
	{
		$document = JFactory::getDocument();
		/*$document->setTitle(JText::_('COM_DOSKA_MANEGER_TYPE_NEW_PAGE_TITLE'));*/
		$document->addStyleSheet(JUri::root(true).'/media/com_masters/css/style.css');
	}
	
	
	
}