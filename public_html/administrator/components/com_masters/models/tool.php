<?php
defined("_JEXEC") or die();

class MastersModelTool extends JModelAdmin {
	
	public function getForm($data = array(),$loadData = true) {
		
		
		$form = $this->loadForm(
					$this->option.'tool',
					'tool',
					array('control'=>'jform','load_data'=>$loadData)
				);
		
		
		if(empty($form)) {
			return FALSE;
		}
		
		return $form;
	}
	
	public function getTable($type = 'Tool', $prefix = 'MastersTable',$config = array()) {
		return JTable::getInstance($type,$prefix,$config);
	}
	
	protected function loadFormData(){
		$data = JFactory::getApplication()->getUserState('com_masters.edit.tool.data',array());
		
		if(empty($data)) {
			$data = $this->getItem();
		}
		
		return $data;
	}
	
	public function save($data) {
		
		if(!trim($data['name'])) {
			$this->setError(JText::_('COM_MASTERS_WARNING_PROVIDE_VALID_NAME'));
			return FALSE;
		}
		
		if(trim($data['alias']) == '') {
			$data['alias'] = $data['name'];
			$data['alias'] = JApplicationHelper::stringURLSafe($data['alias']);
		}
		
		
		if(parent::save($data)) {
			return TRUE;
		}
		return FALSE;
		
	}
}
?>