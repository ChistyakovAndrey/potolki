<?php
defined('_JEXEC') or die('Restricted access');


class MastersModelMasters extends JModelList {
	
	protected function getListQuery() {
		
		$query = parent::getListQuery();
		
		$query->select('id, name, state, alias, icon_href');
		$query->from('#__masters_masters');
		
		return $query;
	}
}
?>