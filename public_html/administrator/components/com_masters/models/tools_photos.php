<?php
defined('_JEXEC') or die('Restricted access');


class MastersModelTools_photos extends JModelList {
	
	protected function getListQuery() {
		
		$query = parent::getListQuery();
		
		$query->select('*');
		$query->from('#__masters_tools_photos');
		$query->where('parent_id='.JFactory::getApplication()->input->get('parent_id'));
		return $query;
	}
}
?>