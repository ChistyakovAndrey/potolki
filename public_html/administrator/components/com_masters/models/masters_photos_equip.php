<?php
defined('_JEXEC') or die('Restricted access');


class MastersModelMasters_photos_equip extends JModelList {
	

	protected function getListQuery() {
		
		$query = parent::getListQuery();
		
		$query->select('*');
		$query->from('#__masters_masters_photos');
		$query->where('parent_id='.JFactory::getApplication()->input->get('parent_id'));
		$query->where('equip="true"');
		return $query;
	}
}
?>