<?php

defined('_JEXEC') or die('Restricted Access');

class MastersControllerMasters_photos extends JControllerAdmin {
	
	public function getModel($name = 'Tools_photos',$prefix = 'MastersModel',$config = array( 'ignore_request' => true )) {
		return parent::getModel($name,$prefix,$config);
	}
	
}