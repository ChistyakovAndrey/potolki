<?php

defined('_JEXEC') or die('Restricted Access');

class MastersControllerMasters_photos_not_equip extends JControllerAdmin {
	
	public function getModel($name = 'Master_photo',$prefix = 'MastersModel',$config = array( 'ignore_request' => true )) {
		return parent::getModel($name,$prefix,$config);
	}
	public function delete(){
		parent::delete();
		$parent_id =  $this->input->get('parent_id');
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=masters&parent_id='.$parent_id.'&view=masters_photos_not_equip', false));
	}
	public function cancel(){
		//parent::cancel();
		$parent_id =  $this->input->get('parent_id');
		$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&category=masters&view=masters', false));
	}
	
}