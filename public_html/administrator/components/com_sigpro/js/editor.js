/**
 * @version    3.6.x
 * @package    Simple Image Gallery Pro
 * @author     JoomlaWorks - https://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2018 JoomlaWorks Ltd. All rights reserved.
 * @license    https://www.joomlaworks.net/license
 */

(function($) {
    window.SigProModal = function(el, link) {
        var href = $(el).attr('href') || link;
        $.fancybox.open({
            type: 'iframe',
            src: href,
            toolbar: false,
            smallBtn: true,
            iframe: {
                css: {
                    width: '96%',
                    height: '96%',
                    //'max-width': '1440px',
                    //'max-height': '1200px',
                    margin: 0,
                    padding: 0
                }
            }
        });
    }
    window.SigProModalClose = function() {
        $.fancybox.close();
    }
})(jQuery);
