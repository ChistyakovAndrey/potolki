<?php
/**
 * Пришлось сделать для совместимости со старыми версиями джумлы
 */

// аналоги старых классов ищи тут: /libraries/classmap.php

if (!class_exists('\Joomla\CMS\Factory')) {
    require_once __DIR__ . '/Factory.php';
}
if (!class_exists('\Joomla\CMS\Component\ComponentHelper')) {
    require_once __DIR__ . '/ComponentHelper.php';
}
if (!class_exists('\Joomla\CMS\HTML\HTMLHelper')) {
    require_once __DIR__ . '/HTMLHelper.php';
}
if (!class_exists('\Joomla\CMS\Uri\Uri')) {
    require_once __DIR__ . '/Uri.php';
}
if (!class_exists('\Joomla\CMS\MVC\Controller\BaseController')) {
    require_once __DIR__ . '/BaseController.php';
}
if (!class_exists('\Joomla\CMS\MVC\Controller\AdminController')) {
    require_once __DIR__ . '/AdminController.php';
}
if (!class_exists('\Joomla\CMS\MVC\View\HtmlView')) {
    require_once __DIR__ . '/HtmlView.php';
}
if (!class_exists('\Joomla\CMS\MVC\Model\AdminModel')) {
    require_once __DIR__ . '/AdminModel.php';
}
if (!class_exists('\Joomla\CMS\Helper\ContentHelper')) {
    require_once __DIR__ . '/ContentHelper.php';
}
if (!class_exists('Joomla\CMS\Language\Text')) {
    require_once __DIR__ . '/Text.php';
}