<?php
defined ('_JEXEC') or die ('restricted access');

$user = JFactory::getUser();
$input 	= JFactory::getApplication()->input;
$view 	= $input->get('view', NULL, 'STRING');
$option = $input->get('option', NULL, 'STRING');
$layout = $input->get('layout', NULL, 'STRING');
$component = $input->get('component', NULL, 'STRING');
$isMe = ($option == 'com_mycityselector');

if ($user->authorise('core.manage', 'com_mycityselector')) {
	?>
	<ul id="mycityselector-menu" class="nav <?= ($layout === 'edit') ? 'disabled': ''; ?>">
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<?= JText::_('MOD_MYCITYSELECTORADMINMENU_TITLE');?> <span class="caret"></span>
			</a>
			<ul aria-labelledby="dropdownMenu" role="menu" class="dropdown-menu">
				<li <?= ($isMe && ($view == '' || $view == 'countries') ) ? 'class="active"' : '' ?>>
					<a href="<?= JRoute::_('index.php?option=com_mycityselector&task=default&view=countries') ?>">
						<?= JText::_('MOD_MYCITYSELECTORADMINMENU_COUNTRIES') ?>
					</a>
				</li>
				<li <?= ($isMe && $view == 'provinces') ? 'class="active"': '' ?>>
					<a href="<?php echo JRoute::_('index.php?option=com_mycityselector&task=default&view=provinces'); ?>">
						<?= JText::_('MOD_MYCITYSELECTORADMINMENU_PROVINCES') ?>
					</a>
				</li>
				<li <?= ($isMe && $view == 'cities') ? 'class="active"': '' ?>>
					<a href="<?= JRoute::_('index.php?option=com_mycityselector&task=default&view=cities') ?>">
						<?= JText::_('MOD_MYCITYSELECTORADMINMENU_CITIES') ?>
					</a>
				</li>
				<li <?= ($isMe && $view == 'fields') ? 'class="active"' : '' ?>>
					<a href="<?= JRoute::_('index.php?option=com_mycityselector&task=default&view=fields') ?>">
						<?= JText::_('MOD_MYCITYSELECTORADMINMENU_FIELDS') ?>
					</a>
				</li>
				<li <?= ($option == 'com_mycityselector' && $view == 'component' && $component == 'com_mycityselector') ? 'class="active"' : '' ?>>
					<a href="<?= JRoute::_('index.php?option=com_config&view=component&component=com_mycityselector') ?>">
						<?= JText::_('MOD_MYCITYSELECTORADMINMENU_OPTIONS') ?>
					</a>
				</li>
			</ul>
		</li>
	</ul>
	<?php
}
