-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: potolki
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d20wc_masters_masters`
--

DROP TABLE IF EXISTS `d20wc_masters_masters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d20wc_masters_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `icon_href` varchar(255) NOT NULL,
  `page_url` varchar(255) DEFAULT NULL,
  `extra_data` text,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `asset_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d20wc_masters_masters`
--

LOCK TABLES `d20wc_masters_masters` WRITE;
/*!40000 ALTER TABLE `d20wc_masters_masters` DISABLE KEYS */;
INSERT INTO `d20wc_masters_masters` VALUES (13,'Алексей','aleksej','images/com_masters/masters/13/master-1.png','http://www.home_page_alexey.com','EXTRA_DATA_ALEXEY',1,251),(14,'Владимир','vladimir','images/com_masters/masters/14/master-2.png','http://www.home_page_vladimir.com','EXTRA_DATA_VLADIMIR',1,252),(15,'Жека','zheka','images/com_masters/masters/15/master-3.png','http://www.home_page_jeka.com','EXRA DATA JEKA',1,253),(16,'Игорь','igor','images/com_masters/masters/16/master-4.png','http://www.home_page_igor.com','EXTRA DATA IGOR',1,254),(17,'Леха','lekha','images/com_masters/masters/17/master-5.png','http://www.home_page_leha.com','EXTRA DATA LEHA',1,255),(18,'Миша','misha','images/com_masters/masters/18/master-6.png','http://www.home_page_misha.com','EXTRA DATA MISHA',1,256);
/*!40000 ALTER TABLE `d20wc_masters_masters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `d20wc_masters_masters_photos`
--

DROP TABLE IF EXISTS `d20wc_masters_masters_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d20wc_masters_masters_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `tool_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `equip` enum('true','false') NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_master_parent` (`parent_id`),
  CONSTRAINT `d20wc_masters_master_photos_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `d20wc_masters_masters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d20wc_masters_masters_photos`
--

LOCK TABLES `d20wc_masters_masters_photos` WRITE;
/*!40000 ALTER TABLE `d20wc_masters_masters_photos` DISABLE KEYS */;
INSERT INTO `d20wc_masters_masters_photos` VALUES (26,13,0,'alexey_not_equip','alexey-not-equip','images/com_masters/masters/13/not_equip/alexey_not_equip.jpg','false',1),(27,13,0,'alexey_equip','alexey-equip','images/com_masters/masters/13/equip/alexey_equip.jpg','true',1),(28,14,0,'vladimir_not_equip','vladimir-not-equip','images/com_masters/masters/14/not_equip/vladimir_not_equip.jpg','false',1),(29,14,0,'vladimir_equip','vladimir-equip','images/com_masters/masters/14/equip/vladimir_equip.jpg','true',1),(30,15,0,'jeka_not_equip','jeka-not-equip','images/com_masters/masters/15/not_equip/jeka_not_equip.jpg','false',1),(31,15,0,'jeka_equip','jeka-equip','images/com_masters/masters/15/equip/jeka_equip.jpg','true',1),(32,16,0,'igor_not_equip','igor-not-equip','images/com_masters/masters/16/not_equip/igor_not_equip.jpg','false',1),(33,16,0,'igor_equip','igor-equip','images/com_masters/masters/16/equip/igor_equip.jpg','true',1),(34,17,0,'leha_not_equip','leha-not-equip','images/com_masters/masters/17/not_equip/leha_not_equip.jpg','false',1),(35,17,0,'leha_equip','leha-equip','images/com_masters/masters/17/equip/leha_equip.jpg','true',1),(36,18,0,'misha_not_equip','misha-not-equip','images/com_masters/masters/18/not_equip/misha_not_equip.jpg','false',1),(37,18,0,'misha_equip','misha-equip','images/com_masters/masters/18/equip/misha_equip.jpg','true',1);
/*!40000 ALTER TABLE `d20wc_masters_masters_photos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-05  7:14:34
