<?php
class Fieryflash {
	public $params;
	public $app;
	public $doc;
	public $config;
	public $site;
	public $settings = [];
	
	public function __construct(){
		$this->set_params();
		$this->site = $this->getSettings();
	}
	private function set_params(){
			$this->params = JFactory::getApplication()->getTemplate(true)->params;
			$this->app = JFactory::getApplication();
			$this->doc = JFactory::getDocument();
			$this->config = JFactory::getConfig();
			$this->settings += ['tpath'=>$this->doc->baseurl . 'templates/' . $this->app->getTemplate()];
			$this->settings += ['minimization_html'=>$this->params->get('minimization_html', 1)];
			// Column widths
			
			if ($this->params->get('leftColumn', 1)){
				$this->settings += ['leftcolgrid'=>($this->countModules('left') == 0) ? 0 :	$this->params->get('leftColumnWidth', 3)];
			}
			
			if ($this->params->get('rightColumn', 1)){
				$this->settings += ['rightcolgrid'=>($this->countModules('right') == 0) ? 0 : $this->params->get('rightColumnWidth', 3)];
			}
			// Parameter
			$this->settings += ['layout'=>$this->params->get('layout')];
			$this->settings += ['scripts_bottom'=>$this->params->get('scripts_bottom', 1)];
			// Parameter
			$this->settings += ['frontpageshow'=>$this->params->get('frontpageshow', 0)];
			$this->settings += ['yandex_counter'=>$this->params->get('yandex_counter')];
			//name
			$this->settings += ['load_name'=>$this->params->get('load_name', 1)];
			$this->settings += ['load_slogan'=>$this->params->get('load_slogan', 1)];
			//phone
			$this->settings += ['telephone'=>($this->params->get('load_phone', 1)) ? $this->params->get('telephone'):null];
			//worktimes
			$this->settings += ['worktimes'=>($this->params->get('load_worktimes', 1)) ? $this->params->get('worktimes'):null];
			//email
			$this->settings += ['email'=>($this->params->get('load_email', 1)) ? $this->params->get('email'):null];						
			//address
			$this->settings += ['address'=>($this->params->get('load_address', 1)) ? $this->params->get('address'):null];
			$this->settings += ['user_stylesheet'=>$this->params->get('user_stylesheet')];
			$this->settings += ['backtotop'=>$this->params->get('backtotop', 1)];
			$this->settings += ['copyright'=>$this->params->get('copyright', 1)];
			
		// Use of Google Font
		if ($this->params->get('googleFont'))
		{
			JHtml::_('stylesheet', '//fonts.googleapis.com/css?family=' . $this->params->get('googleFontName'));
			$this->addStyleDeclaration("
			h1, h2, h3, h4, h5, h6, .site-title {
				font-family: '" . str_replace('+', ' ', $this->params->get('googleFontName')) . "', sans-serif;
			}");
		}
		//Body Font
		if( $this->params->get('bodyFont') ) 
		{
		    	JHtml::_('stylesheet', '//fonts.googleapis.com/css?family=' . $this->params->get('bodyFontName'));
				$this->addStyleDeclaration("
					body {
				font-family: '" . str_replace('+', ' ', $this->params->get('bodyFontName')) . "', sans-serif;
			}");
		}
		//Navigation Font
		if( $this->params->get('navigationFont') ) 
		{
		    	JHtml::_('stylesheet', '//fonts.googleapis.com/css?family=' . $this->params->get('navigationFontname'));
				$this->addStyleDeclaration("
					nav {
				font-family: '" . str_replace('+', ' ', $this->params->get('navigationFontname')) . "', sans-serif;
			}");
		}
	}
	
	/* Get template layout	*/
	private function getLayout(){
		$result = 'default';
		$this->app = JFactory::getApplication();
		$current = $this->app->input->cookie->get('layout');
		if (!empty($current)) {
			$result = $current;
		}
		return $result;
	}

	private function getSettings(){
			$result = new stdClass();
			$this->doc->setGenerator('Fieryflash template');

			// Add reset.css or normalize.css
			$result->reset_css = $this->params->get('reset_browsers_default_css');
			if ($result->reset_css == '1'){
				$this->doc->addStyleSheet($this->settings['tpath'] . '/css/reset.css');
				}
			if ($result->reset_css == '2'){
				$this->doc->addStyleSheet($this->settings['tpath'] . '/css/normalize.css');
				}


			//bootstrap 4
			$result->bootstrap4 = $this->params->get('bootstrap4');
			if ($result->bootstrap4 == '0'){
				$this->unsetDefaults();
			}
			if ($result->bootstrap4 == '1'){
				JHtml::_('bootstrap.framework');
				JHtml::_('bootstrap.loadCss');
			}
			if ($result->bootstrap4 == '2'){
				$this->bootstrap();
				$this->unsetDefaults();
			}

			if ($this->params->get('preloader', 1)){
				$this->preloader();
			}

			if ($this->params->get('responsive', 1)){
				$this->setMetaViewport();
			}

			// Name
			$result->name = $this->config->get('sitename');
			$result->slogan = $this->params->get('slogan');
			$result->decription = $this->config->get('MetaDesc');

			// Layout
			$result->layout = $this->getLayout();
			//setMetaTags
			if($this->params->get('setMetaTags', 1)){
				$this->SetMeta();
			}

			// favicon можно не указывать. достаточно кинуть картинку в корень шаблона
			$result->favicon = '';
			if(!empty($this->params->get('favicon'))) {
				$result->favicon = JUri::base(). $this->settings['tpath'] . '/images/favicon.ico';
				if($this->params->get('favicon') == '2' && !empty($this->params->get('favicon-custom'))){
					$result->favicon = JUri::base().$this->params->get('favicon-custom');
				}
				$this->doc->addFavicon($result->favicon);
			}

			// Logo
			$result->load_logo = $this->params->get('load_logo');
			if(!empty($result->load_logo)) {
				$result->logo = new stdClass();
				$result->logo->img = '';
				$result->logo->img = JUri::base(). $this->settings['tpath'] . '/images/logo.png';
					if($this->params->get('logo') == '2' && !empty($this->params->get('logo-custom'))){
						$result->logo->img = JUri::base().$this->params->get('logo-custom');
					}
				$result->logo->link = $this->params->get('logo_link');
				$result->logo->width = $this->params->get('logo_width');
				$result->logo->height = $this->params->get('logo_height');
				}

			//load template.css or style.css
			$result->default_css = $this->params->get('default_template_css');
			if ($result->default_css == '1'){
				$this->doc->addStyleSheet($this->settings['tpath'] . '/css/template.min.css');
				}
			if ($result->default_css == '2'){
				$this->doc->addStyleSheet($this->settings['tpath'] . '/css/style.css');}
			if ($result->default_css == '3'){
				$this->doc->addStyleSheet($this->settings['tpath'] . '/css/template.min.css');
				$this->doc->addStyleSheet($this->settings['tpath'] . '/css/style.css');
			}
			//	animate begin
			if ($this->params->get('animate_css', 1)){
				$this->doc->addStyleSheet($this->settings['tpath'] . '/css/animate.css');	
			};
			//fontawesome
			if ($this->params->get('fontawesome', 1)){
				$this->doc->addStyleSheet($this->settings['tpath'] . '/css/fontawesome-all.min.css');
			};
			
			//slick_carousel
			if ($this->params->get('slick_carousel', 1)){
				$this->slick();
			};

		return $result;
	}

	/* Set Meta Tags */
	private function setMeta(){
			$meta = new stdClass();
			// Title
			$meta->title = $this->doc->getTitle();
			// Description
			$meta->description = $this->doc->getMetaData('description'); 
			// Keywords
			$meta->keywords = $this->doc->getMetaData('keywords');

			// URL
			$meta->url = $this->doc->getMetaData('ulr');
			if (empty($meta->url)) {
				$meta->url = JUri::current(); 
				$this->doc->setMetaData('url', $meta->url);
			}
			// Image
			$meta->image = $this->doc->getMetaData('image');
			if (empty($meta->image)) {
				$meta->image = JUri::base(). $this->settings['tpath'] . '/images/noimage.jpg';
				$this->doc->setMetaData('image', $meta->image);
			}
			// Site Name
			$meta->siteName = JFactory::getConfig()->get('sitename');	
			// Set Open Graf
			$this->doc->setMetaData('og:type', 'website');
			$this->doc->setMetaData('og:title', $meta->title);
			$this->doc->setMetaData('og:description', $meta->description);
			$this->doc->setMetaData('og:site_name', $meta->siteName);
			$this->doc->setMetaData('og:url', $meta->url);
			$this->doc->setMetaData('og:image', $meta->image);
			// Twitter
			$this->doc->setMetaData('twitter:card', 'summary_large_image');
			$this->doc->setMetaData('twitter:title', $meta->title);
			$this->doc->setMetaData('twitter:description', $meta->description);
			$this->doc->setMetaData('twitter:site', $meta->siteName);
			$this->doc->setMetaData('twitter:url', $meta->url);
			$this->doc->setMetaData('twitter:image', $meta->image);	
		}

		//ViewPort
	private function setMetaViewport(){
			$this->doc->setMetaData('viewport', 'width=device-width, initial-scale=1, shrink-to-fit=no');
			$this->doc->setMetadata('x-ua-compatible', 'IE=edge,chrome=1');
			$stylelink = '<!--[if lt IE 9]>' ."\n";
			$stylelink .= '<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>' ."\n";
			$stylelink .= '<![endif]-->' ."\n";
			$this->doc->addCustomTag($stylelink);
	}

		//bootstrap 4
	private function bootstrap(){
			$this->doc->addStyleSheet('https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css');
			$this->doc->addScript('https://code.jquery.com/jquery-3.3.1.min.js');
			$this->doc->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js');
			$this->doc->addScript('https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js');
			return true;
	}

		//unset js
	private function unsetDefaults(){
			$headData = $this->doc->getHeadData();
			$scripts  = $headData['scripts'];
			unset(
				$scripts['/media/jui/js/bootstrap.min.js'],
				$scripts['/media/jui/js/jquery.min.js'],
				$scripts['/media/jui/js/jquery-noconflict.js'],
				$scripts['/media/jui/js/jquery-migrate.min.js'],
				// $scripts['/media/system/js/caption.js'],
				$scripts['/templates/fieryflash_template/js/jui/bootstrap.min.js'],
				$scripts['/templates/fieryflash_template/js/jui/jquery.min.js'],
				$scripts['/templates/fieryflash_template/js/jui/jquery-noconflict.js'],
				$scripts['/templates/fieryflash_template/js/jui/jquery-migrate.min.js']
			);
			$headData['scripts'] = $scripts;
			$this->doc->setHeadData($headData);
			return true;
	}

		//unset CSS
	private function unsetCss(){
			unset(
				$this->doc->_styleSheets[$this->settings['tpath'] . '/css/style.css'],
				$this->doc->_styleSheets[$this->settings['tpath'] . '/css/animate.css'],
				$this->doc->_styleSheets[$this->settings['tpath'] . '/css/touch-swipe.css'],
				$this->doc->_styleSheets[$this->settings['tpath'] . '/css/font-awesome.min.css'],
				$this->doc->_styleSheets[$this->settings['tpath'] . '/css/template.min.css'],
				$this->doc->_styleSheets[$this->settings['tpath'] . '/css/normalize.css'],
				$this->doc->_styleSheets[$this->settings['tpath'] . 'fieryflash_template/css/jui/bootstrap.min.css'],
				$this->doc->_styleSheets[$this->settings['tpath'] . '/templates/fieryflash_template/css/jui/bootstrap-responsive.min.css'],
				$this->doc->_styleSheets[$this->settings['tpath'] . '/templates/fieryflash_template/css/jui/bootstrap-extended.css'],
				$this->doc->_styleSheets[$this->settings['tpath'] . '/css/bootstrap.min.css']
				);
		return true;
	}

		//preloader
	private function preloader(){
			$this->doc->addStyleSheet($tpath . '/css/preloader.css');
			unset($this->doc->_styleSheets[$this->settings['tpath'] . '/css/animate.css']);
			$this->doc->addScriptDeclaration("
				function mypreloader() {
						setTimeout(function(){
						var preloader = document.getElementById('page-preloader');
						if ( !preloader.classList.contains('done') )
						{			preloader.classList.add('done');
						}
					}, 1000);
				};
			mypreloader();
			");
		return true;
	}

	//склонение городов начало
	public static function get_city_name($flag=true){
		if(class_exists('McsData')) $city = McsData::get('cityName');
		if($flag == false){
			return $city;
		}else{
			$arr_letters_for_replace = array("а","е","й","о","ю","я");
			$arr_letters_for_add = array("б","в","г","д","ж","з","к","л","м","н","п","р","с","т","ф","х","ц","ш","щ");
			$last_letter_position = mb_strlen($city)-1;
			$last_letter = mb_substr($city,$last_letter_position);
			foreach($arr_letters_for_replace as $letter){
				if($letter == $last_letter) {
					
					$city =  self::mb_substr_replace($city,"е",$last_letter_position); break;
				}
			}
			foreach($arr_letters_for_add as $let){
				if($let == $last_letter) {
					$city =  $city."е"; break;
				}
			}
			return $city;
		}
	}

	public static function mb_substr_replace($string, $replacement, $start, $length=NULL) {
	    if (is_array($string)) {
	        $num = count($string);
	        // $replacement
	        $replacement = is_array($replacement) ? array_slice($replacement, 0, $num) : array_pad(array($replacement), $num, $replacement);
	        // $start
	        if (is_array($start)) {
	            $start = array_slice($start, 0, $num);
	            foreach ($start as $key => $value)
	                $start[$key] = is_int($value) ? $value : 0;
	        }
	        else {
	            $start = array_pad(array($start), $num, $start);
	        }
	        // $length
	        if (!isset($length)) {
	            $length = array_fill(0, $num, 0);
	        }
	        elseif (is_array($length)) {
	            $length = array_slice($length, 0, $num);
	            foreach ($length as $key => $value)
	                $length[$key] = isset($value) ? (is_int($value) ? $value : $num) : 0;
	        }
	        else {
	            $length = array_pad(array($length), $num, $length);
	        }
	        // Recursive call
	        return array_map(__FUNCTION__, $string, $replacement, $start, $length);
	    }
	    preg_match_all('/./us', (string)$string, $smatches);
	    preg_match_all('/./us', (string)$replacement, $rmatches);
	    if ($length === NULL) $length = mb_strlen($string);
	    array_splice($smatches[0], $start, $length, $rmatches[0]);
	    return join($smatches[0]);
	}
//склонение городов конец

//slick slider
	private function slick(){
			$this->doc->addStyleSheet($this->settings['tpath'] . '/js/slick/slick.css');
			$this->doc->addStyleSheet($this->settings['tpath'] . '/js/slick/slick-theme.css');
			$this->doc->addScript($this->settings['tpath'] . '/js/slick/slick.min.js');

			$this->doc->addScriptDeclaration("
				(function($){
					$(document).ready(function () {

					});
				})(jQuery);
			");
	return true;
	}

	public function ob_html_compress($buf){
		return preg_replace(array('/<!--(?>(?!\[).)(.*)(?>(?!\]).)-->/Uis','/[[:blank:]]+/'),array('',' '),str_replace(array("\n","\r","\t"),'',$buf));
	}
}
	function ob_html_compress($buf){
		return preg_replace(array('/<!--(?>(?!\[).)(.*)(?>(?!\]).)-->/Uis','/[[:blank:]]+/'),array('',' '),str_replace(array("\n","\r","\t"),'',$buf));
	}
?>