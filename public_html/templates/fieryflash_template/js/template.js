(function($){
	$(document).ready(function () {
		$('.itemid178').children().append($('#mcs-app'));
		$('.slick-slider-responsive-1').slick({
			dots: true,
			infinite: false,
			speed: 300,
			slidesToShow: 1,
			slidesToScroll: 1,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			]
		});


		$('.slick-slider-responsive-2').slick({
			dots: true,
			infinite: false,
			speed: 300,
			slidesToShow: 2,
			slidesToScroll: 2,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			]
		});
		
		$('.slick-slider-responsive-3').slick({
			dots: true,
			infinite: false,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
		
		
		$('.slick-slider-responsive-4').slick({
			dots: true,
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			slidesToScroll: 4,
			responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
		});
	});
})(jQuery);

$(window).resize(function(){
	if ($(window).width() <= 980){
	$('.services-wrap').removeClass("row-flex");
	$('.brands-logo').removeClass("row-flex");
	$('.category-ceilings #itemListLeading').removeClass("row-flex");
	$('.product-intro').removeClass("no-gutters");
	$('.actions-items').removeClass("row-flex");
	}
});

$(window).resize(function(){
	if ($(window).width() >= 980){
		$('.dropdown').hover(function(){
			$('.dropdown-toggle', this).trigger('click');
		});
	}
});

(function($){
	$(document).ready(function () {
		if ($('.parent').children('ul').length > 0) {
			$('.parent').addClass('dropdown');
			$('.dropdown > a').addClass('dropdown-toggle');
			//$('.dropdown > a').attr('data-toggle', 'dropdown');
			$('.dropdown > a').attr('data-close-others', 'false');
			$('.dropdown > a').append('<b class="caret"></b>');
			$('.dropdown > ul').addClass('dropdown-menu');
			$('.main-menu ul li a').addClass('nav-link');
			$('.main-menu ul.nav-child li').addClass('dropdown-item');
		}

		//if ($('.parent').children('ul').length > 0) {
		//	$('.parent').addClass('dropdown');
		//	$('.dropdown > a').addClass('dropdown-toggle');
		//	$('.dropdown-toggle').attr('role', 'button');
		//	$('.dropdown-toggle').attr('aria-haspopup', 'true');
		//	$('.dropdown-toggle').attr('aria-expanded', 'false');
		//	$('.dropdown-toggle').attr('data-toggle', 'dropdown');
		//	$('.dropdown-toggle').attr('data-close-others', 'false');
		//	$('.dropdown-toggle').append('<b class="caret"></b>');
		//	$('.dropdown > ul').attr('aria-labelledby', 'navbarDropdown');
		//	$('.main-menu ul li a').addClass('nav-link');
		//}

	$('.navbar-nav').addClass('animated fadeInDown');
		// Fix mootools hide
		var bootstrapLoaded = (typeof $().carousel == 'function');
		var mootoolsLoaded = (typeof MooTools != 'undefined');
		if (bootstrapLoaded && mootoolsLoaded) {
			Element.implement({
				hide: function () {
					return this;
				},
				show: function (v) {
					return this;
				},
				slide: function (v) {
					return this;
				}
			});
		}

//$(window).resize(function(){
//	if ($(window).width() >= 980){
//      $(".navbar .dropdown-toggle").hover(function () {
//         $(this).parent().toggleClass("show");
//         $(this).parent().find(".dropdown-menu").toggleClass("show");
//       });
//      $( ".navbar .dropdown-menu" ).mouseleave(function() {
//        $(this).removeClass("show");
//      });
//	}
//});

	});
})(jQuery);