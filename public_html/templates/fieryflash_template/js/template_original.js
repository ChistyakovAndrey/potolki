(function($){
	$(document).ready(function () {
			if ($('.parent').children('ul').length > 0) {
				$('.parent').addClass('dropdown');
				$('.parent > a').addClass('dropdown-toggle btn-group');
				$('.parent > a').attr('data-toggle', 'dropdown');
				$('.parent > a').attr('data-close-others', 'false');
				$('.parent > a').append('<b class="caret"></b>');
				$('.parent > ul').addClass('dropdown-menu');
			}
			//add animation effects
			$('.navbar-nav').addClass('animated fadeInDown');
	});
})(jQuery);

//preloader begin
function mypreloader() {
		setTimeout(function(){
		var preloader = document.getElementById('page-preloader');
		if ( !preloader.classList.contains('done') )
		{			preloader.classList.add('done');
		}
	}, 1000);
	
}
//preloader end

(function($){
		$(document).ready(function () {
	  jQuery('.owl-carousel').owlCarousel({
                loop: true,
                margin: 0,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: false
                  },
                  600: {
                    items: 1,
                    nav: false
                  },
                  1000: {
                    items: 1,
                    nav: false,
                    loop: false,
                    margin: 0
                  }
                }
              });
		});
	})(jQuery);
	
	
	
	
//slick carousel
(function($){
	$(document).ready(function () {
		$('.slider-for').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  draggable: false,
		  fade: true,
		  adaptiveHeight: true,
		  asNavFor: '.slider-nav'
		});
		$('.slider-nav').slick({
		  slidesToShow: 6,
		  slidesToScroll: 1,
		  asNavFor: '.slider-for',
		  dots: false,
		  draggable: false,
		  centerMode: true,
		  adaptiveHeight: true,
		  focusOnSelect: true
		});
	});
})(jQuery);	
	
	
	
//panelnav	
(function($) {
	$(document).ready(function(){
		// Hide the bootstrap menu toggle button
		$('.navbar .navbar-header > button').css('display','none');
		// Add the link
		$('.navbar .navbar-header').append('<a id="panel-137-link" class="navbar-toggle panelnav-toggle hidden-sm hidden-md hidden-lg" href="#panel-137"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="title hidden-xs">Menu</span></a>');
		// position the link - uncomment if you want to position with JS instead of CSS
		// $('.navbar .navbar-inner > .navbar-toggle').css('position','absolute').css('top','1em').css('right','0');

		// panel Menu
		$(function() {
			$('nav#panel-137').mmenu({
				
				// options object
				content		: ['prev', 'title', 'close'],
				extensions	: ['pageshadow', 'theme-light', 'effect-slide-menu', 'effect-slide-listitems', 'pagedim-black'], //theme-dark, right, effect-zoom-menu, fullscreen
				dragOpen	: true,
				counters	: true,
				searchfield	: false,
				labels		: {
					fixed		: !$.mmenu.support.touch
				}			},
			{
				// configuration object
				selectedClass: "current"
			});
		});
	});
})(jQuery);