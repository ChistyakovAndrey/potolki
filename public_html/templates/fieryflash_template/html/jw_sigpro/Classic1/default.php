<?php
/**
 * @package     JoomlaZen Education Template
 * @version     0.5.5
 * @author      JoomlaZen - www.joomlazen.com
 * @copyright   Copyright (c) 2016 - 2016 JoomlaZen. All rights reserved.
 * @license     GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
defined('_JEXEC') or die('Restricted access');
$app = JFactory::getApplication();

if ($app->isAdmin()) {
	return;
}
$doc = JFactory::getDocument();
$doc->addScriptDeclaration( "
	(function($){
		$(document).ready(function() {			
			setHeight4x3($('#sigProId".$gal_id.".gallery .item .image'));		
		});
		$(window).resize(function () {		
			setHeight4x3($('#sigProId".$gal_id.".gallery .item .image'));
		});
	})(jQuery);
" );
?>
<div id="sigProId<?php echo $gal_id; ?>"  class="gallery uk-grid uk-grid-small <?php echo $singleThumbClass.$extraWrapperClass; ?>" data-uk-scrollspy="{cls:'uk-animation-fade uk-invisible', target:' > .item', delay:150}" data-uk-grid-match data-uk-grid-margin>
	<?php foreach($gallery as $count=>$photo): 
	?>
	<div class="item uk-invisible uk-width-xsmall-1-2 uk-width-small-1-3 uk-width-medium-1-4 uk-width-large-1-4 uk-width-xlarge-1-4">
		<a 
			class="image uk-thumbnail uk-display-block uk-cover-background" 
			href="<?php echo $photo->sourceImageFilePath; ?>" 
			style="background-image: url('<?php echo $photo->thumbImageFilePath; ?>');" 
			data-uk-lightbox="{group:'articleGallery'}" 
			title="<?php echo $photo->captionTitle; ?>" 
		>
		</a>
	</div>
	<?php endforeach; ?>
</div>