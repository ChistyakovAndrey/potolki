<?php
defined('_JEXEC') or die;
?>

<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>


<div class="jobs white-section">
	<div class="container">
		<?php if($this->params->get('show_page_title')): ?>
			<div class="row">
				<h1 class="pageTitle text-center"><?php echo $this->escape($this->params->get('page_title')); ?></h1>
			</div>
		<?php endif; ?>

		<?php if(isset($this->category) || ( $this->params->get('subCategories') && isset($this->subCategories) && count($this->subCategories) )): ?>
			<?php if(isset($this->category) && ( $this->params->get('catImage') || $this->params->get('catTitle') || $this->params->get('catDescription') || $this->category->event->K2CategoryDisplay )): ?>
				<div class="row categoryHeader">
					<?php if($this->params->get('catTitle')): ?>
						<div class="col-md-12">
							<h2 class="categoryTitle"><?php echo $this->category->name; ?></h2>
						</div>
					<?php endif; ?>

					<?php if($this->params->get('catDescription')): ?>
						<div class="col-md-12">
							<div class="categoryDescription text-center"><?php echo $this->category->description; ?></div>
						</div>
					<?php endif; ?>

					<?php echo $this->category->event->K2CategoryDisplay; ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<?php if((isset($this->leading) || isset($this->primary) || isset($this->secondary) || isset($this->links)) && (count($this->leading) || count($this->primary) || count($this->secondary) || count($this->links))): ?>
				<?php if(isset($this->leading) && count($this->leading)): ?>
					<div id="itemListLeading" class="grid row">
						<?php foreach($this->leading as $key=>$item): ?>
							<?php if((($key+1)%($this->params->get('num_leading_columns'))==0) || count($this->leading)<$this->params->get('num_leading_columns'))
									$lastContainer= ' itemContainerLast';
								else
									$lastContainer='';
								?>
								<div class="grid-sizer grid-item col-sm-<?php echo number_format(12/$this->params->get('num_leading_columns')); ?> ">
									<?php
										$this->item = $item;
										echo $this->loadTemplate('item');
									?>
								</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<?php if(isset($this->primary) && count($this->primary)): ?>
					<div id="itemListPrimary" class="row row-flex">
						<?php foreach($this->primary as $key=>$item): ?>
							<?php if( (($key+1)%($this->params->get('num_primary_columns'))==0) || count($this->primary)<$this->params->get('num_primary_columns') )
								$lastContainer= ' itemContainerLast';
							else
								$lastContainer='';
							?>
							<div class="grid-sizer grid-item col-sm-<?php echo number_format(12/$this->params->get('num_leading_columns')); ?>">
								<?php
									$this->item = $item;
									echo $this->loadTemplate('item');
								?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<?php if(isset($this->secondary) && count($this->secondary)): ?>
					<div id="itemListSecondary" class="row row-flex">
						<?php foreach($this->secondary as $key=>$item): ?>
							<?php if( (($key+1)%($this->params->get('num_secondary_columns'))==0) || count($this->secondary)<$this->params->get('num_secondary_columns') )
								$lastContainer= ' itemContainerLast';
							else
								$lastContainer='';
							?>
							<div class="grid-sizer grid-item col-sm-<?php echo number_format(12/$this->params->get('num_leading_columns')); ?>">
								<?php
									$this->item = $item;
									echo $this->loadTemplate('item');
								?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<?php if(isset($this->links) && count($this->links)): ?>
					<div id="itemListLinks" class="row row-flex">
						<h4><?php echo JText::_('K2_MORE'); ?></h4>
						<?php foreach($this->links as $key=>$item): ?>
							<?php if((($key+1)%($this->params->get('num_links_columns'))==0) || count($this->links)<$this->params->get('num_links_columns'))
								$lastContainer= ' itemContainerLast';
							else
								$lastContainer='';
							?>
							<div class="grid-sizer grid-item col-sm-<?php echo number_format(12/$this->params->get('num_leading_columns')); ?>">
								<?php
									$this->item = $item;
									echo $this->loadTemplate('item');
								?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

			<?php if($this->pagination->getPagesLinks()): ?>
				<div class="k2Pagination">
					<?php if($this->params->get('catPagination')) echo $this->pagination->getPagesLinks(); ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>



<script type="text/javascript">

$('.grid').masonry({
  itemSelector: '.grid-item', // use a separate class for itemSelector, other than .col-
  columnWidth: '.grid-sizer',
  horizontalOrder: true,
  percentPosition: true
});

</script>