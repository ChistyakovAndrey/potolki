<?php
defined('_JEXEC') or die;
// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
?>


	<div class="catItem">

	<div class="">
		<?php echo $this->item->event->BeforeDisplay; ?>
		<?php echo $this->item->event->K2BeforeDisplay; ?>

		<div class="catItemBody">
			<?php if($this->item->params->get('catItemTitle')): ?>
				<h3 class="catItemTitle"><?php echo $this->item->title; ?></h3>
			<?php endif; ?>
			
			<?php if($this->item->params->get('catItemIntroText')): ?>
				<div class="catItemIntroText">
					<?php echo $this->item->introtext; ?>
				</div>
			<?php endif; ?>			
			

			<?php if($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
				<div class="catItemExtraFields">
					<?php if (isset($this->item->extraFields->requirements) && !empty($this->item->extraFields->requirements->value)): ?>
						<div class="itemExtraField"><span><?php echo $this->item->extraFields->requirements->name; ?>: </span><?php echo $this->item->extraFields->requirements->value; ?></div>
					<?php endif; ?>

					<?php if (isset($this->item->extraFields->duties) && !empty($this->item->extraFields->duties->value)): ?>
						<div class="itemExtraField"><span><?php echo $this->item->extraFields->duties->name; ?>: </span><?php echo $this->item->extraFields->duties->value; ?></div>
					<?php endif; ?>

					<?php if (isset($this->item->extraFields->conditions) && !empty($this->item->extraFields->conditions->value)): ?>
						<div class="itemExtraField"><span><?php echo $this->item->extraFields->conditions->name; ?>: </span><?php echo $this->item->extraFields->conditions->value; ?></div>
					<?php endif; ?>

					<?php if (isset($this->item->extraFields->salary) && !empty($this->item->extraFields->salary->value)): ?>
						<div class="itemExtraField price"><?php echo $this->item->extraFields->salary->value; ?></div>
					<?php endif; ?>

					<?php if (isset($this->item->extraFields->link) && !empty($this->item->extraFields->link->value)): ?>
						<div class="itemExtraField link">
							<?php echo $this->item->extraFields->link->value; ?>
						</div>
					<?php endif; ?>

				</div>
			<?php endif; ?>

		</div>
		<?php echo $this->item->event->AfterDisplayTitle; ?>
		<?php echo $this->item->event->K2AfterDisplayTitle; ?>
	</div>

	<?php echo $this->item->event->AfterDisplay; ?>
	<?php echo $this->item->event->K2AfterDisplay; ?>

	</div>