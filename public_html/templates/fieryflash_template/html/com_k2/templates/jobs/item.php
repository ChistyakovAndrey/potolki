<?php
defined('_JEXEC') or die;
?>

<div class="ceiling-examples-wrap">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="ceiling-examples">
					<div class="row no-gutters">

						<div class="col-sm-5" style="padding: 0 5.5rem;">
								<?php if($this->item->params->get('itemTitle')): ?>
									<h2 class="itemTitle"><?php echo $this->item->title; ?> <?php echo $this->item->extraFields->area->value; ?></h2>
								<?php endif; ?>
							<hr/>
								<?php $dateTime = new DateTime($this->item->extraFields->create_date->rawValue);?>
								<div class="location"><?php echo $this->item->extraFields->city->value; ?></div>
								<div class="calendar"><?php echo $dateTime->format('d.m.Y'); ?></div>
							<hr/>

							<?php if(!empty($this->item->fulltext)): ?>
								<?php if($this->item->params->get('itemFullText')): ?>
								<div class="itemFullText">
									<?php echo $this->item->fulltext; ?>
								</div>
								<?php endif; ?>
							<?php endif; ?>
							<hr/>
							<div class="price"><?php echo $this->item->extraFields->price->value; ?></div>
						</div>

						<div class="col-sm-3">
							<?php if($this->item->params->get('itemImage') && !empty($this->item->image)): ?>
							<img src="<?php echo $this->item->image; ?>" alt="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>"/>
							<?php endif; ?>

							<?php if($this->item->params->get('itemImageGallery') && !empty($this->item->gallery)): ?>
								<?php echo $this->item->gallery; ?>
							<?php endif; ?>

							<?php if($this->item->params->get('itemVideo') && !empty($this->item->video)): ?>
								<?php if($this->item->videoType=='embedded'): ?>
								<div class="itemVideoEmbedded">
									<?php echo $this->item->video; ?>
								</div>
								<?php else: ?>
								<span class="itemVideo"><?php echo $this->item->video; ?></span>
								<?php endif; ?>
							<?php endif; ?>
						</div>

						<div class="col-sm-4" style="padding: 0 5rem;">
							<div class="row client-wrap">
								<div class="col-sm-4">
									<div class="client-avatar"><?php echo $this->item->extraFields->avatar->value; ?></div>
								</div>
								<div class="col-sm-8">
									<div class="client-name"><?php echo $this->item->extraFields->clientname->value; ?></div>
									<a href="#" class="client-link">https://vk.com/feed</a>
								</div>
							</div>

							<hr/>

							<div class="assessment-wrap">	
								<div class="row">
									<div class="col-sm-7">
										<span>Уровень обслуживания: </span>
									</div>
									<div class="col-sm-5">
										<ul class="assessment">
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-7">
										<span>Скорость выполнения: </span>
									</div>
									<div class="col-sm-5">
										<ul class="assessment">
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-7">
										<span>Качество монтажа: </span>
									</div>
									<div class="col-sm-5">
										<ul class="assessment">
											<li></li>
											<li></li>
											<li></li>
											<li></li>
											<li></li>
										</ul>
									</div>
								</div>
							</div>

							<hr/>

							<?php if($this->item->params->get('itemIntroText')): ?>
								<div class="itemIntroText">
									<?php echo $this->item->introtext; ?>
								</div>
							<?php endif; ?>


							<div class="ceilings-details">
								<ul>
									<li><span><?php echo $this->item->extraFields->dignities->name; ?>:</span> <?php echo $this->item->extraFields->dignities->value; ?></li>
									<li><span><?php echo $this->item->extraFields->flaws->name; ?>:</span> <?php echo $this->item->extraFields->flaws->value; ?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<?php echo $this->item->event->BeforeDisplay; ?>
	<?php echo $this->item->event->K2BeforeDisplay; ?>

	<?php echo $this->item->event->AfterDisplayTitle; ?>
	<?php echo $this->item->event->K2AfterDisplayTitle; ?>

	<?php echo $this->item->event->BeforeDisplayContent; ?>
	<?php echo $this->item->event->K2BeforeDisplayContent; ?>

	<?php echo $this->item->event->AfterDisplayContent; ?>
	<?php echo $this->item->event->K2AfterDisplayContent; ?>

	<?php echo $this->item->event->AfterDisplay; ?>
	<?php echo $this->item->event->K2AfterDisplay; ?>