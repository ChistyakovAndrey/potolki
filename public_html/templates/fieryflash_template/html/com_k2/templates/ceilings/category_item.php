<?php
defined('_JEXEC') or die;
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
?>

<div class="catItem reviewItem">
	<div class="col-sm-12">
		<?php if (isset($this->item->extraFields->avatar) && !empty($this->item->extraFields->avatar->value)): ?>
			<div class="row">
				<div class="catItemHeader">
          <div class="image-wrap">
            <?php echo $this->item->extraFields->avatar->value; ?><a href="<?php echo $this->item->link; ?>"></a>
          </div>
				</div>
			</div>
		<?php endif; ?>
	</div>

	<div class="col-sm-12">
		<?php echo $this->item->event->BeforeDisplay; ?>
		<?php echo $this->item->event->K2BeforeDisplay; ?>

		<div class="catItemBody">
			<?php if($this->item->params->get('catItemTitle')): ?>
				<h3 class="catItemTitle"><?php echo $this->item->title; ?>
				<?php if (isset($this->item->extraFields->area) && !empty($this->item->extraFields->area->value)): ?>
					<?php echo $this->item->extraFields->area->value; ?>
				<?php endif; ?>
				</h3>
			<?php endif; ?>

			<?php if (isset($this->item->extraFields->create_date) && !empty($this->item->extraFields->create_date->value)): ?>
        <?php $dateTime = new DateTime($this->item->extraFields->create_date->rawValue);?>
        Отзыв от <?php echo $dateTime->format('d.m.Y'); ?>
			<?php endif; ?>

			<hr/>
			<?php if (isset($this->item->extraFields->price) && !empty($this->item->extraFields->price->value)): ?>
        <div class="price"><?php echo $this->item->extraFields->price->value; ?></div>
			<?php endif; ?>
		</div>

		<?php echo $this->item->event->AfterDisplayTitle; ?>
		<?php echo $this->item->event->K2AfterDisplayTitle; ?>

	</div>

	<?php echo $this->item->event->AfterDisplay; ?>
	<?php echo $this->item->event->K2AfterDisplay; ?>
</div>