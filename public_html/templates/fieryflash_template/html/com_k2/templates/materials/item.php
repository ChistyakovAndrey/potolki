<?php
defined('_JEXEC') or die;
?>

<div class="materials container">
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->item->event->BeforeDisplay; ?>
			<?php echo $this->item->event->K2BeforeDisplay; ?>

			<div class="itemHeader text-center">
				<?php if($this->item->params->get('itemTitle')): ?>
					<h2 class="itemTitle"><?php echo $this->item->title; ?></h2>
				<?php endif; ?>
			</div>

			<?php echo $this->item->event->AfterDisplayTitle; ?>
			<?php echo $this->item->event->K2AfterDisplayTitle; ?>

			<div class="itemBody">
				<?php echo $this->item->event->BeforeDisplayContent; ?>
				<?php echo $this->item->event->K2BeforeDisplayContent; ?>

				<?php if($this->item->params->get('itemImage') && !empty($this->item->image)): ?>
					<div class="itemImageBlock">
						<span class="itemImage">
							<a data-k2-modal="image" href="<?php echo $this->item->imageXLarge; ?>" title="<?php echo JText::_('K2_CLICK_TO_PREVIEW_IMAGE'); ?>">
								<img src="<?php echo $this->item->image; ?>" alt="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>" style="width:<?php echo $this->item->imageWidth; ?>px; height:auto;" />
							</a>
						</span>
					</div>
				<?php endif; ?>

				<?php if(!empty($this->item->fulltext)): ?>

				<?php if($this->item->params->get('itemIntroText')): ?>
					<div class="itemIntroText">
						<?php echo $this->item->introtext; ?>
					</div>
				<?php endif; ?>

				<?php if($this->item->params->get('itemFullText')): ?>
					<div class="itemFullText">
						<?php echo $this->item->fulltext; ?>
					</div>
				<?php endif; ?>

				<?php else: ?>
					<div class="itemFullText">
						<?php echo $this->item->introtext; ?>
					</div>
				<?php endif; ?>

				<?php echo $this->item->event->AfterDisplayContent; ?>
				<?php echo $this->item->event->K2AfterDisplayContent; ?>
			</div>

			<?php echo $this->item->event->AfterDisplay; ?>
			<?php echo $this->item->event->K2AfterDisplay; ?>
		</div>
	</div>
</div>