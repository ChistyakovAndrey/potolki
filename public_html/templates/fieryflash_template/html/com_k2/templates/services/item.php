<?php
defined('_JEXEC') or die;
$template = 'fieryflash_template';
JLoader::register('Fieryflash', JPATH_THEMES.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'functions.php');
$city = Fieryflash::get_city_name(true);
?>
<div class="service-item">
	<div class="container">
		<!-- Plugins: BeforeDisplay -->
		<?php echo $this->item->event->BeforeDisplay; ?>
		<!-- K2 Plugins: K2BeforeDisplay -->
		<?php echo $this->item->event->K2BeforeDisplay; ?>

		<div class="row itemHeader">
			<div class="col-sm-12 text-center">
				<?php if($this->item->params->get('itemTitle')): ?>
				<!-- Item title -->
				<h2 class="itemTitle">
					<?php echo $this->item->title; ?> <?php echo $city;?>
				</h2>
				<?php endif; ?>
			</div>
		</div>

		<!-- Plugins: AfterDisplayTitle -->
		<?php echo $this->item->event->AfterDisplayTitle; ?>
		<!-- K2 Plugins: K2AfterDisplayTitle -->
		<?php echo $this->item->event->K2AfterDisplayTitle; ?>


		<div class="row itemBody">
			<div class="col-md-12">
				<!-- Plugins: BeforeDisplayContent -->
				<?php echo $this->item->event->BeforeDisplayContent; ?>
				<!-- K2 Plugins: K2BeforeDisplayContent -->
				<?php echo $this->item->event->K2BeforeDisplayContent; ?>

				<?php if(!empty($this->item->fulltext)): ?>
					<?php if($this->item->params->get('itemIntroText')): ?>
					<!-- Item introtext -->
					<div class="itemIntroText">
						<?php echo $this->item->introtext; ?>
					</div>
					<?php endif; ?>

					<?php if($this->item->params->get('itemFullText')): ?>
					<!-- Item fulltext -->
					<div class="itemFullText">
						<?php echo $this->item->fulltext; ?>
					</div>
					<?php endif; ?>

					<?php else: ?>

					<!-- Item text -->
					<div class="itemFullText">
						<?php echo $this->item->introtext; ?>
					</div>
				<?php endif; ?>
				<?php if($this->item->params->get('itemExtraFields') && isset($item) && !is_null($item->extra_fields) && count($this->item->extra_fields)): ?>
					<!-- Item extra fields -->
				<?php endif; ?>

				<!-- Plugins: AfterDisplayContent -->
				<?php echo $this->item->event->AfterDisplayContent; ?>
				<!-- K2 Plugins: K2AfterDisplayContent -->
				<?php echo $this->item->event->K2AfterDisplayContent; ?>
			</div>
		</div>
	</div>
	<!-- Plugins: AfterDisplay -->
	<?php echo $this->item->event->AfterDisplay; ?>
	<!-- K2 Plugins: K2AfterDisplay -->
	<?php echo $this->item->event->K2AfterDisplay; ?>
</div>