<?php
defined('_JEXEC') or die;
// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
?>

<div class="video-item">
	<?php echo $this->item->event->BeforeDisplay; ?>
	<?php echo $this->item->event->K2BeforeDisplay; ?>

	<?php if($this->item->params->get('catItemVideo') && !empty($this->item->video)): ?>
		<div class="catItemVideoBlock">
			<?php if($this->item->videoType=='embedded'): ?>
			<div class="catItemVideoEmbedded">
				<?php echo $this->item->video; ?>
			</div>
			<?php else: ?>
			<span class="catItemVideo"><?php echo $this->item->video; ?></span>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<?php echo $this->item->event->AfterDisplayTitle; ?>
	<?php echo $this->item->event->K2AfterDisplayTitle; ?>

	<div class="catItemBody">
		<?php echo $this->item->event->BeforeDisplayContent; ?>
		<?php echo $this->item->event->K2BeforeDisplayContent; ?>

		<?php if($this->item->params->get('catItemTitle')): ?>
			<h3 class="catItemTitle"><?php echo $this->item->title; ?></h3>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemIntroText')): ?>
			<div class="catItemIntroText">
				<?php echo $this->item->introtext; ?>
			</div>
		<?php endif; ?>

		<?php echo $this->item->event->AfterDisplayContent; ?>
		<?php echo $this->item->event->K2AfterDisplayContent; ?>
	</div>

	<?php echo $this->item->event->AfterDisplay; ?>
	<?php echo $this->item->event->K2AfterDisplay; ?>
</div>