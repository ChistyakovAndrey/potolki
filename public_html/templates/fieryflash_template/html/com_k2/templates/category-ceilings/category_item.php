<?php
defined('_JEXEC') or die;
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
if(class_exists('McsData')) $cityHref = McsData::get('city');
?>

<div class="catItem">

	<?php if($this->item->params->get('catItemImage') && !empty($this->item->image)): ?>
		<div class="col-sm-12">
			<div class="row">
				<div class="image-wrap">
					<img src="<?php echo $this->item->image; ?>" alt="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>"/>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="col-sm-12">
		<?php echo $this->item->event->BeforeDisplay; ?>
		<?php echo $this->item->event->K2BeforeDisplay; ?>

		<div class="catItemBody">
			<?php if($this->item->params->get('catItemTitle')): ?>
				<h3 class="catItemTitle"><?php echo $this->item->title; ?> <br/>S=<?php echo $this->item->extraFields->area->value; ?> м2</h3>
			<?php endif; ?>

			<div class="catItemExtraField">
				<div class="catItemExtraFieldTitle">Включено:</div>


				<div class="">
					<div class="col-sm-6">
						<div><?php echo $this->item->extraFields->area->name; ?> <?php echo $this->item->extraFields->area->value; ?> м2</div>
						<div><?php echo $this->item->extraFields->cornermachining->name; ?> <?php echo $this->item->extraFields->cornermachining->value; ?></div>
						<div><?php echo $this->item->extraFields->pipebypass->name; ?> <?php echo $this->item->extraFields->pipebypass->value; ?></div>
					</div>
					<div class="col-sm-6">
						<div><?php echo $this->item->extraFields->profile->name; ?> <?php echo $this->item->extraFields->profile->value; ?></div>
						<div><?php echo $this->item->extraFields->holes->name; ?> <?php echo $this->item->extraFields->holes->value; ?></div>
						<div><?php echo $this->item->extraFields->lampstand->name; ?> <?php echo $this->item->extraFields->lampstand->value; ?></div>
					</div>
				</div>


			</div>
			<hr/>
			<?php if($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
			<div class="row">
				<div class="col-sm-6">
					<?php if (isset($this->item->extraFields->price) && !empty($this->item->extraFields->price->value)): ?>
						<div class="price"><?php echo $this->item->extraFields->price->value; ?></div>
					<?php endif; ?>
				</div>

				<div class="col-sm-6">
					<?php if (isset($this->item->extraFields->freeservice) && !empty($this->item->extraFields->freeservice->value)): ?>
						<div class="catItemExtraField">
							<span class="free-service"><?php echo $this->item->extraFields->freeservice->name; ?></span>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<?php endif; ?>

			<hr/>

			<div class="row">
				<div class="col-sm-12">
					<?php if ($this->item->params->get('catItemReadMore')): ?>
						<!--<a class="light-dark-button" href="<//?php echo $this->item->link; ?>">Подробнее</a>-->
						<a class="light-dark-button" href="<?php printf($cityHref);?>/natyazhnye-potolki/tekhnologii-potolkov">Подробнее</a>
					<?php endif; ?>
					<a href="<?php printf($cityHref);?>/natyazhnye-potolki/kalkulyator-potolkov" class="light-dark-button go-to-calculator">Калькулятор</a>				
				</div>

			</div>
		</div>
		<?php echo $this->item->event->AfterDisplayTitle; ?>
		<?php echo $this->item->event->K2AfterDisplayTitle; ?>

	</div>

	<?php echo $this->item->event->AfterDisplay; ?>
	<?php echo $this->item->event->K2AfterDisplay; ?>
</div>