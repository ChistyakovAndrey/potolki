<?php
defined('_JEXEC') or die;
if(class_exists('McsData')) $cityHref = McsData::get('city');
?>
<div class="category-ceilings gray-section">
	<div class="container">

		<?php if($this->params->get('show_page_title')): ?>
			<div class="row tagsItemsPageTitle">
				<h2 class="text-center"><?php echo $this->escape($this->params->get('page_title')); ?></h2>
			</div>
		<?php endif; ?>

	<?php if(count($this->items)): ?>
			<div class="row">
				<?php foreach($this->items as $item): ?>
				<div class="col-sm-6 tagItemView">
					<div class="catItem">
						<?php if($item->params->get('tagItemImage',1) && !empty($item->imageGeneric)): ?>
							<div class="row">
								<div class="col-sm-12">
									<div class="image-wrap">
										<img src="<?php echo $item->imageGeneric; ?>" alt="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>"/>
									</div>
								</div>
							</div>
						<?php endif; ?>

						<div class="row">
							<div class="col-sm-12">
								<div class="catItemBody">
									<?php if($item->params->get('tagItemTitle',1)): ?>
										<h3 class="catItemTitle"><?php echo $item->title; ?> <br/>S=<?php echo $item->extraFields->area->value; ?> м2</h3>
									<?php endif; ?>

									<?php if($item->params->get('tagItemExtraFields',0) && count($item->extra_fields)): ?>
										<div class="catItemExtraField">
											<div class="catItemExtraFieldTitle">Включено:</div>
											<div class="row">
												<div class="col-sm-6">
													<div><?php echo $item->extraFields->area->name; ?> <?php echo $item->extraFields->area->value; ?> м2</div>
													<div><?php echo $item->extraFields->cornermachining->name; ?> <?php echo $item->extraFields->cornermachining->value; ?></div>
													<div><?php echo $item->extraFields->pipebypass->name; ?> <?php echo $item->extraFields->pipebypass->value; ?></div>
												</div>
												<div class="col-sm-6">
													<div><?php echo $item->extraFields->profile->name; ?> <?php echo $item->extraFields->profile->value; ?></div>
													<div><?php echo $item->extraFields->holes->name; ?> <?php echo $item->extraFields->holes->value; ?></div>
													<div><?php echo $item->extraFields->lampstand->name; ?> <?php echo $item->extraFields->lampstand->value; ?></div>
												</div>
											</div>
										</div>
									<?php endif; ?>
									<hr/>
									<?php if($item->params->get('tagItemExtraFields',0) && count($item->extra_fields)): ?>
									<div class="row">
										<div class="col-sm-6">
											<?php if (isset($item->extraFields->price) && !empty($item->extraFields->price->value)): ?>
												<div class="price"><?php echo $item->extraFields->price->value; ?></div>
											<?php endif; ?>
										</div>

										<div class="col-sm-6">
											<?php if (isset($item->extraFields->freeservice) && !empty($item->extraFields->freeservice->value)): ?>
												<div class="catItemExtraField">
													<span class="free-service"><?php echo $item->extraFields->freeservice->name; ?></span>
												</div>
											<?php endif; ?>
										</div>
									</div>
									<?php endif; ?>
									<hr/>
									<div class="row">
										<div class="col-sm-12">
											<?php if ($item->params->get('tagItemReadMore')): ?>
												<!--<a class="light-dark-button" href="<//?php echo $item->link; ?>">Подробнее</a>-->
												<a class="light-dark-button" href="<?php printf($cityHref);?>/natyazhnye-potolki/tekhnologii-potolkov">Подробнее</a>
											<?php endif; ?>

											<a href="<?php printf($cityHref);?>/natyazhnye-potolki/kalkulyator-potolkov" class="light-dark-button go-to-calculator">Калькулятор</a>				
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
      <?php endif; ?>

		<?php if($this->pagination->getPagesLinks()): ?>
			<div class="row k2Pagination">
				<?php echo $this->pagination->getPagesLinks(); ?>
			</div>
		<?php endif; ?>
		
</div>
</div>