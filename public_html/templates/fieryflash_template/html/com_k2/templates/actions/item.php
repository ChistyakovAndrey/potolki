<?php
defined('_JEXEC') or die;
?>

<div class="actions gray-section action-item">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-12">
						<div class="action-item-header">
							<?php if($this->item->params->get('itemTitle')): ?>
								<h2 class="itemTitle text-center"><?php echo $this->item->title; ?></h2>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="catItemWrap">
					<div class="categoryItem">
						<div class="catItemHeader" style="background:url(<?php echo $this->item->image; ?>) no-repeat center; background-size:cover;">
							<?php if($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
								<div class="row">
									<div class="col-sm-8">
										<?php if (isset($this->item->extraFields->title) && !empty($this->item->extraFields->title->value)): ?>
											<div class="action-title">
												<span><?php echo $this->item->extraFields->title->value; ?></span>
												<?php echo $this->item->extraFields->aftertitle->value; ?>
											</div>
										<?php endif; ?>
									</div>
									<div class="col-sm-4">
									<?php if (!empty($this->item->extraFields->validity->value) || !empty($this->item->extraFields->labelbigtext->value)): ?>
										<div class="action-label">
											<div class="action-label-wrap">
											<?php if (isset($this->item->extraFields->validity) && !empty($this->item->extraFields->validity->value)): ?>
												<div class="action-date">
													<?php $dateTime = new DateTime($this->item->extraFields->validity->rawValue);?>
													<span>До </span><?php echo $dateTime->format('d.m.Y'); ?>
												</div>
											<?php endif; ?>
												<div class="action-price">
													<?php if (isset($this->item->extraFields->labelbigtext) && !empty($this->item->extraFields->labelbigtext->value)): ?>
														<span><?php echo $this->item->extraFields->labelbigtext->value; ?></span>
													<?php endif; ?>
													<?php if (isset($this->item->extraFields->labelsmalltext) && !empty($this->item->extraFields->labelsmalltext->value)): ?>
														<?php echo $this->item->extraFields->labelsmalltext->value; ?>
													<?php endif; ?>
												</div>
											</div>
										</div>
									<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>	
						</div>

						<?php echo $this->item->event->BeforeDisplay; ?>
						<?php echo $this->item->event->K2BeforeDisplay; ?>
						<div class="catItemBody">
							<?php if($this->item->params->get('catItemTitle')): ?>
								<h3 class="catItemTitle"><?php echo $this->item->title; ?></h3>
							<?php endif; ?>
							<?php if($this->item->params->get('catItemIntroText')): ?>
								<div class="catItemIntroText">
									<?php echo $this->item->introtext; ?>
								</div>
							<?php endif; ?>
						</div>
						<?php echo $this->item->event->AfterDisplayTitle; ?>
						<?php echo $this->item->event->K2AfterDisplayTitle; ?>
						<?php echo $this->item->event->AfterDisplay; ?>
						<?php echo $this->item->event->K2AfterDisplay; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>