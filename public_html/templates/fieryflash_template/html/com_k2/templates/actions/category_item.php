<?php
defined('_JEXEC') or die;
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
$viewtype = $this->item->extraFields->viewtype->value; //тип макета
?>
<?php if (isset($this->item->extraFields->viewtype) && !empty($this->item->extraFields->viewtype->value)): ?>
	<?php if($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
		<?php if ($viewtype == 'type1'): ?>
			<!-- шаблон вывода 1 -->
				<div class="categoryItem-<?php echo $viewtype; ?>">
					<div class="catItemHeader" style="background:url(<?php echo $this->item->image; ?>) no-repeat center; background-size:cover;">
					<div class="row">
						<div class="col-xs-12 col-sm-8">
							<?php if($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
								<?php if (isset($this->item->extraFields->title) && !empty($this->item->extraFields->title->value)): ?>
									<div class="action-title">
										<span><?php echo $this->item->extraFields->title->value; ?></span>
										<?php echo $this->item->extraFields->aftertitle->value; ?>
									</div>
								<?php endif; ?>
							<?php endif; ?>
						</div>
			<div class="col-xs-12 col-sm-4" style="position:relative;">
						<?php if($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>

							<?php if (
								!empty($this->item->extraFields->validity->value) ||
								!empty($this->item->extraFields->labelbigtext->value))
							: ?>
								<div class="action-label">
									<div class="action-label-wrap">
									<?php if (isset($this->item->extraFields->validity) && !empty($this->item->extraFields->validity->value)): ?>
										<div class="action-date">
											<?php $dateTime = new DateTime($this->item->extraFields->validity->rawValue);?>
											<span>До </span><?php echo $dateTime->format('d.m.Y'); ?>
										</div>
									<?php endif; ?>

									<?php if (isset($this->item->extraFields->labelbigtext) && !empty($this->item->extraFields->labelbigtext->value)): ?>
										<div class="action-price">
											<span><?php echo $this->item->extraFields->labelbigtext->value; ?></span>
											<?php if(isset($this->item->extraFields->labelsmalltext) && !is_null($this->item->extraFields->labelsmalltext->value)) echo $this->item->extraFields->labelsmalltext->value; ?>
										</div>
									<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>

						<?php endif; ?>
				</div>
			</div>
					</div>

					<?php echo $this->item->event->BeforeDisplay; ?>
					<?php echo $this->item->event->K2BeforeDisplay; ?>

					<div class="catItemBody">
						<?php if($this->item->params->get('catItemTitle')): ?>
							<h3 class="catItemTitle h4"><?php echo $this->item->title; ?></h3>
						<?php endif; ?>
						
						<?php if($this->item->params->get('catItemIntroText')): ?>
							<div class="catItemIntroText">
								<?php echo $this->item->introtext; ?>
							</div>
						<?php endif; ?>

					</div>

					<?php echo $this->item->event->AfterDisplayTitle; ?>
					<?php echo $this->item->event->K2AfterDisplayTitle; ?>

					<?php echo $this->item->event->AfterDisplay; ?>
					<?php echo $this->item->event->K2AfterDisplay; ?>

					<?php if ($this->item->params->get('catItemReadMore')): ?>
						<div class="catItemReadMore">
							<a class="light-dark-button" href="<?php echo $this->item->link; ?>">Подробнее</a>
						</div>
					<?php endif; ?>
				</div>
		<?php else : ?>
	<!-- шаблон вывода 2 -->
	<div class="categoryItem-<?php echo $viewtype; ?>" style="background: url(<?php echo $this->item->image; ?>) no-repeat center;">
		<div class="catItemHeader">
			<div class="action-label-2">
				<?php if (isset($this->item->extraFields->validity) && !empty($this->item->extraFields->validity->value)): ?>
					<div class="action-date">
						<?php $dateTime = new DateTime($this->item->extraFields->validity->rawValue);?>
						<span>До </span><?php echo $dateTime->format('d.m.Y'); ?>
					</div>
				<?php endif; ?>

				<?php if (isset($this->item->extraFields->labelbigtext) && !empty($this->item->extraFields->labelbigtext->value)): ?>
					<div class="action-price">
						<span><?php echo $this->item->extraFields->labelbigtext->value; ?></span>
						<?php if(isset($this->item->extraFields->labelsmalltext) && !is_null($this->item->extraFields->labelsmalltext->value)) echo $this->item->extraFields->labelsmalltext->value; ?>
					</div>
				<?php endif; ?>
			</div>
			<?php if($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
				<?php if (isset($this->item->extraFields->title) && !empty($this->item->extraFields->title->value)): ?>
					<div class="action-title">
						<span><?php echo $this->item->extraFields->title->value; ?></span>
						<?php echo $this->item->extraFields->aftertitle->value; ?>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>

		<?php echo $this->item->event->BeforeDisplay; ?>
		<?php echo $this->item->event->K2BeforeDisplay; ?>

		<?php echo $this->item->event->AfterDisplayTitle; ?>
		<?php echo $this->item->event->K2AfterDisplayTitle; ?>

		<?php echo $this->item->event->AfterDisplay; ?>
		<?php echo $this->item->event->K2AfterDisplay; ?>

		<?php if ($this->item->params->get('catItemReadMore')): ?>
			<div class="catItemReadMore">
				<a class="light-dark-button" href="<?php echo $this->item->link; ?>">Подробнее</a>
			</div>
		<?php endif; ?>
	</div>
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>