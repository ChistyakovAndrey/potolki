<?php
defined('_JEXEC') or die;
?>
<div class="ceilings-photo-section">
	<div class="container">
	<?php echo $this->item->event->BeforeDisplay; ?>
		<?php echo $this->item->event->K2BeforeDisplay; ?>
		<?php if($this->item->params->get('itemTitle')): ?>
			<div class="row">
				<div class="col-xs-12 ceilings-photo-section-title">
					<h3 class="itemTitle"><?php echo $this->item->title; ?></h3>
				</div>
			</div>
		<?php endif; ?>
		<?php echo $this->item->event->AfterDisplayTitle; ?>
		<?php echo $this->item->event->K2AfterDisplayTitle; ?>
		<?php echo $this->item->event->BeforeDisplayContent; ?>
		<?php echo $this->item->event->K2BeforeDisplayContent; ?>

		<?php if($this->item->params->get('itemImageGallery') && !empty($this->item->gallery)): ?>
			<div class="row">
				<?php echo $this->item->gallery; ?>
			</div>
		<?php endif; ?>

		<?php if(!empty($this->item->fulltext)): ?>

		<?php if($this->item->params->get('itemIntroText')): ?>
		<div class="itemIntroText">
			<?php echo $this->item->introtext; ?>
		</div>
		<?php endif; ?>

		<?php if($this->item->params->get('itemFullText')): ?>
		<div class="itemFullText">
			<?php echo $this->item->fulltext; ?>
		</div>
		<?php endif; ?>
		<?php else: ?>
		<div class="itemFullText">
			<?php echo $this->item->introtext; ?>
		</div>
		<?php endif; ?>
		<?php echo $this->item->event->AfterDisplayContent; ?>
		<?php echo $this->item->event->K2AfterDisplayContent; ?>
	</div>
	<?php echo $this->item->event->AfterDisplay; ?>
	<?php echo $this->item->event->K2AfterDisplay; ?>
</div>