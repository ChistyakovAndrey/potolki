<?php
defined('_JEXEC') or die;
// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
?>

<div class="light-fixtures-item">
	<div class="col-sm-6">
	<div class="item-body1">
		<div class="lamp-selected">
							<?php 
						$dir = array();
						if($this->item->gallery !== "" && $this->item->gallery !== "NULL" && $this->item->gallery !== null  && !empty($this->item->gallery)){
							$id_gallery = explode('{/gallery}',explode("{gallery}",$this->item->gallery)[1])[0];
							$dir = array();
							$labels = JPATH_BASE.'/media/k2/galleries/'.$id_gallery.'/ru-RU.labels.txt';
							if(file_exists($labels)){ 
								$lines = file($labels); 
								foreach($lines as $line){ 
									$tmp = explode('|',$line); 
									$tmp_arr = array('file'=>$tmp[0],'title'=>$tmp[1],'description'=>$tmp[2]); 
									array_push($dir,$tmp_arr); 
								} 
							}
						}
					?>
					
				<?php if(count($dir) < 1):?>
					<img src="<?php echo $this->item->image;?>" alt="" id="ch0" class="">
					<?else:?>
					<?php $counter = 0;?>
					<?php foreach($dir as $image):?>
					<?php $counter++;?>
						<img src="<?php echo '/media/k2/galleries/'.$id_gallery.'/'.$image['file']?>" alt="" id="ch<?php echo $counter;?>" class="">
									
					<?php endforeach;?>
					<?php endif;?>
													
				</div>
		</div>
	</div>

	<div class="col-sm-6">
		<?php echo $this->item->event->BeforeDisplay; ?>
		<?php echo $this->item->event->K2BeforeDisplay; ?>
		<div class="catItemHeader">
			<?php if($this->item->params->get('catItemTitle')): ?>
				<h3 class="catItemTitle"><?php echo $this->item->title; ?></h3>
			<?php endif; ?>
		</div>
		
		<?php echo $this->item->event->AfterDisplayTitle; ?>
		<?php echo $this->item->event->K2AfterDisplayTitle; ?>

		<div class="catItemBody">
			<?php echo $this->item->event->BeforeDisplayContent; ?>
			<?php echo $this->item->event->K2BeforeDisplayContent; ?>

			<div>Цвет:</div>
			
			<!--БЛОК РАСЦВЕТОК-->
			<div class="lamp-select">
				<div class="select-list">
					<?php $counter = 0; ?> 
					<?php foreach($dir as $image):?>
					<?php $counter++;?>
					<div class="lamp-selector" data-color='<?php echo $image['title'];?>' data-chair="ch<?php echo $counter;?>" style="background:url(<?php echo '/media/k2/galleries/'.$id_gallery.'/'.$image['file']?>) no-repeat center; background-size:cover; ">
						<span class="select-color"><?php echo $image['title'];?></span>
					</div>
					<?php endforeach;?>
				</div>
			</div>
			<?php if($this->item->params->get('catItemImageGallery') && !empty($this->item->gallery)): ?>
				<?php echo $this->item->gallery; ?>
			<?php endif; ?>

			<?php if($this->item->params->get('catItemIntroText')): ?>
				<div class="catItemIntroText">
					<?php echo $this->item->introtext; ?>
				</div>
			<?php endif; ?>

			<div class="catItemExtraFields">
				<?php if (isset($this->item->extraFields->voltage) && !empty($this->item->extraFields->voltage->value)): ?>
					<span><?php echo $this->item->extraFields->voltage->name; ?>:</span> <?php echo $this->item->extraFields->voltage->value; ?>
				<?php endif; ?>

				<?php if (isset($this->item->extraFields->frequency) && !empty($this->item->extraFields->frequency->value)): ?>
					<span><?php echo $this->item->extraFields->frequency->name; ?>:</span> <?php echo $this->item->extraFields->frequency->value; ?>
				<?php endif; ?>

				<?php if (isset($this->item->extraFields->equipment) && !empty($this->item->extraFields->equipment->value)): ?>
					<span><?php echo $this->item->extraFields->equipment->name; ?>:</span> <?php echo $this->item->extraFields->equipment->value; ?>
				<?php endif; ?>

				<?php if ($this->item->params->get('itemExtraFields') && isset($this->item->extraFields->price) && !empty($this->item->extraFields->price->value)): ?>
					<div class="catItemPrice"><?php echo $this->item->extraFields->price->value; ?> руб</div>
				<?php endif; ?>
			</div>
			
			<?php echo $this->item->event->AfterDisplayContent; ?>
			<?php echo $this->item->event->K2AfterDisplayContent; ?>
		</div>

		<?php if ($this->item->params->get('catItemReadMore')): ?>
			<div class="catItemReadMore">
				<a class="button light-dark-button" href="<?php echo $this->item->link; ?>">Подробнее</a>
			</div>
		<?php endif; ?>
	</div>
	<?php echo $this->item->event->AfterDisplay; ?>
	<?php echo $this->item->event->K2AfterDisplay; ?>
</div>

