<?php
defined('_JEXEC') or die;
?>
<div class="container product">

		<div class="col-sm-10 col-sm-offset-1">
			<div class="row product-intro no-gutters">
				<div class="col-sm-6">
				<div class="item-body1">
		<div class="lamp-selected">
							<?php 
							$gallery = $this->item->params->get('itemImageGallery');
						$dir = array();
						if($gallery !== "" && $gallery !== "NULL" && $gallery !== null  && !empty($gallery)){
							$gallery = $gallery;
							$dir = array();
							$labels = JPATH_BASE.'/media/k2/galleries/'.$gallery.'/ru-RU.labels.txt';
							if(file_exists($labels)){ 
								$lines = file($labels); 
								foreach($lines as $line){ 
									$tmp = explode('|',$line); 
									$tmp_arr = array('file'=>$tmp[0],'title'=>$tmp[1],'description'=>$tmp[2]); 
									array_push($dir,$tmp_arr); 
								} 
							}
						}
					?>
				<?php if(count($dir) < 1):?>
					<img src="<?php echo $this->item->image;?>" alt="" id="ch0" class="">
					<?else:?>
					<?php $counter = 0;?>
					<?php foreach($dir as $image):?>
					<?php $counter++;?>
						<img src="<?php echo '/media/k2/galleries/'.$gallery.'/'.$image['file']?>" alt="" id="ch<?php echo $counter;?>" class="">
									
					<?php endforeach;?>
					<?php endif;?>
													
				</div>
				
		</div>
				</div>
				<div class="col-sm-6">
					<?php if($this->item->params->get('itemTitle')): ?>
						<h2 class="product-title"><?php echo $this->item->title; ?></h2>
					<?php endif; ?>
					
					<?php if($this->item->params->get('itemIntroText')): ?>
					<div class="product-short-description">
						<?php echo $this->item->introtext; ?>
					</div>
					<?php endif; ?>

					<div class="product-color">
						<h4>Цвет:</h4>
						<div class="lamp-select">
							<div class="select-list">
								<?php $counter = 0; ?> 
								<?php foreach($dir as $image):?>
									<?php $counter++;?>
									<div class="lamp-selector" data-color='<?php echo $image['title'];?>' data-chair="ch<?php echo $counter;?>" style="background:url(<?php echo '/media/k2/galleries/'.$gallery.'/'.$image['file']?>) no-repeat center; background-size:cover; ">
									<span class="select-color"><?php echo $image['title'];?></span>
									</div>
								<?php endforeach;?>
							</div>
						</div>
					</div>

					<?php if ($this->item->params->get('itemExtraFields') && isset($this->item->extraFields->price) && !empty($this->item->extraFields->price->value)): ?>
						<div class="product-price"><?php echo $this->item->extraFields->price->value; ?> руб</div>
					<?php endif; ?>

				</div>
			</div>

			<div class="row product-specifications">
				<div class="col-sm-6">
					<?php if(!empty($this->item->fulltext)): ?>
						<?php if($this->item->params->get('itemFullText')): ?>
						<div class="product-full-description">
							<div class="product-full-description-title">Описание:</div>
							<?php echo $this->item->fulltext; ?>
						</div>
						<?php endif; ?>
						<?php else: ?>
						<div class="product-full-description">
							<div class="product-full-description-title">Описание:</div>
							<?php echo $this->item->introtext; ?>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-sm-6">
					<div class="product-details">
						<h3 class="product-details-title">Характеристики:</h3>
						<?php if ($this->item->params->get('itemExtraFields')):?>
							<ul>
								<?php if (isset($this->item->extraFields->voltage) && !empty($this->item->extraFields->voltage->value)): ?>
									<li>
										<span><?php echo $this->item->extraFields->voltage->name; ?>:</span> <?php echo $this->item->extraFields->voltage->value; ?>
									</li>
								<?php endif; ?>
								<?php if (isset($this->item->extraFields->frequency) && !empty($this->item->extraFields->frequency->value)): ?>
									<li>
										<span><?php echo $this->item->extraFields->frequency->name; ?>:</span> <?php echo $this->item->extraFields->frequency->value; ?>
									</li>
								<?php endif; ?>
								<?php if (isset($this->item->extraFields->material) && !empty($this->item->extraFields->material->value)): ?>
									<li>
										<span><?php echo $this->item->extraFields->material->name; ?>:</span> <?php echo $this->item->extraFields->material->value; ?>
									</li>
								<?php endif; ?>
								<?php if (isset($this->item->extraFields->height) && !empty($this->item->extraFields->height->value)): ?>
									<li>
										<span><?php echo $this->item->extraFields->height->name; ?>:</span> <?php echo $this->item->extraFields->height->value; ?>
									</li>
								<?php endif; ?>
								<?php if (isset($this->item->extraFields->diameter) && !empty($this->item->extraFields->diameter->value)): ?>
									<li>
										<span><?php echo $this->item->extraFields->diameter->name; ?>:</span> <?php echo $this->item->extraFields->diameter->value; ?>
									</li>
								<?php endif; ?>
								<?php if (isset($this->item->extraFields->equipment) && !empty($this->item->extraFields->equipment->value)): ?>
									<li>
										<span><?php echo $this->item->extraFields->equipment->name; ?>:</span> <?php echo $this->item->extraFields->equipment->value; ?>
									</li>
								<?php endif; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>

	<?php echo $this->item->event->BeforeDisplay; ?>
	<?php echo $this->item->event->K2BeforeDisplay; ?>

	<?php echo $this->item->event->AfterDisplayTitle; ?>
	<?php echo $this->item->event->K2AfterDisplayTitle; ?>

	<?php echo $this->item->event->BeforeDisplayContent; ?>
	<?php echo $this->item->event->K2BeforeDisplayContent; ?>

	<?php echo $this->item->event->AfterDisplayContent; ?>
	<?php echo $this->item->event->K2AfterDisplayContent; ?>

	<?php echo $this->item->event->AfterDisplay; ?>
	<?php echo $this->item->event->K2AfterDisplay; ?>
</div>
<script>
	(function($){
		$(document).ready(function () {
			$('.lamp-selector').parent(".select-list").each(function(elem){
				$(this).children().first().addClass('active');
			});
			
			$('.lamp-selected').each(function(elem){
				$(this).children('img').first().addClass('active');
			});
			
			$('.item-body1 .lamp-selected img:not(".active")').hide();
			// смена цвета светильников начало
			$('.lamp-select .lamp-selector').click(function(){
				var id = $(this).attr('data-chair');
				$(this).parents('.col-sm-6').prev().children('.item-body1').children('.lamp-selected').children('#' + id).show();
				$(this).parents('.col-sm-6').prev().children('.item-body1').children('.lamp-selected').children('img').removeClass('active');
				$(this).parents('.col-sm-6').prev().children('.item-body1').children('.lamp-selected').children('#' + id).addClass('active');
				$(this).parent().children().removeClass('active');
				$(this).addClass('active');
				$('.item-body1 .lamp-selected img:not(".active")').hide();
			});
			// смена цвета светильников конец
		});
	})(jQuery);
</script>