<?php
defined('JPATH_BASE') or die;

$title = $this->escape($displayData['item']->category_title);
?>
<div class="col category-name" data-title="<?php echo JText::sprintf('COM_CONTENT_CATEGORY', $title); ?>" >
	<i class="far fa-folder"></i>
	<?php if ($displayData['params']->get('link_category') && $displayData['item']->catslug) : ?>
		<?php $url = '<span><a href="' . JRoute::_(ContentHelperRoute::getCategoryRoute($displayData['item']->catslug)) . '" itemprop="genre">' . $title . '</a></span>'; ?>
		<?php echo $url; ?>
	<?php else : ?>
		<?php echo JText::sprintf('COM_CONTENT_CATEGORY', '<span itemprop="genre">' . $title . '</span>'); ?>
	<?php endif; ?>
</div>