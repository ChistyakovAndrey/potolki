<?php
defined('JPATH_BASE') or die;
?>
<div class="parent-category-name">
	<?php $title = $this->escape($displayData['item']->parent_title); ?>
		<?php if ($displayData['params']->get('link_parent_category') && !empty($displayData['item']->parent_slug)) : ?>
			<?php $url = '<a href="' . JRoute::_(ContentHelperRoute::getCategoryRoute($displayData['item']->parent_slug)) . '" itemprop="genre">' . $title . '</a>'; ?>
			<?php echo JText::sprintf('COM_CONTENT_PARENT', $url); ?>
		<?php else : ?>
			<?php echo JText::sprintf('COM_CONTENT_PARENT', '<span itemprop="genre">' . $title . '</span>'); ?>
		<?php endif; ?>
</div>