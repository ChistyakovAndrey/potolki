<?php
defined('JPATH_BASE') or die;

JLog::add('The layout joomla.content.info_block.block is deprecated, use joomla.content.info_block instead.', JLog::WARNING, 'deprecated');
echo JLayoutHelper::render('joomla.content.info_block', $displayData);
