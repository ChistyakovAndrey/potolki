<?php
defined('JPATH_BASE') or die;
?>
<div class="col hits" data-title="<?php echo JText::sprintf('COM_CONTENT_ARTICLE_HITS', $hits) . $displayData['item']->hits; ?>">
	<i class="far fa-eye"></i>
		<meta itemprop="interactionCount" content="UserPageVisits:<?php echo $displayData['item']->hits; ?>" />
	<span><?php echo $displayData['item']->hits; ?></span>
</div>