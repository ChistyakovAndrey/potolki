<?php
defined('JPATH_BASE') or die;
$authorname = ($displayData['item']->created_by_alias ?: $displayData['item']->author);
?>
<div class="col createdby" itemprop="author" itemscope itemtype="https://schema.org/Person" 
data-title="<?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY', $author); ?><?php echo JText::sprintf($author) . JText::sprintf($authorname); ?>">
	<?php $author = '<span itemprop="name">' . $authorname . '</span>'; ?>
	<?php if (!empty($displayData['item']->contact_link ) && $displayData['params']->get('link_author') == true) : ?>
		<?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY', JHtml::_('link', $displayData['item']->contact_link, $author, array('itemprop' => 'url'))); ?>
	<?php else : ?>
		<?php echo JText::sprintf("<i class=\"far fa-user\"></i> " . $author); ?>
	<?php endif; ?>
</div>