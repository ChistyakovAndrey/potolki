<?php
defined('JPATH_BASE') or die;
?>
<div class="col create" data-title="<?php echo JText::sprintf('COM_CONTENT_CREATED_DATE_ON', $date) . JHtml::_('date', $displayData['item']->created, JText::_('DATE_FORMAT_LC3')); ?>">
	<i class="far fa-file-alt"></i>
	<span>
		<time datetime="<?php echo JHtml::_('date', $displayData['item']->created, 'c'); ?>" itemprop="dateCreated">
			<?php echo JHtml::_('date', $displayData['item']->created, JText::_('DATE_FORMAT_LC3')); ?>
		</time>
	</span>
</div>