<?php
defined('JPATH_BASE') or die;
?>
<div class="col published" data-title="<?php echo JText::sprintf('COM_CONTENT_PUBLISHED_DATE_ON', $date) . JHtml::_('date', $displayData['item']->publish_up, JText::_('DATE_FORMAT_LC3'));; ?>">
	<i class="far fa-calendar-plus"></i>
	<span>
		<time datetime="<?php echo JHtml::_('date', $displayData['item']->publish_up, 'c'); ?>" itemprop="datePublished">
		<?php echo JHtml::_('date', $displayData['item']->publish_up, JText::_('DATE_FORMAT_LC3')); ?>
		</time>
	</span>
</div>