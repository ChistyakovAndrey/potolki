<?php
defined('JPATH_BASE') or die;
?>
<div class="col modified" data-title="<?php echo JText::sprintf('COM_CONTENT_LAST_UPDATED', $date) . JHtml::_('date', $displayData['item']->modified, JText::_('DATE_FORMAT_LC3')); ?> ">
	<i class="far fa-sync-alt"></i>
	<span>
		<time datetime="<?php echo JHtml::_('date', $displayData['item']->modified, 'c'); ?>" itemprop="dateModified">
			<?php echo JHtml::_('date', $displayData['item']->modified, JText::_('DATE_FORMAT_LC3')); ?>
		</time>
	</span>
</div>