<?php 
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;

?>
<link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@17.12.0/dist/css/suggestions.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--[if lt IE 10]>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/suggestions-jquery@17.12.0/dist/js/jquery.suggestions.min.js"></script>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div id="helloworld-<?php if (isset($module->id)){echo $module->id;};?>" class="pelletgroup-calc helloworld-<?php if (isset($module->id)){echo $module->id;};?>">

	<!--выводим копирайт из настроек модуля-->
	<?php if($params->get('load_copyright', 1)) :?>
		<div class="copy">
			<?php echo $params->get('copyright') ?>
		</div>
	<?php endif;?>

	<!-- подложка модального окна -->
<div id="overlay"></div>

	<form action="<?php echo JURI::base().'modules/'.$module->module . '/helper.php';?>" class="needs-validation effect8" id="pellet-form" novalidate method="POST">

	<input type="hidden" name="execute_form" id="execute-form" value=""/>
	<input type="hidden" name="result_count" id="result-count" value=""/>
	<input type="hidden" name="calc_pellet" id="calc-pellet" value="" />
	<input type="hidden" name="calc_logistic" id="calc-logistic" value="" />
	
			<legend class="text-center"><?php echo JText::_('MOD_HELLOWORLD_TITLE'); ?></legend>

	<div class="calc-form-row">

<!--Блок слева начало -->
	<div class="calc-form-col-left">
		<div class="step-buttons">
<!--          <div class="custom-control custom-radio step-radio">
               <input id="step-1" name="paymentMethod" type="radio" class="steps-btn current-btn"/>
                <label class="custom-control-label" for="step-1">Шаг 1</label> 
              </div>
               <div class="custom-control custom-radio step-radio">
                <input id="step-2" name="paymentMethod" type="radio" class=steps-btn" />
                <label class="custom-control-label" for="step-2">Шаг 2</label>
              </div>
              <div class="custom-control custom-radio step-radio">
                <input id="step-3" name="paymentMethod" type="radio" class="steps-btn"/>
                <label class="custom-control-label" for="step-3">Шаг 3</label>
              </div> -->
			<div class="form-group">
				<div class="custom-control step-radio">
					<label class="radio">
					  <input id="step-1" name="paymentMethod" type="radio" class="steps-btn current-btn"/>
					  <div class="radio__text"><?php echo JText::_('MOD_HELLOWORLD_STEP_ONE'); ?></div>
					</label>
				</div>
				<div class="custom-control step-radio">
					<label class="radio">
					  <input id="step-2" name="paymentMethod" type="radio" class="steps-btn"/>
					  <div class="radio__text"><?php echo JText::_('MOD_HELLOWORLD_STEP_TWO'); ?></div>
					</label>
				</div>
				<div class="custom-control step-radio">
					<label class="radio">
					  <input id="step-3" name="paymentMethod" type="radio" class="steps-btn"/>
					  <div class="radio__text"><?php echo JText::_('MOD_HELLOWORLD_STEP_THREE'); ?></div>
					</label>
				</div>
			</div>
		</div>
	
	</div>
<!--Блок слева конец -->

<div class="calc-form-col-right steps">
				
<!--Шаг 1 начало-->	
	<div id="step-1-content" class="step animated fadeInRight current">
		<h4 class="mb-3 list-title"><?php echo JText::_('MOD_HELLOWORLD_CALC_PELLETS'); ?></h4>
		<div class="col">
			<div class="form-group">
				<label for="volume" class="field__text" style="font-size: 5rem;"><?php echo JText::_('MOD_HELLOWORLD_CALC_VOLUME'); ?></label>
				<input  name="volume" type="text" readonly id="volume" style="border:0px; font-weight: bold; display: inline-block; text-align: center; font-size: 8rem; width: 100px;">
				<div id="slider-range-pellets"></div>
			</div>

			<div class="form-group">
				<label for="packaging_type" class="field__text"><?php echo JText::_('MOD_HELLOWORLD_CALC_PACKAGING_TYPE'); ?> <span class="text-muted">(На поддоне)</span></label>
				<select name="packaging_type" class="custom-select d-block w-100" id="packaging_type">
					<option value="15">Мешки по 15кг</option>
					<option value="1000" disabled>Big-Bag (1т)</option>
				</select>
			</div>

			<div class="form-group">

				<div class="custom-control">
					<label class="checkbox" for="client">
					  <input name="client" type="checkbox" id="client" />
					  <div class="checkbox__text">Наличие карты клиента <span class="text-muted">(10% скидка)</span></div>
					</label>
				</div>

				<div class="form-group" id="clientCard" style="display: none;" >
					<label for="clientCard" class="field__text" style="display:none;">Номер карты клиента:</label>
					<input name="clientCard" class="form-control" type="text" placeholder="Введите номер карты клиента" value="" required="required">
				</div>
			</div>
			
			<div class="form-group text-center">
				<input type="button" class="calc-pellet" value="Рассчитать стоимость пеллет" name="count">
				<input type="button" class="next" value="<?php echo JText::_('MOD_HELLOWORLD_CALC_NEXT'); ?>"/>
			</div>
		</div>
		<div class="pagination-form">Шаг 1/3</div>
	</div>
<!--Шаг 1 конец-->

<!--Шаг 2 начало-->
	<div id="step-2-content" class="step animated fadeInRight">
		<h4 class="mb-3 list-title"><?php echo JText::_('MOD_HELLOWORLD_CALC_LOGISTIC'); ?></h4>
		<div class="col">
			<div class="form-group">
				<div class="custom-control">
					<label class="radio">
					  <input id="self-logistic" name="self_logistic" type="radio" value="0"/>
					  <div class="radio__text">Cамовывоз</div>
					</label>
				</div>
				<div class="custom-control">
					<label class="radio">
					  <input id="logistic" name="logistic" type="radio" value="3500"/>
					  <div class="radio__text">Доставка манипулятором до 10т <span class="text-muted">(Расчитывается индивидуально)</span></div>
					</label>
				</div>
			</div>

			<div class="mb-3" id="address_wrap" style="display:none;">
				<label for="address" class="field__text">Адрес доставки:</label>
				<input type="text" name="address" id="address" class="form-control" placeholder="МО, снт..." required="required" value=""/>
			</div>






<style>
html, body, #map {
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }
        #map {
            height:400px;
        }

        #map input{
        	color:#000;
        }
    </style>




<script type="text/javascript">
	(function($){ 
// яндекс карта

ymaps.ready(init);

function init() {
    // Стоимость за километр.
    var DELIVERY_TARIFF = 40,
    // Минимальная стоимость.
        MINIMUM_COST = 3500,
        myMap = new ymaps.Map('map', {
            center: [55.751574, 37.573856],
            zoom: 9,
            controls: []
        }),
    // Создадим панель маршрутизации.
        routePanelControl = new ymaps.control.RoutePanel({
            options: {
                // Добавим заголовок панели.
                showHeader: false,
                title: 'Расчёт доставки'
            }
        }),
        zoomControl = new ymaps.control.ZoomControl({
            options: {
                size: 'small',
                float: 'none',
                position: {
                    bottom: 145,
                    right: 10
                }
            }
        });
    // Пользователь сможет построить только автомобильный маршрут.
    routePanelControl.routePanel.options.set({
        types: {auto: true},
        allowSwitch: false
    });


//присваиваем переменной значение поля ввода адреса
var address = $("#address").val();

    // Если вы хотите задать неизменяемую точку "откуда", раскомментируйте код ниже.
    routePanelControl.routePanel.state.set({
        fromEnabled: false,
        from: 'с.Тарасовка пушкинский район',
        toEnabled: false,
        to: address  /*выводим переменную во вторую точку маршрута*/
     });

    myMap.controls.add(routePanelControl)/*.add(zoomControl) кнопки масштаба*/;

    // Получим ссылку на маршрут.
    routePanelControl.routePanel.getRouteAsync().then(function (route) {

        // Зададим максимально допустимое число маршрутов, возвращаемых мультимаршрутизатором.
        route.model.setParams({results: 1}, true);

        // Повесим обработчик на событие построения маршрута.
        route.model.events.add('requestsuccess', function () {

            var activeRoute = route.getActiveRoute();
            if (activeRoute) {
                // Получим протяженность маршрута.
                var length = route.getActiveRoute().properties.get("distance"),
                // Вычислим стоимость доставки.
                    price = calculate(Math.round(length.value / 1000)),
                // Создадим макет содержимого балуна маршрута.
                    balloonContentLayout = ymaps.templateLayoutFactory.createClass(
                        '<span>Расстояние: ' + length.text + '.</span><br/>' +
                        '<span style="font-weight: bold; font-style: italic">Стоимость доставки: ' + price + ' р.</span>');
                // Зададим этот макет для содержимого балуна.
                route.options.set('routeBalloonContentLayout', balloonContentLayout);
                // Откроем балун.
                activeRoute.balloon.open();



		$("#logistic_range").val(length.text); /*получаем длину маршрута и выводим ее на экран*/
				
            }
        });

    });
    // Функция, вычисляющая стоимость доставки.
    function calculate(routeLength) {
        return Math.max(routeLength * DELIVERY_TARIFF, MINIMUM_COST);
    }
}

})(jQuery);

</script>





<div id="map" style="display: none;"></div>












			<div class="form-group range-line" id="mkad" style="display:none;">
				<label for="logistic_range" class="field__text" style="font-size: 5rem;">Расстояние от МКАД <span class="text-muted">(км)</span>:</label>
				<input name="logistic_range" type="text" id="logistic_range" readonly style="border:0px; font-weight: bold; display: inline-block; text-align: center; font-size: 8rem; width: 100px;">
				<div id="slider-range-logistic"></div>
			</div>

			<div class="form-group text-center">
				<input type="button" value="Рассчитать стоимость доставки" class="calc-logistic">
				<input type="button" class="back" value="<?php echo JText::_('MOD_HELLOWORLD_CALC_BACK'); ?>"/>
				<input type="button" class="next" value="<?php echo JText::_('MOD_HELLOWORLD_CALC_NEXT'); ?>"/>
			</div>
		</div>
		<div class="pagination-form">Шаг 2/3</div>
	</div>
<!--Шаг 2 конец-->

<!--Шаг 3 начало-->
	<div id="step-3-content" class="step animated fadeInRight last">
		<h4 class="list-title"><?php echo JText::_('MOD_HELLOWORLD_CALC_ORDER'); ?></h4>																					
			<div class="col-md-12">

				<div class="form-group">
					<label for="client_name" class="field__text">Фамилия Имя Отчество:</label>
					<input class="form-control" name="client_name" id="client_name" type="text" autocomplete="off" placeholder="Иванов Иван Иванович" required="required" />
				</div>

				<div class="form-group">
					<label for="client_tel" class="field__text">Номер телефона:</label>
					<input class="form-control" type="text" id="client_tel" name="client_tel"  class="bfh-phone" data-format="+7 (ddd) ddd-dd-dd" maxlength="14"  required="required" />
				</div>

				<div class="form-group">
					<label for="date_logistic" class="field__text">Дата доставки:</label>
					<input class="form-control" name="date_logistic" type="text" id="datepicker" autocomplete="off" placeholder="<?php date('d-m-Y');?>" required="required">
				</div>

				<div class="form-group">
					<label class="checkbox">
					  <input name="personData" type="checkbox" value="" id="personData" required="required" />
					  <div class="checkbox__text field__text">Согласие на обработку персональных данных:</div>
					</label>
				</div>

				<div class="form-group text-center">
					<input type="button" value="<?php echo JText::_('MOD_HELLOWORLD_CALC_SEND_ORDER'); ?>" id="payment" class="execute">
					<input type="button" class="back" value="<?php echo JText::_('MOD_HELLOWORLD_CALC_BACK'); ?>"/>
				</div>

			</div>
		<div class="pagination-form">Шаг 3/3</div>
	</div>
<!--Шаг 3 конец-->

</div>

</div>

<div id='result-form' class='text-center'></div>
				
				

	</form>
</div>
