	<style>
		table{
			border: 1px solid black;
			
		}
		.table_mael tr:nth-child{
			background-color: #ccc;
		}
		.table_mail tr td:first-child{
			font-weight: bold;
		}
	</style>
		<table class="table_mail">
				<tr>
					<td class="thead">Имя:</td>
					<td><?php echo $this->client_name;?></td>
				</tr>
				<tr>
					<td class="thead">Телефон:</td>
					<td><?php echo $this->client_tel;?></td>
				</tr>
				<?php if(!empty($this->clientCard)):?>
				<tr>
					<td class="thead">Карта клиента:</td>
					<td><?php echo $this->clientCard;?></td>
				</tr>
				<?php endif;?>
				<tr>
					<td class="thead">Количество:</td>
					<td><?php echo $this->volume;?></td>
				</tr>
				<tr>
					<td class="thead">Доставка:</td>
					<td><?php 
					if($this->self_logistic == '0'){
						echo "Нет";
					}else{
						echo "Да";
					}					
					?></td>
				</tr>
				<?php if($this->self_logistic !== '0'):?>
				<tr>
					<td class="thead">Дата доставки:</td>
					<td><?php echo $this->date_logistic;?></td>
				</tr>
				<tr>
					<td class="thead">Адрес:</td>
					<td><?php echo $this->address;?></td>
				</tr>
				<tr>
					<td class="thead">Км от МКАД:</td>
					<td><?php echo $this->mkad;?></td>
				</tr>
				<?php endif;?>
				<tr>
					<td class="thead">Сумма товара:</td>
					<td><?php echo $this->summa();?></td>
				</tr>
				<tr>
					<td class="thead">Сумма доставки:</td>
					<td><?php echo $this->control_calc_logistic();?></td>
				</tr>
				<tr>
					<td class="thead">Итого:</td>
					<td><?php echo $this->resultat;?></td>
				</tr>
			
		</table>
		