<?php
defined('_JEXEC') or die;

function modChrome_block($module, &$params, &$attribs)
{
 	if (!empty ($module->content)) : ?>
           <div class="block <?php if ($params->get('moduleclass_sfx')!='') : ?><?php echo $params->get('moduleclass_sfx'); ?><?php endif; ?>">
           	<div class="moduletable">           	
	           	<?php if ($module->showtitle != 0) : ?>
			<div class="module-title">
	                	<h3 class="title"><span class="<?php echo $params->get('header_class'); ?>" ></span><?php echo $module->title ; ?></h3>
			</div>
	                	<?php endif; ?>
	                	<div class="module-content">
	                		<?php echo $module->content; ?>
	                	</div>
              </div>
           </div>
	<?php endif;
}



function modChrome_header_block($module, &$params, &$attribs){
	if (!empty ($module->content)) : ?>
		<div class="header_block<?php if ($params->get('moduleclass_sfx')!='') : ?><?php echo $params->get('moduleclass_sfx'); ?><?php endif; ?>">       	
			<?php if ($module->showtitle != 0) : ?>
				<div class="title">
				<span class="<?php echo $params->get('header_class'); ?>" ></span><?php echo $module->title ; ?>
				</div>
			<?php endif; ?>
			<div class="module-content">
				<?php echo $module->content; ?>
			</div>           	
		</div>
	<?php endif;
}




function modChrome_footer_block($module, &$params, &$attribs)
{
 	if (!empty ($module->content)) : ?>
           <div class="footer_block<?php if ($params->get('moduleclass_sfx')!='') : ?><?php echo $params->get('moduleclass_sfx'); ?><?php endif; ?>">       	
	           	<?php if ($module->showtitle != 0) : ?>
	            <div class="title"><span class="<?php echo $params->get('header_class'); ?>" ></span><?php echo $module->title ; ?></div>
	                	<?php endif; ?>
	            <div class="module-content">
	                <?php echo $module->content; ?>
	            </div>           	
           </div>
	<?php endif;
}



function modChrome_k2_filter($module, &$params, &$attribs){
	if (!empty ($module->content)) : ?>
		<div class="container text-center <?php if ($params->get('moduleclass_sfx')!='') : ?><?php echo $params->get('moduleclass_sfx'); ?><?php endif; ?>"> 
			<div class="row">
				<div class="col-sm-12">
					<?php if ($module->showtitle != 0) : ?>
						<div class="module-title">
							<div class="title h3">
								<span class="<?php echo $params->get('header_class'); ?>" ></span><?php echo $module->title ; ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="module-content">
			<?php echo $module->content; ?>
		</div>
	<?php endif;
}



	function modChrome_MBstyle($module, &$params, &$attribs){
		if (!empty ($module->content)) :
		?>
           <div class="MBstyle <?php  if ($params->get('moduleclass_sfx')!='') : ?><?php  echo $params->get('moduleclass_sfx'); ?><?php  endif; ?>">
           	<div class="moduletable">           	
	           	<?php  if ($module->showtitle != 0) : ?>
			<div class="module-title">
	                		<h3 class="title"><span class="<?php echo $params->get('header_class'); ?>" ></span><?php echo $module->title ; ?></h3>
                            			<div class="title-line"> <span></span> </div>
			</div>
	                	<?php endif; ?>
	                	<div class="module-content">
	                	<?php  echo $module->content; ?>
	                	</div>
              </div>             	
           </div>
	<?php endif; } ?>