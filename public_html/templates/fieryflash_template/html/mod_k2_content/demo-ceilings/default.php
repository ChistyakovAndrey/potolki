<?php
defined('_JEXEC') or die;
?>
<style>
	.category-ceilings .catItem{
		background: #fff;
		color:#000;
	}
</style>

<div class="category-ceilings">
	<div class="row">
		<div class="col-md-12">
			<div class="section-finished-objects-title">
				<h3><?php echo $module->title; ?></h3>
				<?php if($params->get('itemPreText')): ?>
					<div><?php echo $params->get('itemPreText'); ?></div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<?php if(count($items)): ?>
		<div class="slick-slider-responsive-2 slick-arrows-white">
		<?php foreach ($items as $key=>$item):	?>
			<div class="catItem">

				<?php if($params->get('itemImage') && isset($item->image)): ?>
					<div class="col-sm-12">
						<div class="row">
							<div class="image-wrap">
								<img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>" />
							</div>
						</div>
					</div>
				<?php endif; ?>

				<div class="col-sm-12">
					<div class="catItemBody">
						<?php if($params->get('itemTitle')): ?>
							<h3 class="catItemTitle"><?php echo $item->title; ?> <br/>S=<?php echo $item->extraFields->area->value; ?> м2</h3>
						<?php endif; ?>
						<div class="catItemExtraField">
							<div class="catItemExtraFieldTitle">Включено:</div>
							<div class="row">
								<div class="col-sm-6">
									<div><!-- <?php //echo $item->extraFields->area->name; ?>  -->полотно <?php echo $item->extraFields->area->value; ?> м2</div>
									<div><?php echo $item->extraFields->cornermachining->name; ?> <?php echo $item->extraFields->cornermachining->value; ?></div>
									<div><?php echo $item->extraFields->pipebypass->name; ?> <?php echo $item->extraFields->pipebypass->value; ?></div>
								</div>
								<div class="col-sm-6">
									<div><?php echo $item->extraFields->profile->name; ?> <?php echo $item->extraFields->profile->value; ?></div>
									<div><?php echo $item->extraFields->holes->name; ?> <?php echo $item->extraFields->holes->value; ?></div>
									<div><?php echo $item->extraFields->lampstand->name; ?> <?php echo $item->extraFields->lampstand->value; ?></div>
								</div>
							</div>
						</div>
						<hr/>
						<?php if($params->get('itemExtraFields') && count($item->extra_fields)): ?>
						<div class="row">
							<div class="col-sm-6">
								<?php if (isset($item->extraFields->price) && !empty($item->extraFields->price->value)): ?>
									<div class="price"><?php echo $item->extraFields->price->value; ?></div>
								<?php endif; ?>
							</div>
							<div class="col-sm-6">
								<?php if (isset($item->extraFields->freeservice) && !empty($item->extraFields->freeservice->value)): ?>
									<div class="catItemExtraField">
										<span class="free-service"><?php echo $item->extraFields->freeservice->name; ?></span>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<?php endif; ?>
						<hr/>
						<div class="row">
							<div class="col-sm-12">
								<?php if($params->get('itemReadMore')): ?>
									<!--<a class="light-dark-button" href="<?php echo $item->link; ?>">Подробнее</a>-->
									<a class="light-dark-button" href="/natyazhnye-potolki/faktury-potolkov">Подробнее</a>
								<?php endif; ?>
								<a href="/natyazhnye-potolki/kalkulyator-potolkov" class="light-dark-button go-to-calculator">Калькулятор</a>				
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php endforeach;?>
	</div>
	<?php endif; ?>
</div>