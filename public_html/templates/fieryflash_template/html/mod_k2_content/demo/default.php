<?php
defined('_JEXEC') or die;
$template = 'fieryflash_template';
JLoader::register('Fieryflash', JPATH_THEMES.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'functions.php');
$city = Fieryflash::get_city_name(true);
?>

<div id="<?php echo $module->id; ?>" class="<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">

	<div class="row">
		<div class="col-md-12">
			<div class="section-finished-objects-title">
				<h3><?php echo $module->title; ?> <?php echo $city; ?></h3>
				<?php if($params->get('itemPreText')): ?>
					<div><?php echo $params->get('itemPreText'); ?></div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<?php if(count($items)): ?>
	<div class="row">
		<div class="col-md-12 slick-slider-responsive-2 finished-objects-slider slick-arrows-white">
		<?php foreach ($items as $key=>$item):	?>
			<div class="slide-object">
				<div class="row">
					<div class="col-md-5">

					<?php if($params->get('itemTitle')): ?>
						<div class="slide-object-title"><?php echo $item->title; ?><br/><?php echo $item->extraFields->area->value; ?></div>
					<?php endif; ?>

						<hr/>
						<div class="assessment-wrap">
							<span>Оценка заказчика</span>
							<ul class="assessment">
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
							</ul>
						</div>
						<hr/>
						<?php $dateTime = new DateTime($item->extraFields->create_date->rawValue);?>
						<div class="location"><?php echo $item->extraFields->city->value; ?></div>
						<div class="calendar"><?php echo $dateTime->format('d.m.Y'); ?></div>
						<hr/>
						<div class="price"><?php echo $item->extraFields->price->value; ?></div>

						<a class="readmore light-dark-button" href="<?php echo $item->link; ?>">Подробнее</a>
						

					</div>
					<div class="col-md-7">
					<?php 
						$dir = array();
						if($item->gallery !== "" && $item->gallery !== "NULL" && $item->gallery !== null  && !empty($item->gallery)){
							$id_gallery = explode('{/gallery}',explode("{gallery}",$item->gallery)[1])[0];
							$dir = array();
							$labels = JPATH_BASE.'/media/k2/galleries/'.$id_gallery.'/ru-RU.labels.txt';
							if(file_exists($labels)){
							$lines = file($labels);
							foreach($lines as $line){
							$tmp = explode('|',$line);
							$tmp_arr = array('file'=>$tmp[0],'title'=>$tmp[1],'description'=>$tmp[2]); 
							array_push($dir,$tmp_arr);
							}
						}
					}
								?>
						<div class="ceilings-pictures">
							<div class="main-picture responsive-slider-4">
								<div class="ceilings-selected ceilings-image-wrap">
								<?php if(count($dir) < 1):?>
									<img src="<?php echo $item->image;?>" alt="" id="ch0" class="">
								<?else:?>
								<?php $counter = 0;?>
								<?php foreach($dir as $image):?>
								<?php $counter++;?>
									<img src="<?php echo '/media/k2/galleries/'.$id_gallery.'/'.$image['file'];?>" 
										alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>" 
										id="ceilings<?php echo $counter;?>" class=""/>
								<?php endforeach;?>
								<?php endif;?>
								</div>
							</div>
							<div class="all-pictures row">
								<?php $counter = 0; ?>
								<?php foreach($dir as $image):?>
								<?php $counter++;?>

								<div class="ceilings-miniatures-wrap col-xs-4">
									<img class="ceilings-selector" data-chair="ceilings<?php echo $counter;?>" src="<?php echo '/media/k2/galleries/'.$id_gallery.'/'.$image['file'];?>" alt="<?php echo $image['title'];?>">
								</div>			
									<?php endforeach;?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
	</div>
	<?php endif; ?>

	<?php if($params->get('itemCategory')): ?>
		<div class="row">
			<div class="col-md-12 all-reviews">
				<a href="<?php echo $item->categoryLink; ?>" class="button light-pink-button">Все отзывы</a>
			</div>
		</div>
	<?php endif; ?>	
</div>

<script>
	(function($){
		$(document).ready(function () {
			$('.ceilings-selector').each(function(elem){
				$(this).first().addClass('active');
			});

			$('.ceilings-selected').each(function(elem){
				$(this).children('img').first().addClass('active');
				$(this).children("img:not('.active')").hide();
			});

			$('.ceilings-selector').click(function(){
				var id = jQuery(this).attr('data-chair');
				$(this).parent().parent().siblings('.main-picture').children().children('img').each(function(elem){
					$(this).removeClass('active');
					$(this).hide();
				});
				$(this).parent().parent().siblings('.main-picture').children().children('img').filter('#' + id).addClass('active');
				$(this).parent().parent().siblings('.main-picture').children().children('img').filter('#' + id).show();

			});
		});
	})(jQuery);
</script>