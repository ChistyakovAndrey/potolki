<?php
defined('_JEXEC') or die;
?>
	<div class="row">
		<div class="col-md-12 texture-section-title">
			<h3><?php echo $module->title; ?></h3>
			<?php if($params->get('itemPreText')): ?>
				<?php echo $params->get('itemPreText'); ?>
			<?php endif; ?>
		</div>
	</div>

	<?php if(count($items)): ?>
		<div class="row row-flex textures-wrap mobile-slider slick-arrows-white">
			<?php foreach ($items as $key=>$item):	?>
				<div class="col-md-4">
					<div class="texture-item-<?php echo $key;?>">
					
						<?php if ($params->get('itemExtraFields') && isset($item->extraFields->discount) && !empty($item->extraFields->discount->value)): ?>
							<div class="item-discount"><?php echo $item->extraFields->discount->value; ?></div>
						<?php endif; ?>
						
						<?php if($params->get('itemImage') && isset($item->image)): ?>
							<a href="#" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">
								<img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>" />
							</a>
						<?php endif; ?>
						
						<div class="texture-item-body">

							<?php if($params->get('itemTitle')): ?>
								<div class="item-title"><?php echo $item->title; ?></div>
							<?php endif; ?>

							<?php if($params->get('itemIntroText')): ?>
								<div class="item-description">
									<?php echo $item->introtext; ?>
								</div>
							<?php endif; ?>

							<?php if ($params->get('itemExtraFields') && isset($item->extraFields->price) && !empty($item->extraFields->price->value)): ?>
								<div class="item-price"><?php echo $item->extraFields->price->value; ?></div>
							<?php endif; ?>

							<a class="readmore light-pink-button-inversion" href="/natyazhnye-potolki/tekhnologii-potolkov">Подробнее</a>
							<!--<a href="<//?php echo $item->link; ?>" class="readmore light-pink-button-inversion"><//?php echo JText::_('K2_READ_MORE'); ?></a>-->
						</div>

					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>

	<?php if($params->get('itemCategory')): ?>
		<div class="row">
			<div class="col-md-12 all-textures">
				<!--<a href="<//?php echo $item->categoryLink; ?>" class="button light-pink-button">Все фактуры</a>-->
				<a href="/natyazhnye-potolki/tekhnologii-potolkov" class="button light-pink-button">Все фактуры</a>
			</div>
		</div>
	<?php endif; ?>