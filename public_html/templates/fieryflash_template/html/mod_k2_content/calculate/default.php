<?php
defined('_JEXEC') or die;
?>
<div id="<?php echo $module->id; ?>" class="<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="section-finished-objects-title">
				<h3><?php echo $module->title; ?></h3>
				<?php if($params->get('itemPreText')): ?>
					<div><?php echo $params->get('itemPreText'); ?></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php if(count($items)): ?>
	<div class="row">
		<div class="col-md-12 responsive-slider-4 filter">
		<?php foreach ($items as $key=>$item):	?>
			<div class="slide-object">
				<?php if($params->get('itemTitle')): ?>
					<div class="slide-object-title"><?php echo $item->title; ?><br/><?php echo $item->extraFields->area->value; ?></div>
					<hr/>
				<?php endif; ?>
					<div>
						Включено:
					</div>
					<hr/>
					<div class="price"><?php echo $item->extraFields->price->value; ?></div>
			</div>
		<?php endforeach; ?>
		</div>
	</div>
	<?php endif; ?>
</div>
<script>
$('.responsive-slider-4').slick({
	dots: true,
	infinite: false,
	speed: 300,
	slidesToShow: 3,
	slidesToScroll: 3,
	responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: true
			}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
</script>