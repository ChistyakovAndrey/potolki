<?php
defined('_JEXEC') or die;
?>
<div class="catalog-preview">
	<div class="container">
		<div class="row">
			<div class="col-md-12 catalog-preview-title">
				<h3><?php echo $module->title; ?></h3>
				<?php if($params->get('itemPreText')): ?>
					<?php echo $params->get('itemPreText'); ?>
				<?php endif; ?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="catalog-items-preview slider slick-slider-responsive-4 slick-arrows-black">
					<?php foreach ($items as $key=>$item):	?>
						<div class="catalog-item-preview">
							<div class="item-body">
								<?php 
									$dir = array();
										if($item->gallery !== "" && $item->gallery !== "NULL" && $item->gallery !== null  && !empty($item->gallery)){
											$id_gallery = explode('{/gallery}',explode("{gallery}",$item->gallery)[1])[0];
											$dir = array();
											$labels = JPATH_BASE.'/media/k2/galleries/'.$id_gallery.'/ru-RU.labels.txt';
											if(file_exists($labels)){ 
												$lines = file($labels); 
												foreach($lines as $line){ 
													$tmp = explode('|',$line); 
													$tmp_arr = array('file'=>$tmp[0],'title'=>$tmp[1],'description'=>$tmp[2]); 
													array_push($dir,$tmp_arr); 
												} 
											}
										}
								?>
								<div class="lamp-selected">
								<?php if(count($dir) < 1):?>
									<img src="<?php echo $item->image;?>" alt="" id="ch0" class="">
								<?else:?>
									<?php $counter = 0;?>
									<?php foreach($dir as $image):?>
									<?php $counter++;?>
										<img src="<?php echo '/media/k2/galleries/'.$id_gallery.'/'.$image['file']?>" alt="" id="ch<?php echo $counter;?>" class="">
									<?php endforeach;?>
									<?php endif;?>
								</div>
									<div class="item-title"><?php echo $item->title;?></div>
									<div class="color-title">Цвет:</div>

									<div class="lamp-select">
										<div class="select-list">
											<?php 
											$counter = 0;
											foreach($dir as $image):?>
											<?php $counter++;?>
												<div class="lamp-selector" data-color='<?php echo $image['title'];?>' data-chair="ch<?php echo $counter;?>" style="background:url(<?php echo '/media/k2/galleries/'.$id_gallery.'/'.$image['file']?>) no-repeat center; background-size:cover; ">
													<span class="select-color"><?php echo $image['title'];?></span>
												</div>
											<?php 
											endforeach;?>
										</div>
									</div>

									<?php if (isset($item->extraFields->price) && !empty($item->extraFields->price->value)): ?>
										<div class="item-price"><?php echo $item->extraFields->price->value; ?> руб.</div>
									<?php endif;?>
							</div>
							<div class="item-readmore">
								<a class="light-dark-button" href="<?php echo $item->link; ?>"><?php echo JText::_('K2_READ_MORE'); ?></a>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>

		<?php if($params->get('itemCategory')): ?>
			<div class="row">
				<div class="col-md-12 to-catalog">
					<a href="<?php echo $item->categoryLink; ?>" class="button light-dark-button-inversion">Перейти в каталог</a>
				</div>
			</div>
		<?php endif; ?>	
	</div>
</div>

<script>
	(function($){
		$(document).ready(function () {
			$('.lamp-selector').each(function(elem){
				$(this).first().addClass('active');
			});
			
			$('.lamp-selected').each(function(elem){
				$(this).children('img').first().addClass('active');
			});
			// смена цвета светильников начало
			jQuery('.item-body .lamp-selector').click(function(){
				var id = jQuery(this).attr('data-chair');
				//jQuery('.selected img, .selector').removeClass('active');
				jQuery(this).parent().parent().siblings('.lamp-selected').children('img').removeClass('active');
				jQuery(this).parent().parent().siblings('.lamp-selected').children('#' + id).addClass('active');
				jQuery(this).parent().children().removeClass('active');
				//jQuery(this).parents('.lamp-selected img, .selector #' + id).addClass('active');
				jQuery(this).addClass('active');
			});
			// смена цвета светильников конец
		});
	})(jQuery);
</script>