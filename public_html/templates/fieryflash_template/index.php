<?php
defined('_JEXEC') or die('Restricted access');
$this->setHtml5(true);
JLoader::register('Fieryflash', JPATH_THEMES.DIRECTORY_SEPARATOR.$this->template.DIRECTORY_SEPARATOR.'functions.php');

$template = new Fieryflash();

$menu = $template->app->getMenu()->getActive();

if ($template->params->get('compile_sass', '0') === '1'){
	require_once "includes/sass.php";
};

if($template->settings['minimization_html'] == (int)"1"){
	ob_start('ob_html_compress');
}
$scripts_bottom = $template->settings['scripts_bottom'];
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<jdoc:include type="head" />
	<meta name="theme-color" content="#000"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
</head>
<body>

	<style>
		@media screen and (min-width: 968px){
			.container{
				padding-left:0px;
				padding-right:0px;
			}
		}
	</style>

<?php if ($template->params->get('preloader', 1)) : ?><div id="page-preloader" class="preloader"><div class="loader"></div></div><?php endif;?>

<main id="main">

<?php if($this->countModules('test')) : ?>
	<jdoc:include type="modules" name="test" style="none"/>
<?php endif; ?>

<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="/templates/fieryflash_template/js/slick/slick.min.js"></script>

<div class="ceilings">
<?php if($this->countModules('navigation')) : ?>
	<div id="navigation">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<jdoc:include type="modules" name="navigation" style="none" />
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

	<div class="container">
		<div class="row row-1">
			<?php if(($template->site->load_logo) && !empty($template->site->logo->img)) : ?>
				<div class="col-md-3">
					<div class="logo"> 
						<a href="<?php $link = "";
							if(class_exists('McsData')) $link .= McsData::get('city');
								$link .= $template->site->logo->link;
								echo $link;
							?>"> 
							<img style="width:<?php echo $template->site->logo->width; ?>px; height:<?php echo $template->site->logo->height; ?>px;" src="<?php echo $template->site->logo->img; ?>" alt="<?php echo $template->site->name; ?>" /> 
						</a> 
					</div>
				</div>
			<?php endif; ?>
			<?php if($this->countModules('contact-info-top')) : ?>
				<div class="hidden-xs col-md-7 col-md-offset-2">
					<jdoc:include type="modules" name="contact-info-top" style="none" />
				</div>
			<?php endif; ?>
		</div>

			<?php if(
				($this->countModules('discounter')) ||
				($this->countModules('free-metering'))
			) :?>
					<div class="row-2">
						<jdoc:include type="modules" name="discounter" style="none" />
						<div class="free-metering">
							<jdoc:include type="modules" name="free-metering" style="none" />
						</div>
					</div>
			<?php endif; ?>
	</div>
</div>

<?php include __DIR__ . '/includes/main.php'; ?>

<?php if($this->countModules('gray-section')) : ?>
	<jdoc:include type="modules" name="gray-section" style="none"/>
<?php endif; ?>

<?php if(
	($this->countModules('services-counter')) ||
	($this->countModules('range-slider'))) :?>
	<div class="section-2">
		<div class="container">
			<div class="row row-flex services-wrap">
				<?php if($this->countModules('services-counter')) : ?>
					<div class="col-sm-12 col-md-6">
						<jdoc:include type="modules" name="services-counter" style="none" />
					</div>
				<?php endif; ?>
				<?php if($this->countModules('range-slider')) : ?>
				<div class="col-sm-12 col-md-6">
					<jdoc:include type="modules" name="range-slider" style="none" />
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<div id="filter">
	<jdoc:include type="modules" name="filter" style="k2_filter" />
</div>

	<?php if($this->countModules('demo')) : ?>
		<div class="finished-objects">
			<div class="container">
				<jdoc:include type="modules" name="demo" style="none" />
			</div>
		</div>
	<?php endif; ?>

	<?php if($this->countModules('free-metering')) : ?>
		<div class="zamer">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="section-zamer-title">От замера<br/>до монтажа<br/>24 часа!</div>
						<!--<span class="information-label">до 30.09</span>
						<div class="price">
							<span>299</span>
							руб/м<sup>2</sup>
						</div>-->
					</div>
					<div class="col-md-4">
						<jdoc:include type="modules" name="free-metering" style="none"/>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if($this->countModules('ceiling-calculation')) : ?>
		<jdoc:include type="modules" name="ceiling-calculation" style="none"/>
	<?php endif; ?>

	<?php if($this->countModules('firm-counter')) : ?>
		<div class="firm-counter">
			<div class="container">
				<jdoc:include type="modules" name="firm-counter" style="none" />
			</div>
		</div>
	<?php endif; ?>	

	<?php if($this->countModules('ceilings-photo')) : ?>
		<jdoc:include type="modules" name="ceilings-photo" style="none" />
	<?php endif; ?>

	<?php if($this->countModules('fiery-masters')) : ?>
		<jdoc:include type="modules" name="fiery-masters" style="none" />
	<?php endif; ?>

	<?php if($this->countModules('panelnav')) : ?>
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src="/templates/fieryflash_template/js/slick/slick.min.js"></script>
		<jdoc:include type="modules" name="panelnav" style="none" />
	<?php endif; ?>

	<?php if($this->countModules('textures')) : ?>
		<div class="texture">
			<div class="container">
				<jdoc:include type="modules" name="textures" style="none" />
			</div>
		</div>
	<?php endif; ?>

	<?php if($this->countModules('white-section')) : ?>
		<div class="white-section">
			<div class="container">
				<jdoc:include type="modules" name="white-section" style="none" />
			</div>
		</div>
	<?php endif; ?>

	<?php if($this->countModules('ceiling-technology')) : ?>
		<jdoc:include type="modules" name="ceiling-technology" style="none" />
	<?php endif; ?>

	<?php if($this->countModules('collback-module')) : ?>
		<jdoc:include type="modules" name="collback-module" style="none" />
	<?php endif; ?>

	<?php if($this->countModules('maps')) : ?>
		<jdoc:include type="modules" name="maps" style="none" />
	<?php endif; ?>

	<?php include __DIR__ . '/includes/footer.php'; ?>

	</div>
</main>


<script src="<?php echo $template->settings['tpath']; ?>/js/template.js"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
	(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
	m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
	(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	ym(61501636, "init", {
		clickmap:true,
		trackLinks:true,
		accurateTrackBounce:true,
		webvisor:true
	});
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/61501636" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
<?php
	if($template->settings['minimization_html'] == (int)"1"){
		ob_end_flush();		
	}
?>