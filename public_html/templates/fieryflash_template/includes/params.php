<?php
defined('_JEXEC') or die;

// Getting params from template
	$params = JFactory::getApplication()->getTemplate(true)->params;
	$app = JFactory::getApplication();
	$doc = JFactory::getDocument();
	$config = JFactory::getConfig();
	$tpath = $this->baseurl . 'templates/' . $this->template;
	
	$minimization_html = $this->params->get('minimization_html', 1);

	// Column widths
	$leftColumn = $this->params->get('leftColumn', 1);
	if ($leftColumn){
		$leftcolgrid = ($this->countModules('left') == 0) ? 0 :
		$this->params->get('leftColumnWidth', 3);
	}
	$rightColumn = $this->params->get('rightColumn', 1);
	if ($rightColumn){
		$rightcolgrid = ($this->countModules('right') == 0) ? 0 :
		$this->params->get('rightColumnWidth', 3);
	}
	// Parameter

	$layout = $this->params->get('layout');
	$scripts_bottom = $this->params->get('scripts_bottom', 1);

	// Parameter
	$frontpageshow = $this->params->get('frontpageshow', 0);
	$yandex_counter = $this->params->get('yandex_counter');

	//name
	$load_name = $this->params->get('load_name', 1);
	$load_slogan = $this->params->get('load_slogan', 1);

	//phone
	$load_phone = $this->params->get('load_phone', 1);
	if ($load_phone){
		$telephone = $this->params->get('telephone');
	}

	//worktimes
	$load_worktimes = $this->params->get('load_worktimes', 1);
	if ($load_worktimes){
		$worktimes = $this->params->get('worktimes');}

	//email
	$load_email = $this->params->get('load_email', 1);
	if ($load_email){
		$email = $this->params->get('email');}

	//address
	$load_address = $this->params->get('load_address', 1);
	if ($load_address){
		$address = $this->params->get('address');}

	$user_stylesheet = $this->params->get('user_stylesheet');

	$backtotop = $this->params->get('backtotop', 1);
	$copyright = $this->params->get('copyright', 1);
	
// Use of Google Font
if ($this->params->get('googleFont'))
{
	JHtml::_('stylesheet', '//fonts.googleapis.com/css?family=' . $this->params->get('googleFontName'));
	$this->addStyleDeclaration("
	h1, h2, h3, h4, h5, h6, .site-title {
		font-family: '" . str_replace('+', ' ', $this->params->get('googleFontName')) . "', sans-serif;
	}");
}
//Body Font
if($this->params->get('bodyFont')){
    	JHtml::_('stylesheet', '//fonts.googleapis.com/css?family=' . $this->params->get('bodyFontName'));
		$this->addStyleDeclaration("
			body {
		font-family: '" . str_replace('+', ' ', $this->params->get('bodyFontName')) . "', sans-serif;
	}");
}
//Navigation Font
if( $this->params->get('navigationFont')){
    	JHtml::_('stylesheet', '//fonts.googleapis.com/css?family=' . $this->params->get('navigationFontname'));
		$this->addStyleDeclaration("
			nav {
		font-family: '" . str_replace('+', ' ', $this->params->get('navigationFontname')) . "', sans-serif;
	}");
}
	
function ob_html_compress($buf){
		return preg_replace(array('/<!--(?>(?!\[).)(.*)(?>(?!\]).)-->/Uis','/[[:blank:]]+/'),array('',' '),str_replace(array("\n","\r","\t"),'',$buf));
	}
?>