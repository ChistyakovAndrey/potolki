<?php
defined('_JEXEC') or die;




?>

<!-- breadcrumbs начало -->
<?php if($this->countModules('breadcrumbs')) :?>
	<div id="breadcrumbs">
		<div class="container">
			<jdoc:include type="modules" name="breadcrumbs" style="block" />
		</div>
	</div>
<?php endif; ?>
<!-- breadcrumbs конец -->



          <!-- Front page show or hide -->
          <?php
            $app = JFactory::getApplication();
            $menu = $app->getMenu();
            if ($this->params->get('frontpageshow', 0)){
              // show on all pages
              ?>
              
<div class="main-content">
  <div class="container<?php if($template->settings['layout']=='full'){ ?>-fluid<?php }; ?>">
    <div class="row">
      <main id="main-box"> 
              
          <div id="main-box">
            <jdoc:include type="message" />
            <jdoc:include type="component" />
          </div>
          
      </main>
    </div>	
  </div>
</div>      
          
          
          <?php
            } else {
              if ($menu->getActive() !== $menu->getDefault()) {
                // show on all pages but the default page
                ?>
                
<div class="main-content">
  <div class="container<?php if($template->settings['layout']=='full'){ ?>-fluid<?php }; ?>">
    <div class="row">
      <main id="main-box">              
                
          <div id="main-box">
          <jdoc:include type="message" />
          <jdoc:include type="component" />
          </div>
          
      </main>
    </div>

    <?php if($this->countModules('content-bottom')) : ?>
       <div class="row" id="content-bottom">
         <jdoc:include type="modules" name="content-bottom" style="block" />
      </div>
    <?php endif; ?>
	
  </div>
</div>    
          
          <?php
           }} ?>
          <!-- Front page show or hide -->
