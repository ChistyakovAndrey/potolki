<?php defined('_JEXEC') or die; ?>
<footer>
	<div class="container">
		<div class="row">
			<?php if(($template->site->load_logo) && !empty($template->site->logo->img)) : ?>
				<div class="col-md-4 f-2">
          <div class="logo"> 
            <a href="<?=$template->site->logo->link;?>"> 
              <img style="width:<?php echo $template->site->logo->width; ?>px; height:<?php echo $template->site->logo->height; ?>px;" src="<?php echo $template->site->logo->img; ?>" alt="<?php echo $template->site->name; ?>" /> 
            </a> 
          </div>
				</div>
			<?php endif; ?>

			<?php if($this->countModules('footer-1')) : ?>
				<div class="hidden-xs col-sm-3 col-md-2 f-2 text-sm-left">
					<jdoc:include type="modules" name="footer-1" style="footer_block" />
				</div>
			<?php endif; ?>

			<?php if($this->countModules('footer-2')) : ?>
				<div class="hidden-xs col-sm-3 col-md-2 f-3 text-sm-left">
					<jdoc:include type="modules" name="footer-2" style="footer_block" />
				</div>
			<?php endif; ?>

			<?php if($this->countModules('footer-3')) : ?>
				<div class="hidden-xs col-sm-3 col-md-2 f-4 text-sm-left">
					<jdoc:include type="modules" name="footer-3" style="footer_block" />
				</div>
			<?php endif; ?>

			<?php if($this->countModules('footer-4')) : ?>
				<div class="hidden-xs col-sm-3 col-md-2 f-5 text-sm-left">
					<jdoc:include type="modules" name="footer-4" style="footer_block" />
				</div>
			<?php endif; ?>
		</div>

		<div class="row" style="margin-top:50px;">
			<jdoc:include type="modules" name="pb-copyright" style="none" />

			<div class="col-md-12 pb-copy-wrap row-flex">
				<div class="pb-copyright">
					&copy; <?php echo date('Y'); ?> "Потолки будущего"<br/>Россия, Москва<br/>Каширское шоссе, 105
				</div>
				<?php if(!empty($template->settings['telephone']) && $template->settings['telephone'] !== "") : ?>
					<div class="phone">
						<i class="animated tada infinite"></i> <a href="tel:<?php echo $template->settings['telephone'];?>" data-toggle="tooltip" title="Позвонить"><?php echo $template->settings['telephone'];?></a>
						<div>Звонок по РФ<br/>бесплатный</div>
					</div>
				<?php endif;?>
			</div>
		</div>
	
		<?php if($template->settings['copyright']) : ?>
			<div class="row">
				<div class="col-md-12" id="copy" role="contentinfo">
					<?php echo JText::_('TPL_FIERYFLASH_TEMPLATE_COPYRIGHT');?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</footer>